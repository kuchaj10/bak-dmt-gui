﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using DMTGui.Server.Services;
using Microsoft.AspNetCore.Mvc;
using DMTGui.Shared.Contracts;
using DMTGui.Shared.Exceptions;

namespace DMTGui.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase, IJobContract
    {
        private readonly IJobService _jobService;
        private readonly IMigrationService _migrationService;

        public JobsController(IJobService jobService, IMigrationService migrationService)
        {
            _jobService = jobService;
            _migrationService = migrationService;
        }

        /// <summary>
        /// Vrátí všechny úlohy.
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<JobDto>> GetAll([FromQuery] SearchDto search)
        {
            var jobs = await _jobService.GetJobsAsync();

            return jobs;
        }

        /// <summary>
        /// Vrátí konkrétní úlohu podle předaného identifikátoru.
        /// </summary>
        [HttpGet("{jobId}")]
        public async Task<JobDto> Get(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();
            var job = await _jobService.GetJobAsync(jobIdAsGuid);

            if (job is null)
                throw new NotFoundException();

            return job;
        }

        /// <summary>
        /// Přidá předanou úlohu včetně informací jako jsou odkazy na profily a vytvořené triggery pro spuštění úloh.
        /// </summary>
        [HttpPost]
        public async Task<JobDto> Add([FromBody] JobDto job)
        {
            job.ThrowIfNotValid();

            var addedJob = await _jobService.AddJobAsync(job);

            return addedJob;
        }

        [HttpDelete("{jobId}")]
        public async Task Delete(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();

            await _jobService.DeleteJobAsync(jobIdAsGuid);
        }

        [HttpPut("{jobId}")]
        public async Task Update(string jobId, [FromBody] JobDto job)
        {
            job.Id = jobId;

            job.ThrowIfNotValid();

            await _jobService.UpdateJobAsync(job);
        }

        [HttpPut("{jobId}/enable/true")]
        public async Task EnableJob(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();

            await _migrationService.EnableMigrationAsync(jobIdAsGuid);
        }

        [HttpPut("{jobId}/enable/false")]
        public async Task DisableJob(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();

            await _migrationService.DisableMigrationAsync(jobIdAsGuid);
        }

        [HttpPut("{jobId}/run/true")]
        public async Task StartJob(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();

            await _migrationService.RunMigrationAsync(jobIdAsGuid);
        }

        [HttpPut("{jobId}/run/false")]
        public async Task StopJob(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();

            await _migrationService.StopMigrationAsync(jobIdAsGuid);
        }
    }
}
