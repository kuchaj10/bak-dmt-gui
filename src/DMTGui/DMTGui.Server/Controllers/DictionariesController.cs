﻿using DMTGui.Shared.Exceptions;
using DMTGui.Server.Services;
using DMTGui.Shared.Models;
using DMTGui.Shared.Contracts;
using Microsoft.AspNetCore.Mvc;
using DMTGui.Shared.Extensions;

namespace DMTGui.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DictionariesController : ControllerBase, IDictionaryContract
    {
        private readonly IDictionaryService _dictionaryService;

        public DictionariesController(IDictionaryService dictionaryService)
        {
            _dictionaryService = dictionaryService;
        }

        [HttpGet]
        public async Task<IEnumerable<DictionaryDto>> GetAll([FromQuery] SearchDto search)
        {
            return await _dictionaryService.GetDictionariesAsync();
        }

        [HttpGet("names")]
        public async Task<IEnumerable<DictionaryDto>> GetAllNames([FromQuery] SearchDto search)
        {
            return await _dictionaryService.GetDictionariesNamesAsync();
        }

        [HttpGet("{name}")]
        public async Task<DictionaryDto> Get([FromRoute] string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new WrongOperationException("Název slovníku nemůže být prázdný");

            var dictionary = await _dictionaryService.GetDictionaryByNameAsync(name);
            if (dictionary is null)
                throw new NotFoundException();

            return dictionary;
        }


        [HttpPost]
        public async Task<DictionaryDto> Add([FromBody] DictionaryDto item)
        {
            item.ThrowIfNotValid();

            item = await _dictionaryService.AddNewDictionaryAsync(item);

            return item;
        }

        [HttpPut("{name}")]
        public async Task Update(string name, [FromBody] DictionaryDto item)
        {
            item.Name = name;

            item.ThrowIfNotValid();

            await _dictionaryService.UpdateDictionaryAsync(item);
        }

        [HttpDelete("{name}")]
        public async Task Delete([FromRoute] string name)
        {
            await _dictionaryService.DeleteDictionaryByNameAsync(name);
        }
    }
}
