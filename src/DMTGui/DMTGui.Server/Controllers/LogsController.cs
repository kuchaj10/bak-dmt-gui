﻿using DMTGui.Server.Services;
using DMTGui.Shared.Contracts;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace DMTGui.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogsController : ControllerBase, ILogContract
    {
        private readonly ILogService _logService;

        public LogsController(ILogService logService)
        {
            _logService = logService;
        }

        [HttpGet]
        public async Task<IEnumerable<LogDto>> GetLogs([FromQuery] LogSearchDto logSearch)
        {
            if (logSearch is null)
            {
                return await _logService.GetLogsAsync();
            }

            logSearch.ThrowIfNotValid();

            return await _logService.GetLogsAsync(logSearch);
        }

        [HttpGet("count")]
        public async Task<int> GetLogsCount([FromQuery] LogSearchDto logSearch)
        {
            if (logSearch is null)
            {
                return await _logService.GetLogsCountAsync();
            }

            logSearch.ThrowIfNotValid();

            return await _logService.GetLogsCountAsync(logSearch);
        }

        [HttpGet("states")]
        public async Task<IEnumerable<string>> GetMigrationStates()
        {
            return await _logService.GetMigrationStatesAsync();
        }

        [HttpGet("levels")]
        public async Task<IEnumerable<string>> GetLogLevels()
        {
            return await _logService.GetLogLevelsAsync();
        }
    }
}
