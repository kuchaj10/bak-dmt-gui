﻿using DMTGui.Server.Services;
using DMTGui.Shared.Contracts;
using DMTGui.Shared.Exceptions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace DMTGui.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatisticsController : ControllerBase, IStatisticsContract
    {
        private readonly ILogger<StatisticsController> _logger;
        private readonly IStatisticsService _statisticsService;

        public StatisticsController(ILogger<StatisticsController> logger, IStatisticsService statisticsService)
        {
            _logger = logger;
            _statisticsService = statisticsService;
        }

        [HttpGet]
        public async Task<IEnumerable<MigrationStatisticsDto>> GetAll([FromQuery] StatisticsSearchDto search = null)
        {
            search.ThrowIfNotValid();

            return await _statisticsService.GetAllJobsStatisticsAsync(search);
        }

        [HttpGet("{jobId}")]
        public async Task<MigrationStatisticsDto> Get(string jobId)
        {
            var jobIdAsGuid = jobId.ToGuidFromBase64String();

             return await _statisticsService.GetJobStatisticsAsync(jobIdAsGuid);
        }

        public Task<MigrationStatisticsDto> Add(MigrationStatisticsDto item)
        {
            throw new AuthorizationException();
        }

        public Task Update(string identifier, MigrationStatisticsDto item)
        {
            throw new AuthorizationException();
        }

        public Task Delete(string identifier)
        {
            throw new AuthorizationException();
        }
    }
}
