﻿using Microsoft.AspNetCore.Mvc;

namespace DMTGui.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeapotController : ControllerBase
    {
        [HttpGet("")]
        public ActionResult Get()
        {
            return StatusCode(418, "I am a teapot!");
        }
    }
}
