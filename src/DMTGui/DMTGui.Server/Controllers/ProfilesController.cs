﻿using DMTGui.Shared.Exceptions;
using DMTGui.Server.Services;
using DMTGui.Shared.Contracts;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using Microsoft.AspNetCore.Mvc;

namespace DMTGui.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase, IProfileContract
    {
        private readonly ILogger<ProfilesController> _logger;

        private readonly IProfileService _profileService;

        public ProfilesController(ILogger<ProfilesController> logger, IProfileService profileService)
        {
            _logger = logger;
            _profileService = profileService;
        }

        /// <summary>
        /// Vrátí všechny metadata profilů, které jsou uloženy, bez informace samotných dat profilu.
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<ProfileDto>> GetAll([FromQuery] SearchDto search)
        {
            var profiles = await _profileService.GetProfilesAsync();

            return profiles;
        }

        [HttpGet("default")]
        public async Task<ProfileDto> GetDefault()
        {
            var task = Task.Run(() =>
            {
                var defaultProfile = MigrationProfile.GetDefaultAsString();

                var profileDto = new ProfileDto();
                profileDto.Name = "Nový profil";
                profileDto.Data = defaultProfile;

                return profileDto;
            });

            return await task;
        }

        [HttpGet("{profileId}")]
        public async Task<ProfileDto> Get(string profileId)
        {
            var profileIdAsGuid = profileId.ToGuidFromBase64String();
            var profile = await _profileService.GetProfileAsync(profileIdAsGuid);

            return profile;
        }


        [HttpPost]
        public async Task<ProfileDto> Add([FromBody] ProfileDto profile)
        {
            profile.ThrowIfNotValid();

            var addedProfile = await _profileService.AddProfileAsync(profile);

            return addedProfile;
        }

        [HttpPut("{profileId}")]
        public async Task Update(string profileId, [FromBody] ProfileDto profile)
        {
            profile.Id = profileId;

            profile.ThrowIfNotValid();

            await _profileService.UpdateProfileAsync(profile);
        }

        [HttpDelete("{profileId}")]
        public async Task Delete(string profileId)
        {
            if (string.IsNullOrEmpty(profileId))
                throw new WrongOperationException();

            var profileIdAsGuid = profileId.ToGuidFromBase64String();

            await _profileService.DeleteProfileAsync(profileIdAsGuid);
        }
    }
}
