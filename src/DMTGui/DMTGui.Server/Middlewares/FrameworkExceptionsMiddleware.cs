﻿using DMTGui.Shared.Exceptions;

namespace DMTGui.Server.Middlewares
{
    /// <summary>
    /// Middleware na přemapování chyb z migrace na naše custom chybové hlášky
    /// </summary>
    public class FrameworkExceptionsMiddleware : IMiddleware
    {
        private readonly RequestDelegate _delegateToInvoke;
        private readonly IWebHostEnvironment _environtment;

        public FrameworkExceptionsMiddleware(RequestDelegate delegateToInvoke, IWebHostEnvironment environtment)
        {
            _delegateToInvoke = delegateToInvoke;
            _environtment = environtment;
        }

        /// <inheritdoc/>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _delegateToInvoke(httpContext);
            }
            catch (ArgumentNullException ex)
            {
                throw GetExceptionByContext(ex);
            }
            catch (ArgumentException ex)
            {
                throw GetExceptionByContext(ex);
            }
        }

        private WrongOperationException GetExceptionByContext(Exception ex)
        {
            if (!_environtment.IsDevelopment())
                return new WrongOperationException();

            return new WrongOperationException(ex.Message);
        }
    }
}
