﻿using DMTGui.Shared.Exceptions;
using DMTGui.Shared.Models;
using System.Net;

namespace DMTGui.Server.Middlewares
{
    /// <summary>
    /// Middleware na odchytávání našich chyb a vrácí odpovědí na dotazovatele v očekávaném tvaru.
    /// </summary>
    public class ExceptionsHandlerMiddleware : IMiddleware
    {
        private readonly RequestDelegate _requestDelegate;
        private readonly ILogger _logger;

        public ExceptionsHandlerMiddleware(RequestDelegate requestDelegate, ILogger<ExceptionsHandlerMiddleware> logger)
        {
            _requestDelegate = requestDelegate;
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _requestDelegate(httpContext);
            }
            catch (NotFoundException ex)
            {
                _logger.LogInformation($"Nebyl nalezen zdroj: {ex}");
                await WriteResponseAsync(httpContext, ex, (int)HttpStatusCode.NotFound);
            }
            catch (ServerException ex)
            {
                _logger.LogError(ex, $"Nastal očekávaný problém: {ex}");
                await WriteResponseAsync(httpContext, ex, (int)HttpStatusCode.BadRequest);
            }
            catch (AuthorizationException ex)
            {
                _logger.LogError(ex, $"Nastala chyba autorizace: {ex}");
                await WriteResponseAsync(httpContext, ex, (int)HttpStatusCode.Unauthorized);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Nastal neočekávaný problém: {ex}");
                await WriteResponseAsync(httpContext, ex, (int)HttpStatusCode.InternalServerError);
            }
        }

        private static async Task WriteResponseAsync(HttpContext context, Exception exception, int statusCode)
        {
            var details = new ErrorDetail
            {
                StatusCode = statusCode,
                Message = exception.Message
            };

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;

            await context.Response.WriteAsJsonAsync(details);
        }
    }
}
