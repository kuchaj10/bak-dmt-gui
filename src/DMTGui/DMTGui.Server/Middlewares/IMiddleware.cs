﻿namespace DMTGui.Server.Middlewares
{
    /// <summary>
    /// Rozhraní pro middleware
    /// </summary>
    public interface IMiddleware
    {
        /// <summary>
        /// Invokace chování middleware
        /// </summary>
        public Task InvokeAsync(HttpContext httpContext);
    }
}
