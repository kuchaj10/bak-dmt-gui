﻿using DMTGui.Dal;
using DMTGui.Server.Extensions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using ICZ.Sps.DMT.DMTLib.Migration;
using Microsoft.EntityFrameworkCore;
using Quartz;

namespace DMTGui.Server.Quartz.Jobs
{
    public class MigrationJob : Job<MigrationJobData>
    {
        protected override string GetJobName() => nameof(MigrationJob);

        private ILogger<MigrationJob> _logger;

        private Guid _jobId;
        private IEnumerable<ProfileDto> _profiles = new ProfileDto[0];
        private IDbContextFactory<AppDbContext> _contextFactory;

        private AppDbContext _dbContext;

        private Timer _timerToStop;

        protected override void Initialize(IJobExecutionContext context)
        {
            var jobData = GetJobData(context);

            _logger = jobData.GetLogger();

            _jobId = jobData.GetJobId();
            _profiles = jobData.GetProfiles();
            _contextFactory = jobData.GetContextFactory();

            if (_contextFactory is null)
                throw new InvalidOperationException("Context factory nemůže být prázná");

            _dbContext = _contextFactory.CreateDbContext();
        }

        protected override void ExecuteInternal(IJobExecutionContext context)
        {
            try
            {
                UpdateStart();

                var timeToStop = context.Trigger.GetLastFireTime(DateTimeOffset.Now);
                if (timeToStop.HasValue)
                {
                    var timeSpan = timeToStop - DateTime.Now;
                    _timerToStop = new Timer(async (object state) => await InteruptJob(context));
                    _timerToStop.Change(timeSpan.Value, Timeout.InfiniteTimeSpan);

                    UpdateScheduledStop(timeToStop.Value);
                }

                var progress = new Progress();
                progress.ProgressUpdate += Progress_UpdateProgress;

                var stepCount = _profiles.Count();
                var actualStep = 1;
                foreach (var profile in _profiles)
                {
                    context.CancellationToken.ThrowIfCancellationRequested();

                    UpdateMigrationStep(_jobId, actualStep, stepCount);

                    var migrationData = MigrationProfile.LoadFromString(profile.Data);
                    migrationData.Name = profile.Name;

                    var migration = new Migration(migrationData, _jobId.ToBase64String());
                    migration.Migrate(context.CancellationToken, progress);

                    actualStep++;
                }
            }
            catch (OperationCanceledException)
            {
                _logger.LogInformation("Migrace zrušena");

                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Nastala neočekávaná chyba při migraci {ex}", ex);

                throw;
            }
            finally
            {
                if (context.NextFireTimeUtc is null)
                {
                    UpdateUnchedule();
                }

                UpdateStop();
            }
        }

        private async Task InteruptJob(IJobExecutionContext context)
        {
            await context.Scheduler.Interrupt(context.JobDetail.Key);
        }

        private void Progress_UpdateProgress(object sender, ProgressUpdateEventArgs e)
        {
            var progress = sender as Progress;
            if (progress is null)
                return;

            var job = _dbContext.Jobs.FirstOrDefault(job => job.Id == _jobId);
            if (job is null)
                return;

            job.ActualQueueSize = progress.QueueProcessingSize;
            job.QueueSize = progress.QueueSize;

            _dbContext.Update(job);
            _dbContext.SaveChanges();
        }
        private void UpdateUnchedule()
        {
            var job = _dbContext.Jobs.FirstOrDefault(job => job.Id == _jobId);
            if (job is null)
                return;

            job.IsScheduled = false;

            _dbContext.Update(job);
            _dbContext.SaveChanges();
        }

        private void UpdateStart()
        {
            var job = _dbContext.Jobs.FirstOrDefault(job => job.Id == _jobId);
            if (job is null)
                return;

            job.IsRunning = true;
            job.LastRun = DateTime.Now;
            job.ScheduledStop = null;

            _dbContext.Update(job);
            _dbContext.SaveChanges();
        }

        private void UpdateScheduledStop(DateTimeOffset timeToStop)
        {
            var job = _dbContext.Jobs.FirstOrDefault(job => job.Id == _jobId);
            if (job is null)
                return;

            job.ScheduledStop = timeToStop.DateTime;

            _dbContext.Update(job);
            _dbContext.SaveChanges();
        }

        private void UpdateStop()
        {
            var job = _dbContext.Jobs.FirstOrDefault(job => job.Id == _jobId);
            if (job is null)
                return;

            job.IsRunning = false;

            _dbContext.Update(job);
            _dbContext.SaveChanges();
        }

        private void UpdateMigrationStep(Guid jobId, int actualStep, int stepCount)
        {
            var job = _dbContext.Jobs.FirstOrDefault(job => job.Id == jobId);
            if (job is null)
                return;

            job.ActualStep = actualStep;
            job.StepCount = stepCount;

            _dbContext.Update(job);
            _dbContext.SaveChanges();
        }
    }
}
