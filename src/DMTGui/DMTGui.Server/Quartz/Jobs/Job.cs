using ICZ.Sps.DMT.DMTLib.Extensions;
using Quartz;

namespace DMTGui.Server.Quartz.Jobs
{
    /// <summary>
    /// Abstraktní třída reprezentující implementaci úloh podle <see cref="Quartz"/>
    /// </summary>
    /// <typeparam name="TData">Reprezentuje třídu, která obsahuje všechny potřebná data pro běh dané migrace.</typeparam>
    [DisallowConcurrentExecution]
    public abstract class Job<TData> : IJob
    {
        protected abstract void Initialize(IJobExecutionContext context);

        protected abstract void ExecuteInternal(IJobExecutionContext context);

        protected abstract string GetJobName();

        public async Task Execute(IJobExecutionContext context)
        {
            //Pokud se jedná o poslední volání k běhu, tak provedu ukončení migrace
            //if (IsLastTriggerFire(context))
            //{
            //    await context.Scheduler.Interrupt(context.JobDetail.Key);
            //    return;
            //}

            try
            {
                Initialize(context);
                ExecuteInternal(context);
            }
            catch (OperationCanceledException)
            {
                //_logger.LogInformation("Zpracování bylo přerušeno");

                var nextFireTime = context.NextFireTimeUtc?.LocalDateTime;
                if (nextFireTime is not null)
                {
                    //_logger.LogInformation($"Další zpracování začne {nextFireTime}");
                }
            }
            catch 
            {
                // Pokud nastane chyba při běhu jobu, tak jí Quartz spolkne, bez toho aby dal někomu vědět. 
                // Proto v případě zachycení kritické výjimky při zpracování chci informaci zalogovat a vynutit si vypnutí jobu.
                await context.Scheduler.DeleteJob(context.JobDetail.Key);
            }
        }

        protected MigrationJobData GetJobData(IJobExecutionContext context)
        {
            return new MigrationJobData(context.MergedJobDataMap);
        }

        private bool IsLastTriggerFire(IJobExecutionContext context)
        {
            if (context.PreviousFireTimeUtc == null)
                return false;

            if (context.NextFireTimeUtc == null)
                return true;

            var previousFire = context.FireTimeUtc - context.PreviousFireTimeUtc;
            previousFire = new TimeSpan((long)(previousFire.Value.Ticks * 1.1));

            var nextTimeSpan = context.NextFireTimeUtc - context.FireTimeUtc;

            return nextTimeSpan > previousFire;
        }
    }
}