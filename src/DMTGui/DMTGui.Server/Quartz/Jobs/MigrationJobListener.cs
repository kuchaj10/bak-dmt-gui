﻿//using DMTGui.Shared.Extensions;
//using Quartz;
//using Microsoft.EntityFrameworkCore;
//using ICZ.Sps.DMT.DMTDAL.App;

//namespace DMTGui.Server.Quartz.Jobs
//{
//    public class MigrationJobListener : IJobListener
//    {
//        private readonly IDbContextFactory<AppDbContext> _dbContextFactory;

//        public MigrationJobListener(IDbContextFactory<AppDbContext> contextFactory)
//        {
//            _dbContextFactory = contextFactory;
//        }

//        public string Name => nameof(MigrationJobListener);

//        public Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
//        {
//            return Task.CompletedTask;
//        }

//        public Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
//        {
//            var jobKey = context.JobDetail.Key;
//            var jobId = GetIdFromJobKey(jobKey);

//            using var dbContext = _dbContextFactory.CreateDbContext();

//            var job = dbContext.Jobs.FirstOrDefault(job => job.Id == jobId);
//            if (job is not null)
//            {
//                job.IsRunning = true;
//                dbContext.Update(job);
//                dbContext.SaveChanges();
//            }

//            return Task.CompletedTask;
//        }

//        public Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default)
//        {
//            var jobKey = context.JobDetail.Key;
//            var jobId = GetIdFromJobKey(jobKey);

//            using var dbContext = _dbContextFactory.CreateDbContext();

//            var job = dbContext.Jobs.FirstOrDefault(job => job.Id == jobId);
//            if (job is null)
//                return Task.CompletedTask;

//            job.IsRunning = false;

//            if (context.NextFireTimeUtc is null)
//            {
//                job.IsScheduled = false;
//            }

//            dbContext.Update(job);
//            dbContext.SaveChanges();

//            return Task.CompletedTask;
//        }

//        private Guid GetIdFromJobKey(JobKey key)
//        {
//            if (key.Name.Length != 22)
//                return Guid.Empty;

//            var guid = key.Name.ToGuidFromBase64String();
//            return guid;
//        }
//    }
//}
