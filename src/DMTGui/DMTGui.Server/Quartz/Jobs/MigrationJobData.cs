﻿using DMTGui.Dal;
using DMTGui.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Quartz;

namespace DMTGui.Server.Quartz.Jobs
{
    /// <summary>
    /// Třída na udržování dat sloužící s migrační úlohou.
    /// </summary>
    public class MigrationJobData
    {
        const string ProfilesProperty = "ProfilesProperty";
        const string ContextFactoryProperty = "ContextFactoryProperty";
        const string JobIdProperty = "JobIdProperty";
        const string LoggerProperty = "LoggerProperty";

        private readonly JobDataMap _jobData;

        public MigrationJobData(JobDataMap jobData)
        {
            _jobData = jobData ?? throw new ArgumentNullException(nameof(jobData));
        }

        public void SetProfiles(IEnumerable<ProfileDto> profiles)
        {
            if (profiles is null)
                throw new ArgumentNullException(nameof(profiles));

            _jobData.Put(ProfilesProperty, profiles);
        }

        public void SetContextFactory(IDbContextFactory<AppDbContext> contextFactory)
        {
            if (contextFactory is null)
                throw new ArgumentNullException(nameof(contextFactory));

            _jobData.Put(ContextFactoryProperty, contextFactory);
        }

        public void SetJobId(Guid jobId)
        {
            _jobData.Put(JobIdProperty, jobId);
        }

        public void SetLogger(ILogger<MigrationJob> logger)
        {
            if (logger is null)
                throw new ArgumentNullException(nameof(logger));

            _jobData.Put(LoggerProperty, logger);
        }

        public IEnumerable<ProfileDto> GetProfiles()
        {
            return (IEnumerable<ProfileDto>)_jobData.Get(ProfilesProperty);
        }

        public IDbContextFactory<AppDbContext> GetContextFactory()
        {
            return (IDbContextFactory<AppDbContext>)_jobData.Get(ContextFactoryProperty);
        }

        public Guid GetJobId()
        {
            return (Guid)_jobData.Get(JobIdProperty);
        }

        public ILogger<MigrationJob> GetLogger()
        {
            return (ILogger<MigrationJob>)_jobData.Get(LoggerProperty);
        }
    }
}
