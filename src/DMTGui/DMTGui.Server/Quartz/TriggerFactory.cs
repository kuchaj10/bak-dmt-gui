﻿using System;
using Quartz;
using ICZ.Sps.DMT.DMTLib.Exceptions;

namespace DMTGui.Server.Quartz
{
    public static class TriggerFactory
    {
        /// <summary>
        /// Z předaného <paramref name="cronExpression"/> vytvoří nový trigger.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="DMTConfigurationException"></exception>
        public static ITrigger CreateCronTrigger(TriggerKey key, string cronExpression)
        {
            if (string.IsNullOrEmpty(cronExpression))
                throw new ArgumentNullException(nameof(cronExpression));

            if (!CronExpression.IsValidExpression(cronExpression))
                throw new DMTConfigurationException($"Hodnota '{cronExpression}' není validní cron výraz");

            var trigger = TriggerBuilder.Create()
                .WithIdentity(key)
                .WithCronSchedule(cronExpression, x => x.WithMisfireHandlingInstructionFireAndProceed())
                .Build();

            return trigger;
        }
    }
}
