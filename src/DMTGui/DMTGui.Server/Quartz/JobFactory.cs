﻿using DMTGui.Dal;
using DMTGui.Server.Quartz.Jobs;
using DMTGui.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Quartz;

namespace DMTGui.Server.Quartz
{
    public static class JobFactory
    {
        /// <summary>
        /// Vytvoří úlohu migrace, která slouží pro běh v <see cref="Quartz"/>.
        /// </summary>
        public static IJobDetail CreateMigrationJob(JobKey jobKey, IEnumerable<ProfileDto> profiles, IDbContextFactory<AppDbContext> contextFactory, 
            Guid jobId, ILogger<MigrationJob> logger)
        {
            var data = new JobDataMap();
            var migrationData = new MigrationJobData(data);
            migrationData.SetProfiles(profiles);
            migrationData.SetContextFactory(contextFactory);
            migrationData.SetJobId(jobId);
            migrationData.SetLogger(logger);

            var job = JobBuilder.Create<MigrationJob>()
                .WithIdentity(jobKey)
                .SetJobData(data)
                .Build();

            return job;
        }
    }
}
