﻿//using DMTGui.Server.Exceptions;
//using DMTGui.Server.Extensions;
//using DMTGui.Shared.Models;
//using EFCore.BulkExtensions;
//using ICZ.Sps.DMT.DMTDAL.Main;
//using ICZ.Sps.DMT.DMTDAL.Main.Entities;
//using Microsoft.EntityFrameworkCore;

//namespace DMTGui.Server.Services
//{
//    public interface IIdentifierService
//    {
//        IdentifierDto AddNewIdentifier(IdentifierDto identifier);
//        void AddNewIdentifiers(IEnumerable<IdentifierDto> identifiers);
//        void DeleteIdentifier(int id);
//        void DeleteIdentifiers(IEnumerable<int> ids);
//        IdentifierDto GetIdentifierById(int id);
//        IEnumerable<IdentifierDto> GetIdentifiers(int? skip, int? take);
//        IEnumerable<string> GetUsedStates();
//        IdentifierDto UpdateIdentifier(IdentifierDto identifier);
//        void UpdateIdentifiers(IEnumerable<IdentifierDto> identifiers);
//    }

//    public class IdentifierService : IIdentifierService
//    {
//        private readonly IDbContextFactory<DMTContext> _contextFactory;

//        public IdentifierService(IDbContextFactory<DMTContext> contextFactory)
//        {
//            _contextFactory = contextFactory;
//        }

//        public IEnumerable<IdentifierDto> GetIdentifiers(int? skip, int? take)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var identifiers = context.Identifiers
//                .Include(idt => idt.Related)
//                .AsQueryable();

//            if (skip.HasValue)
//                identifiers = identifiers.Skip(skip.Value);

//            if (take.HasValue)
//                identifiers = identifiers.Take(take.Value);

//            return identifiers
//                .Select(idt => idt.ToDto())
//                .ToArray();
//        }

//        public IdentifierDto GetIdentifierById(int id)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var entity = context.Identifiers
//                .Include(idt => idt.Related)
//                .FirstOrDefault(idt => idt.Id == id);

//            if (entity is null)
//                throw new NotFoundException($"Identifikátor {id} nebyl nelezen");

//            return entity.ToDto();
//        }

//        public IEnumerable<string> GetUsedStates()
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var usedStates = context.Identifiers
//                .Select(idt => idt.State)
//                .Distinct()
//                .ToArray();

//            return usedStates.Select(sta => sta.ToString());
//        }

//        public IdentifierDto AddNewIdentifier(IdentifierDto identifier)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var entity = identifier.ToNewEntity();

//            context.Add(entity);
//            context.SaveChanges();

//            return entity.ToDto();
//        }

//        public void AddNewIdentifiers(IEnumerable<IdentifierDto> identifiers)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var entities = identifiers.Select(ent => ent.ToNewEntity()).ToArray();

//            context.BulkInsert(entities);
//            context.SaveChanges();
//        }

//        /// <summary>
//        /// Aktualizuje předaný identifikátor a i ty do něj vložené. Neprovede reorganizaci identifikátoru, 
//        /// přidání nových a ani vymazání neexistujících. Pouze aktualizci informací.
//        /// </summary>
//        public IdentifierDto UpdateIdentifier(IdentifierDto identifier)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var flattenIdentifiers = identifier.Flatten();

//            var entitiesToUpdate = UpdateFlattenIdentifiers(context, flattenIdentifiers).ToArray();

//            context.BulkUpdate(entitiesToUpdate);
//            context.SaveChanges();

//            return context.Identifiers.First(idt => idt.Id == identifier.Id).ToDto();
//        }

//        /// <summary>
//        /// Stejné jako <see cref="UpdateIdentifier(IdentifierDto)"/>, pouze pro větší množství identifikátorů.
//        /// </summary>
//        public void UpdateIdentifiers(IEnumerable<IdentifierDto> identifiers)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var flattenIdentifiers = identifiers.SelectMany(idt => idt.Flatten());

//            var entitiesToUpdate = UpdateFlattenIdentifiers(context, flattenIdentifiers).ToArray();

//            context.BulkUpdate(entitiesToUpdate);
//            context.SaveChanges();
//        }

//        private IEnumerable<IdentifierEntity> UpdateFlattenIdentifiers(DMTContext context, IEnumerable<IdentifierDto> identifiers)
//        {
//            var newIdentifiers = identifiers
//                .Where(idt => !context.Identifiers.Any(ent => ent.Id == idt.Id))
//                .Select(idt => idt.Id.ToString())
//                .ToArray();

//            if (newIdentifiers.Length > 0)
//                throw new WrongOperationException($"V databázi nebyly nalezeny tyto identifikátory {string.Join(", ", newIdentifiers)}");

//            var entitiesToUpdate = new List<IdentifierEntity>();

//            foreach (var dto in identifiers)
//            {
//                var entity = context.Identifiers.First(idt => idt.Id == dto.Id);

//                entity.UpdateEntity(dto);

//                entitiesToUpdate.Add(entity);
//            }

//            return entitiesToUpdate;
//        }

//        public void DeleteIdentifier(int id)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var entity = context.Identifiers.FirstOrDefault(idt => idt.Id == id);
//            if (entity is null)
//                throw new NotFoundException($"Identifikátor {id} nebyl nelezen");

//            context.Remove(entity);
//            context.SaveChanges();
//        }

//        public void DeleteIdentifiers(IEnumerable<int> ids)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var entities = context.Identifiers.Where(ent => ids.Contains(ent.Id)).ToArray();
//            if (!entities.Any())
//                throw new NotFoundException($"Nebyly nalezeny žádné identifikátory k vymazání");

//            context.BulkDelete(entities);
//            context.SaveChanges();
//        }
//    }
//}
