﻿using DMTGui.Shared.Exceptions;
using DMTGui.Server.Extensions;
using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using ICZ.Sps.DMT.DMTDAL.Dictionaries;

namespace DMTGui.Server.Services
{
    public interface IDictionaryService
    {
        /// <summary>
        /// Vrátí všechny slovníky
        /// </summary>
        Task<IEnumerable<DictionaryDto>> GetDictionariesAsync();

        /// <summary>
        /// Vrátí názvy všech slovníků. Slovníky jsou reprezentovány stejně jako v případě 
        /// metody <see cref="GetDictionariesAsync"/>, ale pouze s vyplěnými názvy.
        /// </summary>
        Task<IEnumerable<DictionaryDto>> GetDictionariesNamesAsync();

        /// <summary>
        /// Vrátí slovník podle předaného jména
        /// </summary>
        Task<DictionaryDto> GetDictionaryByNameAsync(string name);

        /// <summary>
        /// Přidá nový slovník
        /// </summary>
        Task<DictionaryDto> AddNewDictionaryAsync(DictionaryDto dictionary);

        /// <summary>
        /// Aktualizuje předaný slovník 
        /// </summary>
        Task<DictionaryDto> UpdateDictionaryAsync(DictionaryDto dictionary);

        /// <summary>
        /// Vymaže slovník podle předaného jména
        /// </summary>
        Task DeleteDictionaryByNameAsync(string name);
    }

    public class DictionaryService : IDictionaryService
    {
        private readonly DictionariesFactory _dictionariesFactory;

        public DictionaryService(DictionariesFactory dictionariesFactory)
        {
            _dictionariesFactory = dictionariesFactory;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<DictionaryDto>> GetDictionariesAsync()
        {
            var task = Task.Run(() =>
            {
                using var context = _dictionariesFactory.GetDictionariesContext();

                var dictionaries = context.GetDictionaries()
                    .Select(dic => dic.ToDto())
                    .ToArray();

                return dictionaries;
            });

            return await task;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<DictionaryDto>> GetDictionariesNamesAsync()
        {
            var task = Task.Run(() =>
            {
                using var context = _dictionariesFactory.GetDictionariesContext();

                var dictionaries = context.GetDictionaries()
                    .Select(dic => dic.ToDtoWithoutEntries())
                    .ToArray();

                return dictionaries;
            });

            return await task;
        }

        /// <inheritdoc/>
        public async Task<DictionaryDto> GetDictionaryByNameAsync(string name)
        {
            name.ThrowIfEmpty();

            var task = Task.Run(() =>
            {
                using var context = _dictionariesFactory.GetDictionariesContext();

                var dictionary = context.GetDictionary(name);
                if (dictionary is null)
                    throw new NotFoundException($"Slovník se jménem '{name}' neexistuje");

                return dictionary.ToDto();
            });

            return await task;

        }

        /// <inheritdoc/>
        public async Task<DictionaryDto> AddNewDictionaryAsync(DictionaryDto dictionary)
        {
            dictionary.ThrowIfNull();

            var task = Task.Run(() =>
            {
                using var context = _dictionariesFactory.GetDictionariesContext();

                if (context.CheckDictionaryExists(dictionary.Name))
                    throw new WrongOperationException($"Slovník {dictionary.Name} už existuje a není možné jej vytvořit.");

                var dictionaryToAdd = dictionary.ToDictionary();

                context.AddOrUpdateDictionary(dictionaryToAdd);

                dictionaryToAdd = context.GetDictionary(dictionary.Name);

                return dictionaryToAdd.ToDto();
            });

            return await task;
        }

        /// <inheritdoc/>
        public async Task<DictionaryDto> UpdateDictionaryAsync(DictionaryDto dictionary)
        {
            dictionary.ThrowIfNull();

            var task = Task.Run(() =>
            {
                using var context = _dictionariesFactory.GetDictionariesContext();

                if (!context.CheckDictionaryExists(dictionary.Name))
                    throw new NotFoundException($"Slovník {dictionary.Name} neexistuje");

                var dictionaryToAdd = dictionary.ToDictionary();

                context.AddOrUpdateDictionary(dictionaryToAdd);

                dictionaryToAdd = context.GetDictionary(dictionary.Name);

                return dictionaryToAdd.ToDto();
            });

            return await task;
        }

        /// <inheritdoc/>
        public async Task DeleteDictionaryByNameAsync(string name)
        {
            name.ThrowIfEmpty();

            var task = Task.Run(() =>
            {
                using var context = _dictionariesFactory.GetDictionariesContext();

                if (!context.CheckDictionaryExists(name))
                    throw new NotFoundException($"Slovník {name} neexistuje");

                context.RemoveDictionary(name);
            });

            await task;
        }
    }
}
