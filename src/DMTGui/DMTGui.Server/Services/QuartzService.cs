﻿using DMTGui.Shared.Extensions;
using Quartz;
using Quartz.Impl;

namespace DMTGui.Server.Services
{
    public interface IQuartzService
    {
        /// <summary>
        /// Vrací příznak toho, jestli úloha s předaným klíčem aktuálně běží.
        /// </summary>
        Task<bool> IsJobRunningAsync(JobKey jobKey);

        /// <summary>
        /// Vrací příznak toho, jestli je úloha s předaným klíčem naplánovaná ke spuštění.
        /// </summary>
        Task<bool> IsJobScheduledAsync(JobKey jobKey);

        /// <summary>
        /// Naplánuje předanou úlohu <paramref name="jobToSchedule"/> s předanými triggery <paramref name="jobTriggers"/>.
        /// </summary>
        Task<IEnumerable<TriggerKey>> ScheduleJobAsync(IJobDetail jobToSchedule, IEnumerable<ITrigger> jobTriggers);

        /// <summary>
        /// Naplánuje předanou úlohu <paramref name="jobToSchedule"/> s předaným triggerem <paramref name="jobTrigger"/>.
        /// </summary>
        Task<TriggerKey> ScheduleJobAsync(IJobDetail jobToSchedule, ITrigger jobTrigger);

        /// <summary>
        /// Okamžitě spustí předanou úlohu.
        /// </summary>
        Task<IEnumerable<TriggerKey>> StartJobAsync(IJobDetail jobToSchedule);

        /// <summary>
        /// Okamžitě spustí předanou úlohu s předanými triggery.
        /// </summary>
        Task<IEnumerable<TriggerKey>> StartJobAsync(IJobDetail jobToSchedule, IEnumerable<ITrigger> jobTriggers);

        /// <summary>
        /// Zastaví předanou úlohu.
        /// </summary>
        Task StopJobAsync(JobKey jobKey);

        /// <summary>
        /// Zruší naplánování předané úlohy.
        /// </summary>
        Task UnscheduleJobAsync(JobKey jobKey);
    }

    /// <summary>
    /// Služba na spuštění, udržování a manipulaci úloh pomocí quartz.
    /// </summary>
    public class QuartzService : IQuartzService
    {
        private readonly ILogger<QuartzService> _logger;

        private readonly IScheduler _scheduler;

        public QuartzService(ILogger<QuartzService> logger)
        {
            _logger = logger;
            _scheduler = StdSchedulerFactory.GetDefaultScheduler().Result;

            if (_scheduler is null)
                throw new Exception("Nastala chyba při konfiguraci Quartz.NET");

            _scheduler.Start();
        }

        /// <<inheritdoc/>
        public async Task<bool> IsJobRunningAsync(JobKey jobKey)
        {
            jobKey.ThrowIfNull();

            var executingJobs = await _scheduler.GetCurrentlyExecutingJobs();

            return executingJobs.Any(job => job.JobDetail.Key == jobKey);
        }

        /// <<inheritdoc/>
        public async Task<bool> IsJobScheduledAsync(JobKey jobKey)
        {
            jobKey.ThrowIfNull();

            var triggerKeys = await _scheduler.GetTriggersOfJob(jobKey);
            if (triggerKeys is null)
                return false;

            var anyTriggerIsScheduled = triggerKeys.Any(trg => trg?.GetNextFireTimeUtc() != null);

            return anyTriggerIsScheduled;
        }

        /// <<inheritdoc/>
        public async Task<TriggerKey> ScheduleJobAsync(IJobDetail jobToSchedule, ITrigger jobTrigger)
        {
            jobToSchedule.ThrowIfNull();
            jobTrigger.ThrowIfNull();

            var triggerKeys = await ScheduleJobAsync(jobToSchedule, new ITrigger[] { jobTrigger });

            return triggerKeys.First();
        }

        /// <<inheritdoc/>
        public async Task<IEnumerable<TriggerKey>> ScheduleJobAsync(IJobDetail jobToSchedule, IEnumerable<ITrigger> jobTriggers)
        {
            jobToSchedule.ThrowIfNull();
            jobTriggers.ThrowIfNull();

            var readOnlyTriggers = (IReadOnlyCollection<ITrigger>)jobTriggers.ToList().AsReadOnly();

            await _scheduler.ScheduleJob(jobToSchedule, readOnlyTriggers, true);

            var trigersKeys = jobTriggers
                .Select(trg => trg.Key)
                .ToArray();

            return trigersKeys;
        }

        //public IEnumerable<TriggerKey> RescheduleJob(IEnumerable<KeyValuePair<TriggerKey, ITrigger>> trigerPairs)
        //{
        //    foreach (var pair in trigerPairs)
        //    {
        //        _scheduler.RescheduleJob(pair.Key, pair.Value);
        //    }

        //    var trigersKeys = trigerPairs
        //        .Select(par => par.Value.Key)
        //        .ToArray();

        //    return trigersKeys;
        //}

        /// <<inheritdoc/>
        public async Task<IEnumerable<TriggerKey>> StartJobAsync(IJobDetail jobToSchedule)
        {
            jobToSchedule.ThrowIfNull();

            return await StartJobAsync(jobToSchedule, new ITrigger[0]);
        }

        /// <<inheritdoc/>
        public async Task<IEnumerable<TriggerKey>> StartJobAsync(IJobDetail jobToSchedule, IEnumerable<ITrigger> jobTriggers)
        {
            jobToSchedule.ThrowIfNull();
            jobTriggers.ThrowIfNull();

            var randomName = Path.ChangeExtension(Path.GetRandomFileName(), null);
            var startNowJobTrigger = TriggerBuilder.Create()
                .WithIdentity($"trigger-{randomName}", "Jobs")
                .StartNow()
                .Build();

            jobTriggers = jobTriggers.Append(startNowJobTrigger);

            return await ScheduleJobAsync(jobToSchedule, jobTriggers);
        }

        /// <<inheritdoc/>
        public async Task StopJobAsync(JobKey jobKey)
        {
            jobKey.ThrowIfNull();

            await _scheduler.Interrupt(jobKey);
        }

        /// <<inheritdoc/>
        public async Task UnscheduleJobAsync(JobKey jobKey)
        {
            jobKey.ThrowIfNull();

            var triggers = await _scheduler.GetTriggersOfJob(jobKey);

            await _scheduler.Interrupt(jobKey);

            foreach (var trigger in triggers)
            {
                await _scheduler.UnscheduleJob(trigger.Key);
            }
        }
    }
}
