﻿using DMTGui.Dal;
using DMTGui.Server.Extensions;
using DMTGui.Shared.Exceptions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Main;
using ICZ.Sps.DMT.DMTDAL.Main.Enums;
using Microsoft.EntityFrameworkCore;

namespace DMTGui.Server.Services
{
    public interface IStatisticsService
    {
        public Task<MigrationStatisticsDto> GetJobStatisticsAsync(Guid jobId);

        public Task<MigrationStatisticsDto[]> GetAllJobsStatisticsAsync(StatisticsSearchDto search);
    }

    public class StatisticsService : IStatisticsService
    {
        private readonly IDbContextFactory<AppDbContext> _appContextFactory;
        private readonly IDbContextFactory<DMTContext> _dmtContextFactory;

        public StatisticsService(IDbContextFactory<AppDbContext> appContextFactory, IDbContextFactory<DMTContext> dmtContextFactory)
        {
            _appContextFactory = appContextFactory;
            _dmtContextFactory = dmtContextFactory;
        }

        public async Task<MigrationStatisticsDto[]> GetAllJobsStatisticsAsync(StatisticsSearchDto search)
        {
            using var dmtContext = _dmtContextFactory.CreateDbContext();

            var query = dmtContext.MigrationRecords.AsQueryable();
            
            if (search.StartDate.HasValue)
                query = query.Where(rec => rec.StartActionDate.Date >= search.StartDate);

            if (search.EndDate.HasValue)
                query = query.Where(rec => rec.StartActionDate.Date <= search.EndDate);

            var jobIds = await query
                .Where(rec => !string.IsNullOrEmpty(rec.SessionId))
                .Select(rec => rec.SessionId)
                .Distinct()
                .ToArrayAsync();

            using var appContext = _appContextFactory.CreateDbContext();

            var statistics = new List<MigrationStatisticsDto>();
            foreach (var jobId in jobIds)
            {
                if (!jobId.CanConvertToBase64())
                    continue;

                var jobGuid = jobId.ToGuidFromBase64String();
                var job = await appContext.Jobs.SingleOrDefaultAsync(job => job.Id == jobGuid);
                if (job is null)
                    continue;

                var secondQuery = dmtContext.MigrationRecords
                    .Where(rec => rec.SessionId == jobId);

                if (search.StartDate.HasValue)
                    secondQuery = secondQuery.Where(rec => rec.StartActionDate.Date >= search.StartDate);

                if (search.EndDate.HasValue)
                    secondQuery = secondQuery.Where(rec => rec.StartActionDate.Date <= search.EndDate);

                var records = await secondQuery.ToArrayAsync();

                var days = records
                    .GroupBy(rec => rec.StartActionDate.Date)
                    .Select(rec => rec.GetStatisticsDay())
                    .ToArray();

                var stats = new MigrationStatisticsDto
                {
                    MigrationId = jobId,
                    MigrationName = job.Name,
                    Days = days
                };

                statistics.Add(stats);
            }

            return statistics.ToArray();
        }

        public async Task<MigrationStatisticsDto> GetJobStatisticsAsync(Guid jobId)
        {
            using var appContext = _appContextFactory.CreateDbContext();

            var jobIdBase64 = jobId.ToBase64String();

            var job = await appContext.Jobs.SingleOrDefaultAsync(job => job.Id == jobId);
            if (job is null)
                throw new NotFoundException($"Úloha s identifikátorem '{jobIdBase64}' nebyla nalezena");

            using var dmtContext = _dmtContextFactory.CreateDbContext();

            var records = await dmtContext.MigrationRecords
                .Where(rec => rec.SessionId == jobIdBase64)
                .ToListAsync();

            var days = records
                .GroupBy(rec => rec.StartActionDate.Date)
                .Select(rec => rec.GetStatisticsDay())
                .ToArray();

            return new MigrationStatisticsDto
            {
                MigrationId = jobIdBase64,
                MigrationName = job.Name,
                Days = days
            };
        }
    }
}
