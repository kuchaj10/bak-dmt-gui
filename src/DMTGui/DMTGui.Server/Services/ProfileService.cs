﻿using DMTGui.Shared.Exceptions;
using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using Microsoft.EntityFrameworkCore;
using DMTGui.Dal;
using DMTGui.Dal.Entities;
using DMTGui.Server.Extensions;

namespace DMTGui.Server.Services
{
    public interface IProfileService
    {
        /// <summary>
        /// Vrátí všechny profily
        /// </summary>
        Task<IEnumerable<ProfileDto>> GetProfilesAsync();

        /// <summary>
        /// Vrátí profil podle předaného guid profilu
        /// </summary>
        Task<ProfileDto> GetProfileAsync(Guid profileId);

        /// <summary>
        /// Vrátí seznam profilů podle předaného guid úlohy.
        /// </summary>
        Task<IEnumerable<ProfileDto>> GetProfilesByJobIdAsync(Guid jobId);

        /// <summary>
        /// Přidá nový profil. 
        /// </summary>
        Task<ProfileDto> AddProfileAsync(ProfileDto profile);

        /// <summary>
        /// Aktualizuje profil
        /// </summary>
        Task UpdateProfileAsync(ProfileDto profile);

        /// <summary>
        /// Smaže profil podle předaného guid.
        /// </summary>
        Task DeleteProfileAsync(Guid profileId);
    }

    /// <summary>
    /// Služba na manipulaci s profily.
    /// </summary>
    public class ProfileService : IProfileService
    {
        private readonly IDbContextFactory<AppDbContext> _contextFactory;

        public ProfileService(IDbContextFactory<AppDbContext> contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <<inheritdoc/>
        public async Task<IEnumerable<ProfileDto>> GetProfilesAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            var profiles = await context.Profiles
                .Include(ent => ent.Job)
                .Select(ent => ent.ToProfileWithoutData())
                .ToArrayAsync();

            return profiles;
        }

        /// <<inheritdoc/>
        public async Task<IEnumerable<ProfileDto>> GetProfilesByJobIdAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();

            var profiles = await context.Profiles
                .Include(ent => ent.Job)
                .Where(ent => ent.JobId == jobId)
                .Select(prf => prf.ToProfileWithoutData())
                .ToArrayAsync();

            return profiles;
        }

        /// <<inheritdoc/>
        public async Task<ProfileDto> GetProfileAsync(Guid profileId)
        {
            using var context = _contextFactory.CreateDbContext();

            var profile = await context.Profiles
                .Include(ent => ent.Job)
                .FirstOrDefaultAsync(prf => prf.Id == profileId);

            if (profile is null)
                throw new NotFoundException();

            return profile.ToProfile();
        }

        /// <<inheritdoc/>
        public async Task<ProfileDto> AddProfileAsync(ProfileDto profile)
        {
            profile.ThrowIfNull();

            using var context = _contextFactory.CreateDbContext();

            var exists = await context.Profiles.AnyAsync(prf => prf.Name == profile.Name);
            if (exists)
                throw new WrongOperationException($"Již existuje profil s názvem {profile.Name}");

            var profileEntity = new ProfileEntity
            {
                Name = profile.Name,
                Description = null,
                Data = profile.Data,
                Created = DateTime.Now,
                IsValid = false,
            };

            context.Add(profileEntity);
            await context.SaveChangesAsync();

            return profileEntity.ToProfile();
        }

        /// <<inheritdoc/>
        public async Task DeleteProfileAsync(Guid profileId)
        {
            using var context = _contextFactory.CreateDbContext();

            var profileToDelete = await context.Profiles.FirstOrDefaultAsync(prf => prf.Id == profileId);
            if (profileToDelete is null)
                throw new NotFoundException($"Profil s 'Id={profileId}' nebyl nalezen.");

            context.Remove(profileToDelete);
            await context.SaveChangesAsync();
        }

        /// <<inheritdoc/>
        public async Task UpdateProfileAsync(ProfileDto profile)
        {
            profile.ThrowIfNull();

            using var context = _contextFactory.CreateDbContext();

            var profileId = profile.Id.ToGuidFromBase64String();

            var profileToUpdate = await context.Profiles.FirstOrDefaultAsync(prf => prf.Id == profileId);
            if (profileToUpdate is null)
                throw new NotFoundException($"Neexistuje profil s 'Id={profileId}'");

            profileToUpdate.Name = profile.Name;
            profileToUpdate.Description = null;
            profileToUpdate.Data = profile.Data;
            profileToUpdate.Updated = DateTime.Now;
            profileToUpdate.IsValid = false;

            context.Update(profileToUpdate);
            await context.SaveChangesAsync();
        }
    }
}
