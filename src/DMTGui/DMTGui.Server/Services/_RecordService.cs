﻿//using DMTGui.Server.Extensions;
//using DMTGui.Shared.Extensions;
//using DMTGui.Shared.Models;
//using ICZ.Sps.DMT.DMTDAL.Main;
//using Microsoft.EntityFrameworkCore;

//namespace DMTGui.Server.Services
//{
//    public interface IRecordService
//    {
//        IEnumerable<RecordDto> GetRecords(RecordSearchDto search);
//        IEnumerable<string> GetUsedStates();
//    }

//    public class RecordService : IRecordService
//    {
//        private readonly ILogger<RecordService> _logger;

//        private readonly IDbContextFactory<DMTContext> _contextFactory;

//        private readonly IProfileService _profileService;

//        public RecordService(ILogger<RecordService> logger, IDbContextFactory<DMTContext> contextFactory, IProfileService profileService)
//        {
//            _logger = logger;
//            _contextFactory = contextFactory;
//            _profileService = profileService;
//        }
//        public IEnumerable<RecordDto> GetRecords(RecordSearchDto search)
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var query = context.MigrationRecords
//                .Include(ent => ent.MigrationStateEntity)
//                .AsQueryable();

//            if (!string.IsNullOrEmpty(search?.ProfileName))
//                query = query.Where(ent => ent.ProfileName == search.ProfileName);

//            if (!string.IsNullOrEmpty(search?.JobId))
//            {
//                var jobIdAsGuid = search.JobId.ToGuidFromBase64String();
//                var profiles = _profileService.GetProfilesByJobId(jobIdAsGuid);
//                var profileNames = profiles.Select(prf => prf.Name).ToArray();

//                query = query.Where(ent => profileNames.Contains(ent.ProfileName));
//            }

//            if (search?.Skip.HasValue ?? false)
//                query = query.Skip(search.Skip.Value);

//            if (search?.Take.HasValue ?? false)
//                query = query.Skip(search.Take.Value);

//            var records = query
//                .Select(ent => ent.ToRecord())
//                .ToArray();

//            return records;
//        }

//        public IEnumerable<string> GetUsedStates()
//        {
//            using var context = _contextFactory.CreateDbContext();

//            var usedStates = context.MigrationRecords
//                .Select(rec => rec.MigrationState_Id)
//                .Distinct()
//                .ToArray();

//            return usedStates.Select(sta => sta.ToString());
//        }

//        //public IEnumerable<Record> GetRecords(int? skip, int? take)
//        //{
//        //    using var context = _contextFactory.CreateDbContext();

//        //    var records = context.MigrationRecords
//        //        .Include(ent => ent.MigrationStateEntity)
//        //        .AsQueryable()
//        //        .Select(ent => ent.ToRecord())
//        //        .ToArray();

//        //    return records;
//        //}

//        //public IEnumerable<Record> GetRecordsByProfileName(string profileName, int? skip, int? take)
//        //{
//        //    using var context = _contextFactory.CreateDbContext();

//        //    var records = context.MigrationRecords
//        //        .Include(ent => ent.MigrationStateEntity)
//        //        .Where(ent => ent.ProfileName == profileName)
//        //        .Select(ent => ent.ToRecord())
//        //        .ToArray();

//        //    return records;
//        //}

//        //public IEnumerable<Record> GetRecordsByProfileNames(string[] profiles, int? skip, int? take)
//        //{
//        //    using var context = _contextFactory.CreateDbContext();

//        //    var records = context.MigrationRecords
//        //        .Include(ent => ent.MigrationStateEntity)
//        //        .Where(ent => profiles.Contains(ent.ProfileName))
//        //        .Select(ent => ent.ToRecord())
//        //        .ToArray();

//        //    return records;
//        //}

//        //public IEnumerable<Record> GetRecordsByJobId(Guid jobId, int? skip, int? take)
//        //{
//        //    var profiles = _profileService.GetProfilesByJobId(jobId);
//        //    var profileNames = profiles.Select(prf => prf.Name).ToArray();

//        //    return GetRecordsByProfileNames(profileNames);
//        //}
//    }
//}
