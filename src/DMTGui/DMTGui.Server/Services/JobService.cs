﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using Microsoft.EntityFrameworkCore;
using DMTGui.Server.Extensions;
using DMTGui.Shared.Exceptions;
using DMTGui.Dal;
using DMTGui.Dal.Entities;

namespace DMTGui.Server.Services
{
    public interface IJobService
    {
        /// <summary>
        /// Vrátí všechny úlohy
        /// </summary>
        Task<IEnumerable<JobDto>> GetJobsAsync();

        /// <summary>
        /// Vrátí úlohu podle předaného guid
        /// </summary>
        Task<JobDto> GetJobAsync(Guid jobId);

        /// <summary>
        /// Přidá novou úlohu
        /// </summary>
        Task<JobDto> AddJobAsync(JobDto job);

        /// <summary>
        /// Aktualizuje předanou úlohu
        /// </summary>
        Task<JobDto> UpdateJobAsync(JobDto job);

        /// <summary>
        /// Vymaže předanou úlogu podle guid.
        /// </summary>
        Task DeleteJobAsync(Guid jobId);
    }

    public class JobService : IJobService
    {
        private readonly IDbContextFactory<AppDbContext> _contextFactory;

        public JobService(IDbContextFactory<AppDbContext> contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<JobDto>> GetJobsAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            var jobs = await context.Jobs
                .Include(job => job.Profiles)
                .Include(job => job.Triggers)
                .Select(job => job.ToJob())
                .ToArrayAsync();

            return jobs;
        }

        /// <inheritdoc/>
        public async Task<JobDto> GetJobAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();

            var job = await context.Jobs
                .Include(job => job.Profiles)
                .Include(job => job.Triggers)
                .FirstOrDefaultAsync(job => job.Id == jobId);

            return job?.ToJob();
        }

        /// <inheritdoc/>
        public async Task<JobDto> AddJobAsync(JobDto job)
        {
            job.ThrowIfNull();

            using var context = _contextFactory.CreateDbContext();
            using var transaction = await context.Database.BeginTransactionAsync();

            try
            {
                var jobEntity = new JobEntity
                {
                    Description = job.Description,
                    Name = job.Name,
                    IsEnabled = false,
                    IsRunning = false,
                    LastRun = null,
                    Created = DateTime.Now,
                };

                context.Add(jobEntity);
                await context.SaveChangesAsync();

                var jobId = jobEntity.Id;

                await AddProfilesToJob(context, jobId, job.Profiles);
                AddTriggersToJob(context, jobId, job.Triggers);
                await context.SaveChangesAsync();

                await transaction.CommitAsync();

                return jobEntity.ToJob();
            }
            catch
            {
                await transaction.RollbackAsync();

                throw;
            }
        }

        /// <inheritdoc/>
        public async Task DeleteJobAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();
            using var transaction = await context.Database.BeginTransactionAsync();

            try
            {
                var jobToDelete = await context.Jobs.FirstOrDefaultAsync(job => job.Id == jobId);
                if (jobToDelete is null)
                    throw new NotFoundException($"Úloha s 'Id={jobId}' nebyla nalezena");

                if (jobToDelete.IsEnabled)
                    throw new WrongOperationException("Není možné smazat aktivní úlohy");

                ClearProfilesFromJob(context, jobId);
                await DeleteTriggersFromJob(context, jobId);
                context.Remove(jobToDelete);

                await context.SaveChangesAsync();
                await transaction.CommitAsync();
            }
            catch
            {
                await transaction.RollbackAsync();

                throw;
            }
        }

        /// <inheritdoc/>
        public async Task<JobDto> UpdateJobAsync(JobDto job)
        {
            job.ThrowIfNull();

            using var context = _contextFactory.CreateDbContext();
            using var transaction = await context.Database.BeginTransactionAsync();

            var jobId = job.Id.ToGuidFromBase64String();

            try
            {
                var jobEntity = await context.Jobs.FirstOrDefaultAsync(job => job.Id == jobId);
                if (jobEntity is null)
                    throw new NotFoundException($"Úloha s 'Id={jobId}' nebyla nalezena");

                if (jobEntity.IsEnabled)
                    throw new WrongOperationException("Není možné aktualizovat aktivní úlohy");

                await UpdateProfilesOnJob(context, jobId, job.Profiles);
                await UpdateTriggersOnJob(context, jobId, job.Triggers);

                jobEntity.Description = job.Description;
                jobEntity.Name = job.Name;

                context.Update(jobEntity);
                await context.SaveChangesAsync();

                await transaction.CommitAsync();

                return jobEntity.ToJob();
            }
            catch
            {
                await transaction.RollbackAsync();
                throw;
            }
        }

        private static async Task UpdateProfilesOnJob(AppDbContext context, Guid jobId, ProfileLinkDto[] link)
        {
            ClearProfilesFromJob(context, jobId);
            await AddProfilesToJob(context, jobId, link);
        }

        private static void ClearProfilesFromJob(AppDbContext context, Guid jobId)
        {
            var profilesToUpdate = context.Profiles
                .Where(prf => prf.JobId == jobId)
                .ToList();

            profilesToUpdate.ForEach(prf => prf.JobId = null);

            context.UpdateRange(profilesToUpdate);
        }

        private static async Task AddProfilesToJob(AppDbContext context, Guid jobId, ProfileLinkDto[] links)
        {
            if (links is null)
                return;

            var profileEntities = new List<ProfileEntity>();

            var order = 1;
            foreach (var link in links)
            {
                var profileId = link.ProfileId.ToGuidFromBase64String();
                var profileEntity = await context.Profiles.FirstOrDefaultAsync(prf => prf.Id == profileId);

                if (profileEntity is null)
                    throw new NotFoundException($"V databazi nebyl nalezen profil '{link.ProfileName}'");

                profileEntities.Add(profileEntity);
                profileEntity.JobId = jobId;
                profileEntity.JobStepOrder = order;

                order++;
            }

            context.UpdateRange(profileEntities);
        }

        private static async Task UpdateTriggersOnJob(AppDbContext context, Guid jobId, TriggerDto[] triggers)
        {
            await DeleteTriggersFromJob(context, jobId);
            AddTriggersToJob(context, jobId, triggers);
        }

        private static async Task DeleteTriggersFromJob(AppDbContext context, Guid jobId)
        {
            var triggersToDelete = await context.Triggers
                .Where(trg => trg.JobId == jobId)
                .ToListAsync();

            if (triggersToDelete.Any())
            {
                context.Triggers.RemoveRange(triggersToDelete);
            }
        }

        private static void AddTriggersToJob(AppDbContext context, Guid jobId, TriggerDto[] triggers)
        {
            if (triggers is null)
                return;

            var triggerEntities = triggers
                .Select(trg => new TriggerEntity { JobId = jobId, Settings = trg.Settings, Description = trg.Description })
                .ToArray();

            context.Triggers.AddRange(triggerEntities);
        }
    }
}
