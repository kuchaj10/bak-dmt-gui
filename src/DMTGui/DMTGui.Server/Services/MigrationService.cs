﻿using DMTGui.Shared.Exceptions;
using DMTGui.Shared.Extensions;
using DMTGui.Server.Quartz.Jobs;
using Quartz;
using Microsoft.EntityFrameworkCore;
using DMTGui.Dal;
using DMTGui.Dal.Entities;
using DMTGui.Server.Extensions;

namespace DMTGui.Server.Services
{
    public interface IMigrationService
    {
        /// <summary>
        /// Provede inicializaci migrací. Všechny aktivní úlohy, které mají běžet naplánuje.
        /// Provádí se při spuštění aplikaci. 
        /// </summary>
        Task InitializeAsync();

        /// <summary>
        /// Aktivuje migraci podle guidu úlohy
        /// </summary>
        Task EnableMigrationAsync(Guid jobId);

        /// <summary>
        /// Vypne migraci podle guidu úlohy
        /// </summary>
        Task DisableMigrationAsync(Guid jobId);

        /// <summary>
        /// Spustí migraci podle předaného guid úlohy
        /// </summary>
        Task RunMigrationAsync(Guid jobId);

        /// <summary>
        /// Zastaví migraci podle předaného guid úlohy
        /// </summary>
        Task StopMigrationAsync(Guid jobId);
    }

    /// <summary>
    /// Služba pro manipulaci s migračními úlohami
    /// </summary>
    public class MigrationService : IMigrationService
    {
        private readonly ILogger<MigrationService> _logger;
        private readonly IQuartzService _quartzService;
        private readonly IDbContextFactory<AppDbContext> _contextFactory;
        private readonly IServiceProvider _serviceProvider;

        public MigrationService(ILogger<MigrationService> logger, IQuartzService quartzService, IDbContextFactory<AppDbContext> contextFactory, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _quartzService = quartzService;
            _contextFactory = contextFactory;
            _serviceProvider = serviceProvider;
        }

        /// <inheritdoc/>
        public async Task InitializeAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            // Nejdříve vynuluji předchozí stavy
            var jobs = await context.Jobs.ToListAsync();
            jobs.ForEach(job =>
            {
                job.IsRunning = false;
                job.IsScheduled = false;
                job.QueueSize = 0;
                job.ActualQueueSize = 0;
            });

            context.UpdateRange(jobs);
            await context.SaveChangesAsync();

            // Poté nahodím všechny joby, které mají být funkční
            var jobIdsToRun = context.Jobs
                .Where(job => job.IsEnabled)
                .Select(job => job.Id)
                .ToArray();

            foreach (var jobId in jobIdsToRun)
            {
                await EnableMigrationAsync(jobId);
            }
        }

        /// <inheritdoc/>
        public async Task EnableMigrationAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();

            var jobEntity = await context.Jobs
                .Include(ent => ent.Triggers)
                .Include(ent => ent.Profiles)
                .FirstOrDefaultAsync(ent => ent.Id == jobId);

            if (jobEntity is null)
                throw new NotFoundException($"Nepodařilo se najít job {jobId}");

            var jobDetails = CreateJob(jobEntity);
            var jobTriggers = CreateTriggers(jobEntity);

            if (jobTriggers.Any())
            {
                await _quartzService.ScheduleJobAsync(jobDetails, jobTriggers);
                jobEntity.IsScheduled = true;
            }

            jobEntity.IsEnabled = true;

            context.Update(jobEntity);
            await context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task DisableMigrationAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();

            var jobEntity = await context.Jobs
                .FirstOrDefaultAsync(ent => ent.Id == jobId);

            if (jobEntity is null)
                throw new NotFoundException($"Nepodařilo se najít job {jobId}");

            var jobKey = CreateJobKey(jobEntity.Id);
            await _quartzService.UnscheduleJobAsync(jobKey);

            jobEntity.IsEnabled = false;
            jobEntity.IsScheduled = false;
            context.Update(jobEntity);
            await context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task RunMigrationAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();

            var jobEntity = await context.Jobs
                .Include(ent => ent.Triggers)
                .Include(ent => ent.Profiles)
                .FirstOrDefaultAsync(ent => ent.Id == jobId);

            if (jobEntity is null)
                throw new NotFoundException($"Nepodařilo se najít job {jobId}");

            var jobDetails = CreateJob(jobEntity);
            var jobTriggers = CreateTriggers(jobEntity);

            await _quartzService.StartJobAsync(jobDetails, jobTriggers);

            jobEntity.IsEnabled = true;
            jobEntity.IsScheduled = true;

            context.Update(jobEntity);
            await context.SaveChangesAsync();
        }

        /// <inheritdoc/>
        public async Task StopMigrationAsync(Guid jobId)
        {
            using var context = _contextFactory.CreateDbContext();

            var jobEntity = await context.Jobs
                .Include(job => job.Triggers)
                .FirstOrDefaultAsync(ent => ent.Id == jobId);

            if (jobEntity is null)
                throw new NotFoundException($"Nepodařilo se najít job {jobId}");

            var jobKey = CreateJobKey(jobEntity.Id);
            await _quartzService.StopJobAsync(jobKey);

            //jobEntity.IsScheduled = true;
            //var jobTriggers = CreateTriggers(jobEntity);
            //jobEntity.IsScheduled = jobTriggers.Count() > 0;

            context.Update(jobEntity);
            await context.SaveChangesAsync();
        }

        private static JobKey CreateJobKey(Guid jobId)
        {
            return new JobKey(jobId.ToBase64String());
        }

        private IJobDetail CreateJob(JobEntity jobEntity)
        {
            jobEntity.ThrowIfNull();
            jobEntity.Profiles.ThrowIfNull();

            var jobKey = CreateJobKey(jobEntity.Id);

            var profiles = jobEntity.Profiles
                .Where(prf => !string.IsNullOrEmpty(prf.Data))
                .Select(prf => prf.ToProfile())
                .ToArray();

            var logger = _serviceProvider.GetService<ILogger<MigrationJob>>();

            var jobDetails = Quartz.JobFactory.CreateMigrationJob(jobKey, profiles, _contextFactory, jobEntity.Id, logger);

            return jobDetails;
        }

        private static IEnumerable<ITrigger> CreateTriggers(JobEntity jobEntity)
        {
            jobEntity.ThrowIfNull();
            jobEntity.Triggers.ThrowIfNull();

            var nonEmptyTriggers = jobEntity.Triggers
                .Where(trg => !string.IsNullOrEmpty(trg.Settings))
                .ToArray();

            var triggers = new List<ITrigger>();

            foreach (var trigger in nonEmptyTriggers)
            {
                var triggerId = trigger.Id.ToBase64String();
                var triggerKey = new TriggerKey(triggerId);

                var createdTrigger = Quartz.TriggerFactory.CreateCronTrigger(triggerKey, trigger.Settings);

                triggers.Add(createdTrigger);
            }

            return triggers.ToArray();
        }
    }
}
