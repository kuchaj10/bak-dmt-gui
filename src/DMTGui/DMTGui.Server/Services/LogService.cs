﻿using DMTGui.Server.Extensions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Main;
using ICZ.Sps.DMT.DMTDAL.Main.Entities;
using ICZ.Sps.DMT.DMTDAL.Main.Enums;
using Microsoft.EntityFrameworkCore;

namespace DMTGui.Server.Services
{
    public interface ILogService
    {
        /// <summary>
        /// Vrátí všechny použité úrovně chybových hlášek,
        /// </summary>
        Task<IEnumerable<string>> GetLogLevelsAsync();

        /// <summary>
        /// Vrátí všechny chybové hlášky
        /// </summary>
        Task<IEnumerable<LogDto>> GetLogsAsync();

        /// <summary>
        /// Vrátí všechny chybové hlášky podle předaného filtru.
        /// </summary>
        Task<IEnumerable<LogDto>> GetLogsAsync(LogSearchDto logSearch);

        /// <summary>
        /// Vrátí počet všech chybových hlášek
        /// </summary>
        Task<int> GetLogsCountAsync();

        /// <summary>
        /// Vrátí počet všech chybových hlášek podle předaného filtru.
        /// </summary>
        Task<int> GetLogsCountAsync(LogSearchDto logSearch);

        /// <summary>
        /// Vrátí seznam všech použitých stavů migrace.
        /// </summary>
        Task<IEnumerable<string>> GetMigrationStatesAsync();
    }

    /// <summary>
    /// Služba na manipulaci s <see cref="LogService"/>
    /// </summary>
    public class LogService : ILogService
    {
        private readonly ILogger<LogService> _logger;

        private readonly IDbContextFactory<DMTContext> _contextFactory;

        public LogService(ILogger<LogService> logger, IDbContextFactory<DMTContext> contextFactory)
        {
            _logger = logger;
            _contextFactory = contextFactory;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<LogDto>> GetLogsAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            var logs = await context.Logs
                .Include(ent => ent.MigrationRecordEntity)
                .Select(ent => ent.ToLog())
                .ToArrayAsync();

            return logs;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<LogDto>> GetLogsAsync(LogSearchDto logSearch)
        {
            logSearch.ThrowIfNull();

            using var context = _contextFactory.CreateDbContext();

            var query = context.Logs
                .Include(ent => ent.MigrationRecordEntity)
                .AsQueryable();

            query = BuildSearchQuery(logSearch, query);
            query = ResolveSortQuery(logSearch, query);
            query = ResolveCutoutQuery(logSearch, query);

            var logs = await query
                .Select(ent => ent.ToLog())
                .ToArrayAsync();

            return logs;
        }

        /// <inheritdoc/>
        public async Task<int> GetLogsCountAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            var count = await context.Logs.CountAsync();

            return count;
        }

        /// <inheritdoc/>
        public async Task<int> GetLogsCountAsync(LogSearchDto logSearch)
        {
            logSearch.ThrowIfNull();

            using var context = _contextFactory.CreateDbContext();

            var query = context.Logs
                .Include(ent => ent.MigrationRecordEntity)
                .AsQueryable();

            query = BuildSearchQuery(logSearch, query);

            var count = await query.CountAsync();

            return count;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<string>> GetMigrationStatesAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            var migrationStates = await context.Logs
                .Include(ent => ent.MigrationRecordEntity)
                .Where(ent => ent.MigrationRecordEntity != null)
                .Select(ent => ent.MigrationRecordEntity.MigrationState_Id)
                .Distinct()
                .ToArrayAsync();

            return migrationStates.Select(ent => ent.ToString());
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<string>> GetLogLevelsAsync()
        {
            using var context = _contextFactory.CreateDbContext();

            var migrationStates = await context.Logs
                .Select(ent => ent.Level)
                .Distinct()
                .ToArrayAsync();

            return migrationStates;
        }

        private static IQueryable<LogEntity> ResolveSortQuery(LogSearchDto logSearch, IQueryable<LogEntity> query)
        {
            query = logSearch switch
            {
                { SortColumn: LogSearchSortColumn.Id, IsSortAscending: true } => query.OrderBy(ent => ent.Id),
                { SortColumn: LogSearchSortColumn.Id, IsSortAscending: false } => query.OrderByDescending(ent => ent.Id),

                { SortColumn: LogSearchSortColumn.JobId, IsSortAscending: true } => query.OrderBy(ent => ent.MigrationRecordEntity.SessionId),
                { SortColumn: LogSearchSortColumn.JobId, IsSortAscending: false } => query.OrderByDescending(ent => ent.MigrationRecordEntity.SessionId),

                { SortColumn: LogSearchSortColumn.ProfileName, IsSortAscending: true } => query.OrderBy(ent => ent.MigrationRecordEntity.ProfileName),
                { SortColumn: LogSearchSortColumn.ProfileName, IsSortAscending: false } => query.OrderByDescending(ent => ent.MigrationRecordEntity.ProfileName),

                { SortColumn: LogSearchSortColumn.ObjectId, IsSortAscending: true } => query.OrderBy(ent => ent.MigrationRecordEntity.ObjectId),
                { SortColumn: LogSearchSortColumn.ObjectId, IsSortAscending: false } => query.OrderByDescending(ent => ent.MigrationRecordEntity.ObjectId),

                //{ SortColumn: LogSearchSortColumn.Migration_Id, IsSortAscending: true } => query.OrderBy(ent => ent.MigrationRecordEntity.ObjectId),
                //{ SortColumn: LogSearchSortColumn.Migration_Id, IsSortAscending: false } => query.OrderByDescending(ent => ent.MigrationRecordEntity.ObjectId),

                { SortColumn: LogSearchSortColumn.MigrationRecordState, IsSortAscending: true } => query.OrderBy(ent => ent.MigrationRecordEntity.MigrationState_Id),
                { SortColumn: LogSearchSortColumn.MigrationRecordState, IsSortAscending: false } => query.OrderByDescending(ent => ent.MigrationRecordEntity.MigrationState_Id),

                { SortColumn: LogSearchSortColumn.Date, IsSortAscending: true } => query.OrderBy(ent => ent.Date),
                { SortColumn: LogSearchSortColumn.Date, IsSortAscending: false } => query.OrderByDescending(ent => ent.Date),

                { SortColumn: LogSearchSortColumn.Thread, IsSortAscending: true } => query.OrderBy(ent => ent.Thread),
                { SortColumn: LogSearchSortColumn.Thread, IsSortAscending: false } => query.OrderByDescending(ent => ent.Thread),

                { SortColumn: LogSearchSortColumn.Level, IsSortAscending: true } => query.OrderBy(ent => ent.Level),
                { SortColumn: LogSearchSortColumn.Level, IsSortAscending: false } => query.OrderByDescending(ent => ent.Level),

                { SortColumn: LogSearchSortColumn.Context, IsSortAscending: true } => query.OrderBy(ent => ent.Context),
                { SortColumn: LogSearchSortColumn.Context, IsSortAscending: false } => query.OrderByDescending(ent => ent.Context),

                { SortColumn: LogSearchSortColumn.Message, IsSortAscending: true } => query.OrderBy(ent => ent.Message),
                { SortColumn: LogSearchSortColumn.Message, IsSortAscending: false } => query.OrderByDescending(ent => ent.Message),

                { SortColumn: LogSearchSortColumn.Exception, IsSortAscending: true } => query.OrderBy(ent => ent.Exception),
                { SortColumn: LogSearchSortColumn.Exception, IsSortAscending: false } => query.OrderByDescending(ent => ent.Exception),

                _ => query
            };

            return query;
        }

        private static IQueryable<LogEntity> BuildSearchQuery(LogSearchDto logSearch, IQueryable<LogEntity> query)
        {
            if (!string.IsNullOrEmpty(logSearch.JobId))
                query = query.Where(log => log.MigrationRecordEntity.SessionId.Contains(logSearch.JobId));

            if (!string.IsNullOrEmpty(logSearch.ProfileName))
                query = query.Where(log => log.MigrationRecordEntity.ProfileName.Contains(logSearch.ProfileName));

            if (!string.IsNullOrEmpty(logSearch.ObjectId))
                query = query.Where(log => log.MigrationRecordEntity.ObjectId == logSearch.ObjectId);

            //if (logSearch.MigrationRecordId.HasValue)
            //    query = query.Where(log => log.MigrationRecord_Id == logSearch.MigrationRecordId);

            if (!string.IsNullOrEmpty(logSearch.MigrationRecordState) && Enum.TryParse<MigrationState>(logSearch.MigrationRecordState, out var state))
                query = query.Where(log => log.MigrationRecordEntity.MigrationState_Id == state);

            if (logSearch.LogLevels is not null && logSearch.LogLevels.Count() > 0)
                query = query.Where(log => logSearch.LogLevels.Contains(log.Level));

            if (!string.IsNullOrEmpty(logSearch.Context))
                query = query.Where(log => log.Context.Contains(logSearch.Context));

            if (logSearch.FromDate.HasValue)
                query = query.Where(log => log.Date >= logSearch.FromDate.Value);

            if (logSearch.ToDate.HasValue)
                query = query.Where(log => log.Date <= logSearch.ToDate.Value);

            if (!string.IsNullOrEmpty(logSearch.Message))
                query = query.Where(log => log.Message.Contains(logSearch.Message));

            if (!string.IsNullOrEmpty(logSearch.Exception))
                query = query.Where(log => log.Exception.Contains(logSearch.Exception));

            return query;
        }

        private static IQueryable<LogEntity> ResolveCutoutQuery(LogSearchDto logSearch, IQueryable<LogEntity> query)
        {
            if (logSearch.Skip.HasValue)
                query = query.Skip(logSearch.Skip.Value);

            if (logSearch.Take.HasValue)
                query = query.Take(logSearch.Take.Value);

            return query;
        }

    }
}
