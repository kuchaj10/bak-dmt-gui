using DMTGui.Server.Extensions;
using DMTGui.Server.Services;
using Microsoft.AspNetCore.Mvc;

namespace DMTGui.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var app = CreateHostBuilder(args).Build();

            // Inicializace slu�eb po spu�t�n�
            var migrationService = app.Services.GetService<IMigrationService>();
            migrationService?.InitializeAsync();

            app.ConfigureExceptionHandler();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.MapRazorPages();
            app.MapControllers();
            app.MapFallbackToFile("index.html");
            app.Run();
        }

        public static WebApplicationBuilder CreateHostBuilder(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Host.UseWindowsService();

            //builder.Services.AddControllers();
            builder.Services.AddControllersWithViews();
            builder.Services.AddRazorPages();

            builder.ConfigureLogging();
            builder.ConfigureDatabase();

            // Konfigurace migra�n� knihovny
            builder.ConfigureDMTLib();
            builder.ConfigureDMTContext();
            builder.ConfigureDMTDictionaries();
            builder.ConfigureDMTLogging();

            // D�le�it� slu�by
            builder.Services.AddSingleton<IQuartzService, QuartzService>();

            builder.Services.AddTransient<IMigrationService, MigrationService>();
            builder.Services.AddTransient<IJobService, JobService>();
            builder.Services.AddTransient<IProfileService, ProfileService>();
            builder.Services.AddTransient<ILogService, LogService>();
            builder.Services.AddTransient<IDictionaryService, DictionaryService>();
            builder.Services.AddTransient<IStatisticsService, StatisticsService>();

            // Ov��ov�n� prob�h� p�i z�sk�n� dat na stran� slu�by, odpov�� je custom zpr�va
            builder.Services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            return builder;
        }
    }
}