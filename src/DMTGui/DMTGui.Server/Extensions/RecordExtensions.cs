﻿using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Main.Entities;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro záznamy migrace
    /// </summary>
    public static class RecordExtensions
    {
        /// <summary>
        /// Přemapování z <see cref="MigrationRecordEntity"/> na <see cref="RecordDto"/>
        /// </summary>
        public static RecordDto ToRecord(this MigrationRecordEntity entity)
        {
            return new RecordDto
            {
                Id = entity.Id,
                StartActionDate = entity.StartActionDate,
                EndActionDate = entity.EndActionDate,
                MigrationStateId = (int?)entity.MigrationStateEntity?.Id,
                ObjectId = entity.ObjectId,
                ProfileName = entity.ProfileName
            };
        }

        /// <summary>
        /// Vrátí čas zpracování v podobě ticků
        /// </summary>
        public static long GetActionTimeTicks(this MigrationRecordEntity entity)
        {
            return entity.EndActionDate.Ticks - entity.StartActionDate.Ticks;
        }
    }
}
