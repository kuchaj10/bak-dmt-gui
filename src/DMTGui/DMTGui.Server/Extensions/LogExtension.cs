﻿using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Main.Entities;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro chybové hlášky
    /// </summary>
    public static class LogExtension
    {
        /// <summary>
        /// Přemapování z <see cref="LogEntity"/> na <see cref="LogDto"/>
        /// </summary>
        public static LogDto ToLog(this LogEntity entity)
        {
            return new LogDto
            {
                Id = entity.Id,
                JobId = entity.MigrationRecordEntity?.SessionId,
                ProfileName = entity.MigrationRecordEntity?.ProfileName,
                ObjectId = entity.MigrationRecordEntity?.ObjectId,
                //MigrationRecordId = entity.MigrationRecord_Id,
                MigrationRecordState = entity.MigrationRecordEntity?.MigrationState_Id.ToString(),
                Context = entity.Context,
                Date = entity.Date,
                Exception = entity.Exception,
                Level = entity.Level,
                Message = entity.Message,
                Thread = entity.Thread,
            };
        }
    }
}
