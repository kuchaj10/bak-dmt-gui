﻿using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Main.Entities;
using ICZ.Sps.DMT.DMTDAL.Main.Enums;

namespace DMTGui.Server.Extensions
{
    public static class StatisticsExtensions
    {
        /// <summary>
        /// Vrátí statistické informace o předaném dni
        /// </summary>
        public static MigrationDayDto GetStatisticsDay(this IGrouping<DateTime, MigrationRecordEntity> group)
        {
            return new MigrationDayDto
            {
                Day = group.Key,
                StartTime = group.Min(sub => sub.StartActionDate).TimeOfDay,
                EndTime = group.Max(sub => sub.EndActionDate).TimeOfDay,
                Count = group.Count(sub => sub.MigrationState_Id == MigrationState.Exported || sub.MigrationState_Id == MigrationState.Error),
                SuccessfullCount = group.Count(sub => sub.MigrationState_Id == MigrationState.Exported),
                WrongCount = group.Count(sub => sub.MigrationState_Id == MigrationState.Error),
                SuccessfullSpeed = TimeSpan.FromTicks(group.GetAverageExportedTicks())
            };
        }

        /// <summary>
        /// Získá průměrný počet ticků pro úspěšné operace
        /// </summary>
        public static long GetAverageExportedTicks(this IGrouping<DateTime, MigrationRecordEntity> group)
        {
            return group.Any(sub => sub.MigrationState_Id == MigrationState.Exported)
                ? (long)group.Where(sub => sub.MigrationState_Id == MigrationState.Exported).Average(sub => sub.EndActionDate.Ticks - sub.StartActionDate.Ticks)
                : 0;
        }
    }
}
