﻿using DMTGui.Shared.Extensions;
using Quartz;

namespace DMTGui.Server.Extensions
{
    public static class QuartzTriggerExtensions
    {
        public static DateTimeOffset? GetLastFireTime(this ITrigger trigger, DateTimeOffset fromTime)
        {
            trigger.ThrowIfNull();
            fromTime.ThrowIfNull();

            var toTime = fromTime.AddDays(7);
            var breakTimeSpan = TimeSpan.FromHours(1);

            return trigger.GetLastFireTime(fromTime, toTime, breakTimeSpan);
        }

        public static DateTimeOffset? GetLastFireTime(this ITrigger trigger, DateTimeOffset fromTime, DateTimeOffset toTime, TimeSpan breakTimeSpan)
        {
            trigger.ThrowIfNull();
            fromTime.ThrowIfNull();
            toTime.ThrowIfNull();
            breakTimeSpan.ThrowIfNull();

            DateTimeOffset? firstDate = fromTime;
            DateTimeOffset? secondDate = trigger.GetFireTimeAfter(firstDate);

            if (secondDate is null)
                return null;

            while (secondDate is not null && (secondDate - firstDate) <= breakTimeSpan)
            {
                if (secondDate > toTime)
                    return null;
                
                firstDate = secondDate;
                secondDate = trigger.GetFireTimeAfter(firstDate);
            }

            return firstDate;
        }
    }
}
