﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using DMTGui.Dal.Entities;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rošíření na typy související s úlohami
    /// </summary>
    public static class JobExtensions 
    {
        /// <summary>
        /// Přemapování z <see cref="JobEntity"/> na <see cref="JobDto"/>
        /// </summary>
        public static JobDto ToJob(this JobEntity entity)
        {
            return new JobDto
            {
                Id = entity.Id.ToBase64String(),
                Description = entity.Description,
                Name = entity.Name,
                ActualQueueSize = entity.ActualQueueSize,
                QueueSize = entity.QueueSize,
                ActualStep = entity.ActualStep,
                StepCount = entity.StepCount,
                Profiles = entity.GetProfileLinks(),
                Triggers = entity.GetTriggers(),
                State = entity.GetState(),
                LastRun = entity.LastRun,
                ScheduledStop = entity.ScheduledStop,
                Created = entity.Created,
            };
        }

        /// <summary>
        /// Z předané <paramref name="entity"/> získá odkazy na všechny profily
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public static ProfileLinkDto[] GetProfileLinks(this JobEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            return entity.Profiles?
                .OrderBy(prf => prf.JobStepOrder)
                .Select(prf => prf.ToProfileLink())
                .ToArray();
        }

        /// <summary>
        /// Z předanéh <paramref name="entity"/> získá všechny triggery a vrátí je jako DTO.
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public static TriggerDto[] GetTriggers(this JobEntity entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            return entity.Triggers?
                .Select(prf => prf.ToTrigger())
                .ToArray();
        }

        /// <summary>
        /// Z předaného <paramref name="entity"/> získá reprezentaci stavu pro DTO.
        /// </summary>
        public static JobState GetState(this JobEntity entity)
        {
            var state = entity switch
            {
                { IsEnabled: false } => JobState.Disabled,
                { IsRunning: true } => JobState.Running,
                { IsScheduled: true } => JobState.Waiting,
                { IsScheduled: false } => JobState.Enabled,
                _ => JobState.Invalid
            };

            return state;
        }
    }
}
