﻿using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Dictionaries.Models;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro manipulaci se slovníky
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Přemapuje <see cref="Dictionary"/> na <see cref="DictionaryDto"/>
        /// </summary>
        public static DictionaryDto ToDto(this Dictionary dictionary)
        {
            var entries = dictionary.Entries?.Select(ent => ent.ToDto()).ToArray();

            var dictionaryDto = new DictionaryDto
            {
                Name = dictionary.Name,
                Entries = entries ?? new DictionaryEntryDto[0],
                EntriesCount = entries.Length
            };

            return dictionaryDto;
        }

        /// <summary>
        /// Přemapuje <see cref="Dictionary"/> na <see cref="DictionaryDto"/> obsahu, takže pouze jméno.
        /// </summary>
        public static DictionaryDto ToDtoWithoutEntries(this Dictionary dictionary)
        {
            var dictionaryDto = new DictionaryDto
            {
                Name = dictionary.Name,
                EntriesCount = dictionary.Entries?.Length ?? 0
            };

            return dictionaryDto;
        }

        /// <summary>
        /// Přemapuje <see cref="DictionaryEntry"/> na <see cref="DictionaryEntryDto"/>.
        /// </summary>
        public static DictionaryEntryDto ToDto(this DictionaryEntry entry)
        {
            var properties = entry.Properties?.Select(prt => prt.ToDto()).ToArray();

            var entryDto = new DictionaryEntryDto
            {
                Key = entry.Key,
                Description = entry.Descripton,
                Properties = properties ?? new DictionaryEntryPropertyDto[0],
            };

            return entryDto;
        }

        /// <summary>
        /// Přemapuje <see cref="DictionaryEntryProperty"/> na <see cref="DictionaryEntryPropertyDto"/>
        /// </summary>
        public static DictionaryEntryPropertyDto ToDto(this DictionaryEntryProperty property)
        {
            var propertyDto = new DictionaryEntryPropertyDto
            {
                Key = property.Key,
                Descripton = property.Descripton,
                Value = property.Value
            };

            return propertyDto;
        }

        // Objekty

        /// <summary>
        /// Přemapuje <see cref="DictionaryDto"/> na <see cref="Dictionary"/>
        /// </summary>

        public static Dictionary ToDictionary(this DictionaryDto dictionary)
        {
            var entries = dictionary.Entries?.Select(ent => ent.ToEntry()).ToArray();

            var dictionaryDto = new Dictionary
            {
                Name = dictionary.Name,
                Entries = entries ?? new DictionaryEntry[0],
            };

            return dictionaryDto;
        }

        /// <summary>
        /// Přemapuje <see cref="DictionaryEntryDto"/> na <see cref="DictionaryEntry"/>
        /// </summary>

        public static DictionaryEntry ToEntry(this DictionaryEntryDto entry)
        {
            var properties = entry.Properties?.Select(prt => prt.ToProperty()).ToArray();

            var entryDto = new DictionaryEntry
            {
                Key = entry.Key,
                Descripton = entry.Description,
                Properties = properties ?? new DictionaryEntryProperty[0],
            };

            return entryDto;
        }

        /// <summary>
        /// Přemapuje <see cref="DictionaryEntryPropertyDto"/> na <see cref="DictionaryEntryProperty"/>
        /// </summary>
        public static DictionaryEntryProperty ToProperty(this DictionaryEntryPropertyDto property)
        {
            var propertyDto = new DictionaryEntryProperty
            {
                Key = property.Key,
                Descripton = property.Descripton,
                Value = property.Value
            };

            return propertyDto;
        }
    }
}
