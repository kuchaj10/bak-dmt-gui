﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using DMTGui.Dal.Entities;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro triggery
    /// </summary>
    public static class TriggerExtensions
    {
        /// <summary>
        /// Přemapování z <see cref="TriggerEntity"/> na <see cref="TriggerDto"/>
        /// </summary>
        public static TriggerDto ToTrigger(this TriggerEntity entity)
        {
            var trigger = new TriggerDto
            {
                Id = entity.Id.ToBase64String(),
                Description = entity.Description,
                Settings = entity.Settings
            };

            return trigger;
        }
    }
}
