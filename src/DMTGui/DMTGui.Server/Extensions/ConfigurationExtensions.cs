﻿using DMTGui.Dal;
using DMTGui.Server.Middlewares;
using ICZ.Sps.DMT.DMTDAL.Dictionaries;
using ICZ.Sps.DMT.DMTDAL.Extensions;
using ICZ.Sps.DMT.DMTDAL.Main;
using ICZ.Sps.DMT.DMTLib.Configuration;
using ICZ.Sps.DMT.DMTLib.Logging;
using System.Reflection;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro konfiguraci aplikace
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        /// Konfigurace logování aplikace.
        /// </summary>
        public static void ConfigureLogging(this WebApplicationBuilder builder)
        {
            var loggingConfig = builder.Configuration.GetSection("Logging");
            builder.Logging.AddConfiguration(loggingConfig);
        }

        /// <summary>
        /// Konfigurace databáze
        /// </summary>
        public static void ConfigureDatabase(this WebApplicationBuilder builder)
        {
            var appConfig = builder.Configuration.GetSection("App");
            var provider = appConfig.GetSection("Database").GetValue<ProviderType>("ProviderType");
            var connectionString = appConfig.GetSection("Database").GetValue<string>("ConnectionString");

            builder.Services.AddDbContextFactory<AppDbContext>(opt =>
            {
                opt.EnableSensitiveDataLogging();
                opt.UseSqlResolve(provider, connectionString);
            });
        }

        /// <summary>
        /// Konfigurace logování DMT
        /// </summary>
        public static void ConfigureDMTLogging(this WebApplicationBuilder builder)
        {
            var directoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? "";
            var pathToConfig = Path.Combine(directoryPath, "log4net.config");

            if (File.Exists(pathToConfig))
            {
                // Konfigurace logování pomocí log4net
                var options = new Log4NetProviderOptions(pathToConfig, true);
                var log4netProvider = new Log4NetProvider(options);
                DMTLoggerManager.AddLoggerProvider(log4netProvider);
            }
        }

        /// <summary>
        /// Konfigurace slovníků DMT
        /// </summary>
        public static void ConfigureDMTDictionaries(this WebApplicationBuilder builder)
        {
            var contextType = GlobalSettings.AppSettings.DictionariesContextType;

            var connectionPath = contextType == ContextType.EntityFramework
                ? GlobalSettings.AppSettings.DictionariesConnectionString
                : GlobalSettings.AppSettings.DictionariesXmlConfigPath;

            var providerType = GlobalSettings.AppSettings.DictionariesConnectionProvider;

            builder.Services.AddSingleton(sp => new DictionariesFactory(contextType, connectionPath, providerType));
        }

        /// <summary>
        /// Konfigurace databáze DMT
        /// </summary>
        public static void ConfigureDMTContext(this WebApplicationBuilder builder)
        {
            var connectionString = GlobalSettings.AppSettings.MainConnectionString;
            var connectionProvider = GlobalSettings.AppSettings.MainConnectionProvider;

            builder.Services.AddDbContextFactory<DMTContext>(opt =>
            {
                opt.EnableSensitiveDataLogging();
                opt.UseSqlResolve(connectionProvider, connectionString);
            });
        }

        /// <summary>
        /// Konfigurace DMT
        /// </summary>
        public static void ConfigureDMTLib(this WebApplicationBuilder builder)
        {
            var migrationConfig = builder.Configuration.GetSection("DMTLib");
            GlobalSettings.SetAppSettings(migrationConfig);
        }

        /// <summary>
        /// Konfigurace defaultního chování JSON serializeru
        /// </summary>
        public static void ConfigureJsonSerialization(this WebApplicationBuilder builder)
        {
            builder.Services
                .AddControllers()
                .AddJsonOptions(x =>
                {
                    // Při serializaci konvertuj enumy do jejich textové hodnoty
                    //x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

                    // Při serializaci ignoruj prázdný hodnoty
                    //x.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;

                    // Case insensitive 
                    //x.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                });
        }

        /// <summary>
        /// Konfigurace mezivrstvy zodpovědné za reakce na chybové stavy
        /// </summary>
        public static void ConfigureExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionsHandlerMiddleware>();
            app.UseMiddleware<FrameworkExceptionsMiddleware>();
        }
    }
}
