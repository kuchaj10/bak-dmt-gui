﻿using DMTGui.Shared.Exceptions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTDAL.Main.Entities;
using ICZ.Sps.DMT.DMTDAL.Main.Enums;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro identifikátory
    /// </summary>
    public static class IdentifierExtensions
    {
        /// <summary>
        /// Přemapuje <see cref="IdentifierEntity"/> na <see cref="IdentifierDto"/>.
        /// </summary>
        public static IdentifierDto ToDto(this IdentifierEntity entity)
        {
            var relatedIdentifiers = entity.Related?.Select(rel => rel.ToDto()).ToArray() ?? new IdentifierDto[0];

            var identifier = new IdentifierDto
            {
                Id = entity.Id,
                ProfileName = entity.MigrationRecordEntity?.ProfileName,
                SourceId = entity.SourceId,
                ValueId = entity.ValueId,
                ObjectId = entity.ObjectId,
                GroupId = entity.GroupId,
                State = entity.State.ToString(),
                Note = entity.Note,
                RelatedIdentifiers = relatedIdentifiers
            };

            return identifier;
        }

        /// <summary>
        /// Aktualizuje <paramref name="entity"/> daty z předaného <paramref name="dto"/>.
        /// </summary>
        /// <exception cref="WrongOperationException"></exception>
        public static void UpdateEntity(this IdentifierEntity entity, IdentifierDto dto)
        {
            var state = entity.State;

            if (!string.IsNullOrEmpty(dto.State) && !Enum.TryParse(dto.State, out state))
                throw new WrongOperationException($"Nepodařilo se zkonvertovat hodnotu {dto.State} na typ {nameof(IdentifierState)}");

            entity.SourceId = dto.SourceId;
            entity.ValueId = dto.ValueId;
            entity.ObjectId = dto.ObjectId;
            entity.GroupId = dto.GroupId;
            entity.State = state;
            entity.Note = dto.Note;
        }

        /// <summary>
        /// Vrátí novou entitu z <paramref name="dto"/> dělá pouze namapování a vytvořené nové třídy.
        /// </summary>
        public static IdentifierEntity ToNewEntity(this IdentifierDto dto)
        {
            var related = dto.RelatedIdentifiers?.Select(idt => idt.ToNewEntity()).ToArray() ?? new IdentifierEntity[0];

            var identifier = new IdentifierEntity
            {
                SourceId = dto.SourceId,
                ValueId = dto.ValueId,
                ObjectId = dto.ObjectId,
                GroupId = dto.GroupId,
                State = IdentifierState.Unused,
                Note = dto.Note,
                Related = related
            };

            return identifier;
        }

        /// <summary>
        /// Všechny vnořené identifikátory vezme a dá je na stejnou úroveň.
        /// </summary>
        public static IEnumerable<IdentifierDto> Flatten(this IdentifierDto dto)
        {
            var related = dto.RelatedIdentifiers?.SelectMany(idt => idt.Flatten()).ToList() ?? new List<IdentifierDto>();
            
            dto.RelatedIdentifiers = null;

            return related.Prepend(dto);
        }
    }
}
