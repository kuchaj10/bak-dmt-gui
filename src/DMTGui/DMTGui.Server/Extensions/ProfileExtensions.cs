﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using DMTGui.Dal.Entities;

namespace DMTGui.Server.Extensions
{
    /// <summary>
    /// Rozšíření pro migrační profily
    /// </summary>
    public static class ProfileExtensions
    {
        /// <summary>
        /// Vezme předanou entitu <see cref="ProfileEntity"/> a rozbalí jí do DTO <see cref="ProfileDto"/>.
        /// </summary>
        public static ProfileDto ToProfile(this ProfileEntity entity)
        {
            var profile = new ProfileDto
            {
                Id = entity.Id.ToBase64String(),
                Name = entity.Name,
                Data = entity.Data,
                JobId = entity.Job?.Id.ToBase64String(),
                JobName = entity.Job?.Name,
                Created = entity.Created
            };

            return profile;
        }

        /// <summary>
        /// Vezme předanou entitu <see cref="ProfileEntity"/> a rozbalí jí do DTO <see cref="ProfileDto"/> bez dat.
        /// </summary>
        public static ProfileDto ToProfileWithoutData(this ProfileEntity entity)
        {
            return entity.ToProfile()
                .RemoveData();
        }

        /// <summary>
        /// Přemapování části z <see cref="ProfileEntity"/> na link <see cref="ProfileLinkDto"/>
        /// </summary>
        public static ProfileLinkDto ToProfileLink(this ProfileEntity entity)
        {
            var link = new ProfileLinkDto
            {
                ProfileId = entity.Id.ToBase64String(),
                ProfileName = entity.Name
            };

            return link;
        }

        private static ProfileDto RemoveData(this ProfileDto profile)
        {
            profile.Data = null;
            return profile;
        }
    }
}
