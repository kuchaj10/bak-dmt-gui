﻿using DMTGui.Dal.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DMTGui.Dal
{
    /// <summary>
    /// Factory na vytvoření contextu databáze. Není bez ní možné generovat migrační skripty pro databázi.
    /// </summary>
    public sealed class AppDbContextFactory : IDbContextFactory<AppDbContext>
    {
        private readonly string _connectionString;
        private readonly ProviderType _connectionProvider;
        public readonly ILoggerFactory _loggerFactory;

        public AppDbContextFactory()
        {
        }

        public AppDbContextFactory(string connectionString, ProviderType provider)
        {
            _connectionString = connectionString;
            _connectionProvider = provider;
        }

        public AppDbContextFactory(string connectionString, ProviderType provider, ILoggerFactory loggerFactory)
        {
            _connectionString = connectionString;
            _connectionProvider = provider;
            _loggerFactory = loggerFactory;
        }

        public AppDbContext CreateDbContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlResolve(_connectionProvider, _connectionString);

            if (_loggerFactory != null)
            {
                optionsBuilder.UseLoggerFactory(_loggerFactory);
            }

            return new AppDbContext(optionsBuilder.Options);
        }
    }
}
