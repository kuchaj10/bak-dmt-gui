﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DMTGui.Dal.Entities
{
    public class JobEntity
    {
        /// <summary>
        /// Identifkátor úlohy
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Název úlohy
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Popis úlohy
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Aktuálně běžící krok úlohy
        /// </summary>
        public int ActualStep { get; set; }

        /// <summary>
        /// Počet kroků úlohy
        /// </summary>
        public int StepCount { get; set; }

        /// <summary>
        /// Aktuální množství fronty na zpracování 
        /// </summary>
        public int ActualQueueSize { get; set; }

        /// <summary>
        /// Velikost fronty na zpracování
        /// </summary>
        public int QueueSize { get; set; }

        /// <summary>
        /// Příznak, zda úloha aktuálně zpracovává data
        /// </summary>
        public bool IsRunning { get; set; }

        /// <summary>
        /// Příznak, zda je úloha naplánovaná ke spuštění
        /// </summary>
        public bool IsScheduled { get; set; }

        /// <summary>
        /// Příznak, zda je úloha aktivní
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Datum a čas posledního spuštění úlohy
        /// </summary>
        public DateTime? LastRun { get; set; }

        /// <summary>
        /// Datum a čas odhadovaného konce spuštěné úlohy
        /// </summary>
        public DateTime? ScheduledStop { get; set; }

        /// <summary>
        /// Datum a čas vytvoření úlohy
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Spouštěče úlohy
        /// </summary>
        public ICollection<TriggerEntity> Triggers { get; set; }

        /// <summary>
        /// Profily, které s úlohou souvisí
        /// </summary>
        public ICollection<ProfileEntity> Profiles { get; set; }

        public class JobConfiguration : IEntityTypeConfiguration<JobEntity>
        {
            public void Configure(EntityTypeBuilder<JobEntity> builder)
            {
                // Vlastnosti tabulky, indexy a FK
                builder.HasKey(ent => ent.Id);

                builder.HasMany(ent => ent.Triggers)
                    .WithOne(ent => ent.Job);

                builder.HasMany(ent => ent.Profiles)
                    .WithOne(ent => ent.Job);

                // Vlastnosti konkrétních sloupečků
                builder.Property(ent => ent.Id)
                    .ValueGeneratedOnAdd();

                builder.Property(ent => ent.Name)
                    .HasMaxLength(256);

                builder.Property(ent => ent.Description)
                    .HasMaxLength(1024);

                builder.Property(ent => ent.IsRunning)
                    .HasDefaultValue(false);

                builder.Property(ent => ent.IsScheduled)
                    .HasDefaultValue(false);

                builder.Property(ent => ent.IsEnabled)
                    .HasDefaultValue(false);

                builder.Property(ent => ent.LastRun)
                    .HasDefaultValue(null);

                builder.Property(ent => ent.ScheduledStop)
                    .HasDefaultValue(null);

                builder.Property(ent => ent.Created)
                    .HasDefaultValue(DateTime.MinValue)
                    .IsRequired();
            }
        }
    }
}
