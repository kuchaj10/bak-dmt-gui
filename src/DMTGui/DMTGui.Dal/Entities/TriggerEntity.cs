﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DMTGui.Dal.Entities
{
    public class TriggerEntity
    {
        /// <summary>
        /// Identifikátor
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Popis spouštěče
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Nastavení spouštěče
        /// </summary>
        public string Settings { get; set; }

        /// <summary>
        /// Identifikátor úlohy, která je spojená se špouštěčem
        /// </summary>
        public Guid? JobId { get; set; }

        /// <summary>
        /// Úloha, která je spojená se spouštěčem
        /// </summary>
        public JobEntity Job { get; set; }

        /// <summary>
        /// Konfigurace spouštěče
        /// </summary>
        public class TriggerConfiguration : IEntityTypeConfiguration<TriggerEntity>
        {
            public void Configure(EntityTypeBuilder<TriggerEntity> builder)
            {
                // Vlastnosti tabulky, indexy a FK
                builder.HasKey(ent => ent.Id);

                builder.HasOne(ent => ent.Job)
                    .WithMany(oth => oth.Triggers)
                    .HasForeignKey(ent => ent.JobId);

                // Vlasntosti konkrétních sloupečků
                builder.Property(ent => ent.Id)
                    .ValueGeneratedOnAdd();

                builder.Property(ent => ent.Description)
                    .HasMaxLength(1024);
            }
        }
    }
}
