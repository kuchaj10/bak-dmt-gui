﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DMTGui.Dal.Entities
{
    public class ProfileEntity
    {
        /// <summary>
        /// Automaticky generovaný identifikátor profilu.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Unikátní název profilu.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Popis profilu.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Data profilu. Jedná se samotný profil. Jako takový je možné ho rovnou použít při migraci.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Informace o tom, kdy byl profil vytvořený.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Informace o tom, kdy byl profil naposledny aktualizovaný.
        /// </summary>
        public DateTime Updated { get; set; }

        /// <summary>
        /// Informace o tom, zda je profil validní. 
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>
        /// Informace o chybným počtu dokumentů pro daný profil. 
        /// Jedná se o sumarizační jednotku a nemělo by sloužit k výpočtu statistiky.
        /// </summary>
        public int SumErrorObjectCount { get; set; }

        /// <summary>
        /// Informace o počtu zpracovaných dokumentů pro daný profil. 
        /// Jedná se o sumarizační jednotku a nemělo by sloužit k výpočtu statistiky.
        /// </summary>
        public int SumProcessedObjectCount { get; set; }

        /// <summary>
        /// Informace o celkovým počtu dokumentů pro daný profil. 
        /// Jedná se o sumarizační jednotku a nemělo by sloužit k výpočtu statistiky.
        /// </summary>
        public int SumAllObjectCount { get; set; }

        /// <summary>
        /// Cizí klíč spojené úlohy
        /// </summary>
        public Guid? JobId { get; set; }

        /// <summary>
        /// Úloha ve které je profil využíván.
        /// </summary>
        public JobEntity Job { get; set; }

        /// <summary>
        /// Pořadí vykonávání profilu v úloze.
        /// </summary>
        public int? JobStepOrder { get; set; }

        /// <summary>
        /// Externí identifikátor profilu, který slouží k provázání profilů z migrací DMT a migrační aplikace.
        /// </summary>
        public int? ExtId { get; set; }

        public class ProfileConfiguration : IEntityTypeConfiguration<ProfileEntity>
        {
            public void Configure(EntityTypeBuilder<ProfileEntity> builder)
            {
                // Vlastnosti tabulky, indexy a FK
                builder.HasKey(ent => ent.Id);

                builder.HasIndex(ent => ent.Name)
                    .IsUnique();

                builder.HasOne(ent => ent.Job)
                    .WithMany(oth => oth.Profiles)
                    .HasForeignKey(ent => ent.JobId);

                // Vlasntosti konkrétních sloupečků
                builder.Property(ent => ent.Id)
                    .ValueGeneratedOnAdd();

                builder.Property(ent => ent.Name)
                    .HasMaxLength(256);

                builder.Property(ent => ent.Description)
                    .HasMaxLength(1024);

                builder.Property(ent => ent.Created)
                    .IsRequired();
            }
        }
    }
}
