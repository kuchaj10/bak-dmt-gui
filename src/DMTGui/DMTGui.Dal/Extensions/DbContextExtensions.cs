﻿using DMTGui.Dal.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DMTGui.Dal.Extensions
{
    public static class DbContextExtensions
    {
        /// <summary>
        /// Pomocí předaného typu se pokusí najít odpovídající entitu a vrátit název její tabulky z modelu v kontextu.
        /// Pokud se nepodaří najít nic, tak vyhodí chybu.
        /// </summary>
        /// <exception cref="DMTDatabaseException">V případě nenalezení entity</exception>
        public static string GetTableName<TEntity>(this DbContext context) where TEntity : class
        {
            var entityType = context.Model.FindEntityType(typeof(TEntity));
            if (entityType is null)
                throw new DatabaseException($"Entita {typeof(TEntity).Name} nebyla nalezena");

            return entityType.ClrType.Name;
        }

        /// <summary>
        /// Truncate smaže všechny záznamy z tabulky. Jedná se o DLL příkaz. Je rychlejší než klasický DELETE, ale nese sebou několik nevýhod.
        /// Například v případě oracle databáze se jedná o dvou commitovou záležitost, která se nedá rollbacknout. V případě SQLSeververu a jiných to není problém.
        /// Dále je potřeba mít jistotu, že daná tabulka nevyvolá problém s chybějícími FK po smazání. 
        /// </summary>
        public static void TruncateTable<TEntity>(this DbContext context) where TEntity : class
        {
            var tableName = context.GetTableName<TEntity>();

            context.Database.ExecuteSqlRaw($"TRUNCATE TABLE [{tableName}]");
        }

        /// <summary>
        /// Provede smazání všech záznamů z tabulky pro předaný typ.
        /// </summary>
        public static void DeleteFromTable<TEntity>(this DbContext context) where TEntity : class
        {
            var tableName = context.GetTableName<TEntity>();

            context.Database.ExecuteSqlRaw($"DELETE FROM [{tableName}]");
        }

        /// <summary>
        /// Rozpozná typ databáze podle předaného <see cref="ProviderType"/> a použije odpovídající databázový konektor.
        /// </summary>
        /// <exception cref="DMTDatabaseException">V případě neimplementované možnosti <see cref="ProviderType"/></exception>
        public static void UseSqlResolve(this DbContextOptionsBuilder options, ProviderType provider, string connectionString)
        {
            switch (provider)
            {
                case ProviderType.SQLite:
                    options.UseSqlite(connectionString);
                    break;
                case ProviderType.SqlServer:
                    options.UseSqlServer(connectionString);
                    break;
                case ProviderType.Npgsql:
                    options.UseNpgsql(connectionString);
                    break;
                default:
                    throw new DatabaseException($"Neexistuje connection provider pro možnost '{provider.ToString()}'");
            }
        }
    }

    public enum ProviderType
    {
        SQLite,
        SqlServer,
        Npgsql
    }
}
