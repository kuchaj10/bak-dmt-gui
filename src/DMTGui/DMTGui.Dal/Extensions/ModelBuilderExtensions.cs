﻿using System;
using Microsoft.EntityFrameworkCore;

namespace DMTGui.Dal.Extensions
{
    public static class ModelBuilderExtensions 
    {
        /// <summary>
        /// Nastaví mapování všech tabulek podle názvu entity namísto názvu DbSet.
        /// Využití je hlavně z důvodu toho, aby DbSet mohl mít množný název, ale 
        /// tabulky se uložily v jednotným.
        /// </summary>
        public static void SetTablesNamesAsEntityNames(this ModelBuilder modelBuilder)
        {
            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                var entityName = entityType.ClrType.Name;
                if (entityName.EndsWith("Entity"))
                {
                    var lastIndex = entityName.LastIndexOf("Entity", StringComparison.Ordinal);

                    entityName = entityName.Remove(lastIndex, "Entity".Length);
                }

                entityType.SetTableName(entityName);
            }
        }

        public static void EnsureSeedDataExists(this ModelBuilder builder)
        {
            //var profiles = new List<ProfileEntity>
            //{
            //    new ProfileEntity() { Id = Guid.NewGuid(), Name= "Profil 1" },
            //    new ProfileEntity() { Id = Guid.NewGuid(), Name= "Profil 2" },
            //    new ProfileEntity() { Id = Guid.NewGuid(), Name= "Profil 3" },
            //    new ProfileEntity() { Id = Guid.NewGuid(), Name= "Nepoužitý profil" }
            //};

            //var triggers = new List<TriggerEntity>
            //{
            //    new TriggerEntity() { Id = Guid.NewGuid() },
            //    new TriggerEntity() { Id = Guid.NewGuid() },
            //    new TriggerEntity() { Id = Guid.NewGuid() }
            //};

            //var jobs = new List<JobEntity>
            //{
            //    new JobEntity()
            //    {
            //        Id = Guid.NewGuid(),
            //        Name = "Úloha 1",
            //        Description = "Popis úlohy 1",
            //        IsEnabled = true,
            //        IsRunning = false,
            //    }
            //};

            //profiles.ForEach(prf => prf.JobId = jobs[0].Id);
            //triggers.ForEach(trg => trg.JobId = jobs[0].Id);

            //builder.Entity<TriggerEntity>().HasData(triggers);
            //builder.Entity<ProfileEntity>().HasData(profiles);
            //builder.Entity<JobEntity>().HasData(jobs);
        }
    }
}
