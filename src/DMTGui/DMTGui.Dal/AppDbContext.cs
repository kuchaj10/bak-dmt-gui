﻿using DMTGui.Dal.Entities;
using DMTGui.Dal.Extensions;
using Microsoft.EntityFrameworkCore;

namespace DMTGui.Dal
{
    /// <summary>
    /// Databázový kontext migrační aplikace
    /// </summary>
    public class AppDbContext : DbContext
    {
        /// <summary>
        /// Databázová tabulka s úlohami
        /// </summary>
        public DbSet<JobEntity> Jobs { get; set; }

        /// <summary>
        /// Databázová tabulka se spouštěči úloh
        /// </summary>
        public DbSet<TriggerEntity> Triggers { get; set; }

        /// <summary>
        /// Databázová tabulka s profily
        /// </summary>
        public DbSet<ProfileEntity> Profiles { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.SetTablesNamesAsEntityNames();

            modelBuilder.ApplyConfiguration(new ProfileEntity.ProfileConfiguration());

            modelBuilder.EnsureSeedDataExists();

            base.OnModelCreating(modelBuilder);
        }
    }
}
