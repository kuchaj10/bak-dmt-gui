using DMTGui.Client;
using DMTGui.Client.Services;
using DMTGui.Client.Services.Api;
using DMTGui.Shared.Contracts;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;
using Polly;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

var loggingConfig = builder.Configuration.GetSection("Logging");
builder.Logging.AddConfiguration(loggingConfig);

builder.Services.AddMudServices();

builder.Services.AddTransient<ILocalStorage, LocalStorage>();
builder.Services.AddTransient<IProfileContract, ProfileService>();
builder.Services.AddTransient<ILogContract, LogService>();
builder.Services.AddTransient<IRecordContract, RecordService>();
builder.Services.AddTransient<IJobContract, JobService>();
builder.Services.AddTransient<IDictionaryContract, DictionaryService>();
builder.Services.AddTransient<IIdentifierContract, IdentifierService>();
builder.Services.AddTransient<IStatisticsContract, StatisticsService>();

builder.Services.AddTransient<DataTableService>();
builder.Services.AddTransient<ClipboardService>();
builder.Services.AddTransient<SaveFileService>();
builder.Services.AddTransient<EncodingService>();
builder.Services.AddTransient<ProfileValidatorService>();

var appConfig = builder.Configuration.GetSection("App");
var serverUri = appConfig.GetValue<string>("ServerUri");

builder.Services
    .AddHttpClient("Server", client =>
    {
        client.BaseAddress = new Uri(serverUri);
    })
    .AddTransientHttpErrorPolicy(builder =>
        builder.WaitAndRetryAsync(new[]
        {
            TimeSpan.FromSeconds(1),
            TimeSpan.FromSeconds(2),
            TimeSpan.FromSeconds(5)
        }));


builder.Services.AddHttpClient<IProfileContract, ProfileService>("Server");
builder.Services.AddHttpClient<ILogContract, LogService>("Server");
builder.Services.AddHttpClient<IRecordContract, RecordService>("Server");
builder.Services.AddHttpClient<IJobContract, JobService>("Server");
builder.Services.AddHttpClient<IDictionaryContract, DictionaryService>("Server");
builder.Services.AddHttpClient<IIdentifierContract, IdentifierService>("Server");
builder.Services.AddHttpClient<IStatisticsContract, StatisticsService>("Server");

await builder.Build().RunAsync();