﻿using BlazorMonaco;

namespace DMTGui.Client.MonacoOperations
{
    public class MonacoOperationProcessor
    {
        private List<IMonacoOperation> _operations;

        public MonacoOperationProcessor()
        {
            _operations = new List<IMonacoOperation>();
        }

        public void AddOperation(IMonacoOperation operation)
        {
            _operations.Add(operation);
        }

        public void ClearOperations()
        {
            _operations.Clear();
        }

        public List<IdentifiedSingleEditOperation> ApplyOperations(ModelContentChangedEvent changedEvent, string value)
        {
            var lines = value.Split(new[] { "\n\r", "\n" }, StringSplitOptions.None);
            
            var edits = _operations
                .SelectMany(opr => opr.Change(changedEvent, lines))
                .ToList();

            return edits;
        }
    }
}
