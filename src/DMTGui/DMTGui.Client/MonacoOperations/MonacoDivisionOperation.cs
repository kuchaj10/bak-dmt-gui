﻿using BlazorMonaco;
using System.Text.RegularExpressions;

namespace DMTGui.Client.MonacoOperations
{
	public class MonacoDivisionOperation : IMonacoOperation
	{
		public List<IdentifiedSingleEditOperation> Change(ModelContentChangedEvent changedEvent, string[] editorLines)
		{
			var edits = new List<IdentifiedSingleEditOperation>();

			if (changedEvent.Changes.Count() != 1 || changedEvent.IsRedoing || changedEvent.IsUndoing || changedEvent.IsFlush)
				return edits;

			var change = changedEvent.Changes.First();
			if (change.Text != "/")
				return edits;

			var position = new { Line = change.Range.StartLineNumber, Column = change.Range.StartColumn };

			var selectedLine = editorLines[position.Line - 1];
			var columnIndex = position.Column - 1;

			for (; columnIndex >= 0; columnIndex--)
			{
				if (selectedLine[columnIndex] == '<') break;
			}

			var partOfSelectedLine = selectedLine.Substring(columnIndex, change.Range.StartColumn - columnIndex - 1);
			var match = Regex.Match(partOfSelectedLine, "<([a-zA-Z0-9]+)", RegexOptions.Compiled);

			if (match.Success)
			{
				var valueToInsert = $">";

				var operation = new IdentifiedSingleEditOperation()
				{
					Range = new BlazorMonaco.Range(position.Line, position.Column + 1, position.Line, position.Column + valueToInsert.Length + 1),
					Text = valueToInsert
				};

				edits.Add(operation);
			}

			return edits;
		}
	}
}
