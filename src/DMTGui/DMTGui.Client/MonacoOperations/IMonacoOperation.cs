﻿using BlazorMonaco;

namespace DMTGui.Client.MonacoOperations
{
    public interface IMonacoOperation
    {
        List<IdentifiedSingleEditOperation> Change(ModelContentChangedEvent changedEvent, string[] editorLines);
    }
}
