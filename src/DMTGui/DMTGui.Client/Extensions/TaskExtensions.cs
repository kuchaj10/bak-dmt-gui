﻿namespace DMTGui.Client.Extensions
{
    public static class TaskExtensions
    {
		public class TaskResult<T>
		{
			public bool Success { get; set; }

			public T Result { get; set; }
		}

		public static async Task<bool> TryInvoke(this Task task)
		{
			try
			{
				await task;

				return true;
			}
			catch
			{
				return false;
			}
		}

		public static async Task<TaskResult<TResult>> TryInvoke<TResult>(this Task<TResult> task)
		{
			try
			{
				var result = await task;

				return new TaskResult<TResult>
				{
					Success = true,
					Result = result
				};
			}
			catch
			{
				return new TaskResult<TResult>
				{
					Success = false,
					Result = default
				};
			}
		}
	}
}
