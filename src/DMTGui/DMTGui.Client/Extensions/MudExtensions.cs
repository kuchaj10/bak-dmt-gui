﻿using System.Text;
using Microsoft.AspNetCore.Components.Forms;
using DMTGui.Shared.Extensions;

namespace DMTGui.Client.Extensions
{
    /// <summary>
    /// Rozšíření pro elementy a nástroje z knihovny <see cref="MudBlazor"/>
    /// </summary>
    public static class MudExtensions
    {
        /// <summary>
        /// Maximální velikost souboru pro otevření.
        /// </summary>
        private const long MaxSize = 25 * 1024 * 1024;

        /// <summary>
        /// Aktuální soubor otevře a přečte jako by byl textový soubor.
        /// </summary>
        public static async Task<string> ReadTextAsync(this IBrowserFile file, Encoding encoding = null)
        {
            using var memoryStream = new MemoryStream();
            using var stream = file.OpenReadStream(MaxSize);

            await stream.CopyToAsync(memoryStream);

            if (encoding is null)
            {
                encoding = memoryStream.GetEncoding();
            }
            
            var bytes = memoryStream.ToArray();

            return encoding.GetString(bytes);
        }

        /// <summary>
        /// Přečte binární data předaného souboru
        /// </summary>
        public static async Task<byte[]> ReadBytesAsync(this IBrowserFile file)
        {
            using var memoryStream = new MemoryStream();
            using var stream = file.OpenReadStream(MaxSize);

            await stream.CopyToAsync(memoryStream);

            return memoryStream.ToArray();
        }
    }
}
