﻿//using DMTGui.Shared.Extensions;
//using System.Runtime.CompilerServices;
//using System.Xml;

//namespace DMTGui.Client.Extensions
//{
//    public static class ExceptionExtensions
//    {
//        public static void ThrowIfNotOnElement(this XmlReader argument, [CallerArgumentExpression("argument")] string paramName = null)
//        {
//            argument.ThrowIfNull();

//            if (argument.NodeType == XmlNodeType.Element)
//                return;

//            throw new ArgumentException($"XmlReader is not set on node of type '{nameof(XmlNodeType.Element)}'", paramName);
//        }

//        public static void ThrowIfNotOnAttribute(this XmlReader argument, [CallerArgumentExpression("argument")] string paramName = null)
//        {
//            argument.ThrowIfNull();

//            if (argument.NodeType == XmlNodeType.Attribute)
//                return;

//            throw new ArgumentException($"XmlReader is not set on node of type '{nameof(XmlNodeType.Attribute)}'", paramName);
//        }
//    }
//}
