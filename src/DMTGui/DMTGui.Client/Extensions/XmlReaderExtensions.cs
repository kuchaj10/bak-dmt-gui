﻿using DMTGui.Client.Models;
using System.Xml;

namespace DMTGui.Client.Extensions
{
    /// <summary>
    /// Rozšíření pro <see cref="XmlReader"/>
    /// </summary>
    public static class XmlReaderExtensions
    {
        /// <summary>
        /// Aktuální element vezme a vytvoří z něj <see cref="ElementInfo"/>
        /// </summary>
        public static ElementInfo GetElement(this XmlReader reader)
        {
            var xmlLineInfo = (IXmlLineInfo)reader;

            var elementInfo = new ElementInfo
            {
                Name = reader.Name,
                Depth = reader.Depth,
                Position = new PositionInfo
                {
                    StartLineNumber = xmlLineInfo.LineNumber,
                    StartLinePosition = xmlLineInfo.LinePosition,
                    EndLineNumber = xmlLineInfo.LineNumber,
                    EndLinePosition = xmlLineInfo.LinePosition,
                }
            };

            elementInfo.Attributes = reader.GetAttributes();

            return elementInfo;
        }

        /// <summary>
        /// Z předaného elementu získá všechny jeho parametry ve formátu <see cref="AttributeInfo"/>
        /// </summary>
        public static List<AttributeInfo> GetAttributes(this XmlReader reader)
        {
            var attributes = new List<AttributeInfo>();

            if (!reader.HasAttributes)
                return attributes;

            var xmlLineInfo = (IXmlLineInfo)reader;

            while (reader.MoveToNextAttribute())
            {
                var attributeName = reader.Name;
                var attributeLineNumber = xmlLineInfo.LineNumber;
                var attributeLinePosition = xmlLineInfo.LinePosition;

                reader.ReadAttributeValue();
                var attributeValue = reader.Value;
                var valueLineNumber = xmlLineInfo.LineNumber;
                var valueLinePosition = xmlLineInfo.LinePosition;

                var attributeWidth = (valueLinePosition + attributeValue.Length + 1) - attributeLinePosition;
                var valueWidth = attributeValue.Length;

                var attributeInfo = new AttributeInfo
                {
                    Name = attributeName,
                    Value = attributeValue,
                    AttributePosition = new PositionInfo
                    {
                        StartLineNumber = attributeLineNumber,
                        StartLinePosition = attributeLinePosition,
                        EndLineNumber = attributeLineNumber,
                        EndLinePosition = attributeLinePosition + attributeWidth,
                    },
                    ValuePosition = new PositionInfo
                    {
                        StartLineNumber = valueLineNumber,
                        StartLinePosition = valueLinePosition,
                        EndLineNumber = valueLineNumber,
                        EndLinePosition = valueLinePosition + valueWidth,
                    }
                };

                attributes.Add(attributeInfo);
            }

            return attributes;
        }
    }
}
