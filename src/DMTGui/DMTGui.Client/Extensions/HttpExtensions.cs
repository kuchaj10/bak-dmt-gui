﻿using DMTGui.Client.Exceptions;
using DMTGui.Shared.Models;
using System.Reflection;
using System.Text.Json;
using System.Web;

namespace DMTGui.Client.Extensions
{
    /// <summary>
    /// Rozšíření pro HTTP
    /// </summary>
    public static class HttpExtensions
    {
        /// <summary>
        /// Vyhodí chybovou hlášku, pokud odpověď HTTP napovídá chybu
        /// </summary>
        /// <exception cref="ServerException"></exception>
        public static void ThrowIfNotSuccess(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
                return;

            var details = message.Content.Deserialize<ErrorDetail>();
            if (details is null)
            {
                details = new ErrorDetail { StatusCode = (int)message.StatusCode, Message = message.ReasonPhrase };
            }

            throw new ServerException(details);
        }

        /// <summary>
        /// Vyhodí chybovou hlášku, pokud odpověď HTTP napovídá chybu
        /// </summary>
        /// <exception cref="ServerException"></exception>
        public static async Task ThrowIfNotSuccessAsync(this HttpResponseMessage message)
        {
            if (message.IsSuccessStatusCode)
                return;

            var details = await message.Content.DeserializeAsync<ErrorDetail>();
            if (details is null)
            {
                details = new ErrorDetail { StatusCode = (int)message.StatusCode, Message = message.ReasonPhrase };
            }

            throw new ServerException(details);
        }

        /// <summary>
        /// Deserializuje <see cref="HttpContent"/> do předaného objektu typu <typeparamref name="TType"/>
        /// </summary>
        public static TType Deserialize<TType>(this HttpContent content)
        {
            var contentAsString = content.ReadAsStringAsync().Result;

            var item = JsonSerializer.Deserialize<TType>(contentAsString, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            return item;
        }

        /// <summary>
        /// Deserializuje <see cref="HttpContent"/> do předaného objektu typu <typeparamref name="TType"/>
        /// </summary>
        public static async Task<TType> DeserializeAsync<TType>(this HttpContent content)
        {
            var contentAsString = await content.ReadAsStringAsync();

            var item = JsonSerializer.Deserialize<TType>(contentAsString, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });

            return item;
        }        
    }
}
