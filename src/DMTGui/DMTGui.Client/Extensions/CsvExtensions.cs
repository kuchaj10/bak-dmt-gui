﻿using CsvHelper;

namespace DMTGui.Client.Extensions
{
    /// <summary>
    /// Rozšíření pro <see cref="CsvHelper"/>
    /// </summary>
    static class CsvExtensions
    {
        /// <summary>
        /// Aktuální záznam projde a vrátí seznam kombinací názvů a hodnot
        /// </summary>
        public static KeyValuePair<string, string>[] GetFields(this CsvReader reader)
        {
            var result = new List<KeyValuePair<string, string>>();

            var length = reader.HeaderRecord.Length;

            for (var i = 0; i < length; i++)
            {
                var pair = new KeyValuePair<string, string>(
                    reader.HeaderRecord[i],
                    reader.GetField<string>(i));

                result.Add(pair);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Projde všechny názvy sloupečků a zajistí, že mají unikátní názvy. Pokud je název sloupečku
        /// duplicitní, tak použije counter k změně jména.
        /// </summary>
        public static void MakeHeadersUnique(this CsvReader reader)
        {
            var existingNames = new List<string>();

            var length = reader.HeaderRecord.Length;
            for (var i = 0; i < length; i++)
            {
                var header = reader.HeaderRecord[i];
                var uniqueHeader = GetUniqueName(existingNames, header);
                reader.HeaderRecord[i] = uniqueHeader;

                existingNames.Add(uniqueHeader);
            }
        }

        /// <summary>
        /// Z předaného jména udělá unikátní za pomocí counteru
        /// </summary>
        private static string GetUniqueName(IEnumerable<string> existingNames, string name)
        {
            var newName = name;
            var index = 0;
            while (existingNames.Contains(newName))
            {
                index++;
                newName = $"{name}_{index}";
            }

            return newName;
        }
    }
}
