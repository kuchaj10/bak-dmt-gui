﻿using BlazorMonaco;
using DMTGui.Client.Models;

namespace DMTGui.Client.Extensions
{
    public static class ValidatorWarningExtensions
    {
        public static ModelDeltaDecoration ConvertToDecoration(this ValidatorWarning warning)
        {
            var warningDecoration = warning.Level switch
            {
                WarningLevel.Warning => GetWarningDecoration(warning),
                WarningLevel.Error => GetErrorDecoration(warning),
                _ => throw new InvalidOperationException(),
            };

            var warningRange = new BlazorMonaco.Range(
                warning.Postition.StartLineNumber,
                warning.Postition.StartLinePosition,
                warning.Postition.EndLineNumber,
                warning.Postition.EndLinePosition);

            warningDecoration.Range = warningRange;

            return warningDecoration;
        }

        private static ModelDeltaDecoration GetWarningDecoration(ValidatorWarning warning)
        {
            return new ModelDeltaDecoration
            {
                Options = new ModelDecorationOptions
                {
                    IsWholeLine = warning.FullLineWarning,
                    ClassName = "editorWarnLine",
                    GlyphMarginClassName = $"editorWarnIcon oi oi-pencil",
                    GlyphMarginHoverMessage = new[] { new MarkdownString { Value = warning.Message } },
                }
            };
        }

        private static ModelDeltaDecoration GetErrorDecoration(ValidatorWarning warning)
        {
            return new ModelDeltaDecoration
            {
                Options = new ModelDecorationOptions
                {
                    IsWholeLine = warning.FullLineWarning,
                    ClassName = "editorErrorLine",
                    GlyphMarginClassName = $"editorErrorIcon oi oi-pencil",
                    GlyphMarginHoverMessage = new[] { new MarkdownString { Value = warning.Message } },
                }
            };
        }
    }
}
