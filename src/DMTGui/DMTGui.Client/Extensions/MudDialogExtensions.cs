﻿using DMTGui.Client.Dialogs;
using Microsoft.AspNetCore.Components.Forms;
using MudBlazor;

namespace DMTGui.Client.Extensions
{
    public static class MudDialogExtensions
    {
        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="OkDialog"/>
        /// </summary>
        public static IDialogReference ShowOkDialog(this IDialogService service, string title, string message)
        {
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var pamameters = new DialogParameters
            {
                { nameof(OkDialog.DialogMessage), message }
            };

            return service.Show<OkDialog>(title, pamameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="YesNoDialog"/>
        /// </summary>
        public static IDialogReference ShowYesNoDialog(this IDialogService service, string title, string yesNoQuestion,
            Color? yesColor = null, string yesIcon = null)
        {
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var pamameters = new DialogParameters
            {
                { nameof(YesNoDialog.DialogMessage), yesNoQuestion }
            };

            if (yesColor is not null)
            {
                pamameters.Add(nameof(YesNoDialog.YesButtonColor), yesColor);
            }

            if (yesIcon is not null)
            {
                pamameters.Add(nameof(YesNoDialog.YesButtonIcon), yesIcon);
            }

            return service.Show<YesNoDialog>(title, pamameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="TextDialog"/>
        /// </summary>
        /// <param name="onSubmit">Funkce, která se zavolá při validním potvrzení dialogu</param>
        /// <param name="onSubmitValidation">Validace hodnoty vyplněné před potvrzením</param>
        public static IDialogReference ShowTextDialog(this IDialogService service, string title, string message, string originalMessage = null,
            Func<string, Task<bool>> onSubmit = null, Func<string, string> onSubmitValidation = null)
        {
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var pamameters = new DialogParameters
            {
                { nameof(TextDialog.DialogMessage), message }
            };

            if (!string.IsNullOrEmpty(originalMessage))
            {
                pamameters.Add(nameof(TextDialog.OriginalText), originalMessage);
            }

            if (onSubmit is not null)
            {
                pamameters.Add(nameof(TextDialog.OnSubmit), onSubmit);
            }

            if (onSubmitValidation is not null)
            {
                pamameters.Add(nameof(TextDialog.OnSubmitValidation), onSubmitValidation);
            }

            return service.Show<TextDialog>(title, pamameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="SelectItemDialog"/>
        /// </summary>
        public static IDialogReference ShowSelectItemDialog<T>(this IDialogService service, IEnumerable<T> items, string title)
        {
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var parameters = new DialogParameters
            {
                { nameof(SelectItemDialog<T>.Items), items },
                { nameof(SelectItemDialog<T>.IsMultiSelect), true }
            };

            return service.Show<SelectItemDialog<T>>(title, parameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="ModelDetailDialog"/>
        /// </summary>
        public static IDialogReference ShowModelDetailDialog<T>(this IDialogService service, string title, T item)
        {
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var pamameters = new DialogParameters
            {
                { nameof(ModelDetailDialog<T>.Item), item }
            };

            return service.Show<ModelDetailDialog<T>>(title, pamameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="DictionaryImportDialog"/>
        /// </summary>
        public static IDialogReference ShowDictionaryImportDialog(this IDialogService service, IBrowserFile file)
        {
            var message = "Import slovníku";
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true, MaxWidth = MaxWidth.ExtraLarge };

            var parametres = new DialogParameters
            {
                { nameof(DictionaryImportDialog.File), file }
            };

            return service.Show<DictionaryImportDialog>(message, parametres, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="CronDialog"/>
        /// </summary>
        public static IDialogReference ShowCronDialog(this IDialogService service, string defaultCron = "* * * * *")
        {
            var message = "Vytvoření triggeru";
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var pamameters = new DialogParameters
            {
                { nameof(CronDialog.DefaultCronValue), defaultCron }
            };

            return service.Show<CronDialog>(message, pamameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="SelectFilterDialog"/>. 
        /// </summary>
        public static IDialogReference ShowFilterDialog(this IDialogService service)
        {
            var message = "Výběr filtru záznamů";
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true, MaxWidth = MaxWidth.Large };

            var pamameters = new DialogParameters();

            return service.Show<SelectFilterDialog>(message, pamameters, options);
        }

        /// <summary>
        /// Vytvoří, nakonfiguruje a zobrazí <see cref="StatisticsDialog"/>. 
        /// </summary>
        public static IDialogReference ShowStatisticsDialog(this IDialogService service, string jobId)
        {
            var message = "Statistika";
            var options = new DialogOptions { FullWidth = true, CloseOnEscapeKey = true };

            var pamameters = new DialogParameters
            {
                { nameof(StatisticsDialog.JobId), jobId }
            };

            return service.Show<StatisticsDialog>(message, pamameters, options);
        }
    }
}
