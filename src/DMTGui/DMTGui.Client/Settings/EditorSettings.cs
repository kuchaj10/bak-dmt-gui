﻿namespace DMTGui.Client.Settings
{
    /// <summary>
    /// DTO pro nastavení editoru
    /// </summary>
    class EditorSettings
    {
        /// <summary>
        /// Přínak, zda se jedná o horizontální layout
        /// </summary>
        public bool IsLayoutHorizontal { get; set; }

        /// <summary>
        /// Přínak, zda je vidět panel s elementy
        /// </summary>
        public bool ElementsPaneCollapsed { get; set; }

        /// <summary>
        /// Příznak, zda je panel s výstupy vidět.
        /// </summary>
        public bool OutputPaneCollapsed { get; set; }
    }
}
