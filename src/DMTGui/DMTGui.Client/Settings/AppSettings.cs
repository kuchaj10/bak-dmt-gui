﻿namespace DMTGui.Client.Settings
{
    /// <summary>
    /// DTO pro ukládání nastavení aplikace
    /// </summary>
    class AppSettings
    {
        /// <summary>
        /// Příznak, zda se jedná o tmavý mód
        /// </summary>
        public bool IsDarkMode { get; set; }

        /// <summary>
        /// Příznak, že jke otevřená levá navigační lišta
        /// </summary>
        public bool IsLeftPanelOpen { get; set; }
    }
}
