﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("DMTGui.Client")]
[assembly: AssemblyDescription("Klient grafického rozhraní pro DMT")]
[assembly: AssemblyCompany("ICZ a.s.")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("ICZ a.s. 2022")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0.0+0")]

[assembly: InternalsVisibleTo("DMTGui.Tests")] 