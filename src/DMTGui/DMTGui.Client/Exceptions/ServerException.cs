﻿using DMTGui.Shared.Models;

namespace DMTGui.Client.Exceptions
{
	/// <summary>
	/// Chyba, která nastala na straně serveru. Slouží k rozpoznání interní chyb, které se podařilo odchytit a na které aplikace reagovala.
	/// </summary>
	public class ServerException : Exception
	{
		/// <summary>
		/// Detaily chyby
		/// </summary>
		public ErrorDetail ErrorDetail { get; }

		public ServerException(ErrorDetail errorDetail)
		{
			ErrorDetail = errorDetail;
		}
	}
}
