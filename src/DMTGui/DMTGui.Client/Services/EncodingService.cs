﻿using System.Text;
using System.Text.RegularExpressions;

namespace DMTGui.Client.Services
{
	/// <summary>
	/// Služba na pomoc s encodingy podporované aplikací
	/// </summary>
	public class EncodingService
	{
		private readonly Encoding[] _availableEncodings;

		public EncodingService()
		{
			Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

			_availableEncodings = new Encoding[]
			{
				Encoding.UTF8,
				Encoding.GetEncoding(1250),
				Encoding.GetEncoding("ISO-8859-1"),
				Encoding.GetEncoding("CP852")
			};
		}

		/// <summary>
		/// Vrátí seznam podporovaných encondings
		/// </summary>
		public Encoding[] GetAvailableEncodings()
		{
			return _availableEncodings;
		}

		/// <summary>
		/// Vrátí názvy podporovaných encodings
		/// </summary>
		public string[] GetAvailableEncodingNames()
		{
			return _availableEncodings
				.Select(FormatName)
				.ToArray();
		}

		/// <summary>
		/// Vrátí encoding podle předaného jména. Funkce počítá s názvem, 
		/// který byl získaný z metody <see cref="GetAvailableEncodingNames"/>.
		/// </summary>
		public Encoding GetEncodingByName(string encodingName)
		{
			return ParseEncoding(encodingName);
		}

		private static Encoding ParseEncoding(string encodingName)
		{
			var match = Regex.Match(encodingName, @"([0-9]+)");
			if (match.Success)
			{
				var codepage = int.Parse(match.Groups[1].ToString());

				return Encoding.GetEncoding(codepage);
			}

			return Encoding.GetEncoding(encodingName);
		}

		private static string FormatName(Encoding encoding)
		{
			return $"{encoding.CodePage} ({encoding.WebName})";
		}
	}
}
