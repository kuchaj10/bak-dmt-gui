﻿using DMTGui.Shared.Extensions;
using Microsoft.JSInterop;
using System.Text;

namespace DMTGui.Client.Services
{
    /// <summary>
    /// Služba na pomoc s ukládáním souborů
    /// </summary>
    public class SaveFileService
    {
        private readonly IJSRuntime _jsRuntime;

        public SaveFileService(IJSRuntime jsRuntime)
        {
            _jsRuntime = jsRuntime;
        }

        /// <summary>
        /// Vybídne k uložení předaného textového souboru
        /// </summary>
        public async Task SaveAsAsync(string filename, string text)
        {
            text.ThrowIfEmpty();

            await SaveAsAsync(filename, Encoding.UTF8.GetBytes(text));
        }

        /// <summary>
        /// Vybídne k uložení předaného binárního souboru
        /// </summary>
        public async Task SaveAsAsync(string filename, byte[] data)
        {
            data.ThrowIfNull();

            await _jsRuntime.InvokeAsync<object>("saveAsFile", filename, Convert.ToBase64String(data));
        }
    }
}
