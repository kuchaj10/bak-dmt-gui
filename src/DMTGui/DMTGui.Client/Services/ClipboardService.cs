﻿using Microsoft.JSInterop;

namespace DMTGui.Client.Services
{
    /// <summary>
    /// Služba pro manipulaci s uživatelskou schránkou
    /// </summary>
    public class ClipboardService
    {
        private readonly ILogger<ClipboardService> _logger;
        private readonly IJSRuntime _jSRuntime;

        public ClipboardService(ILogger<ClipboardService> logger, IJSRuntime jSRuntime)
        {
            _logger = logger;
            _jSRuntime = jSRuntime;
        }

        /// <summary>
        /// Zkopíruje předaný <paramref name="textToCopy"/> do schránky
        /// </summary>
        public async Task CopyTextToClipboardAsync(string textToCopy)
        {
            try
            {
                await _jSRuntime.InvokeVoidAsync("CopyText", textToCopy);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Nastala chyba při kopírování do schránky.");
            }
        }
    }
}
