﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.Excel;
using DMTGui.Shared.Extensions;
using System.Globalization;

namespace DMTGui.Client.Services
{
    /// <summary>
    /// Služba sloužící k pomoci s manipulací s tabulkovými daty za pomoci <see cref="CsvHelper"/>.
    /// </summary>
    public class DataTableService
    {
        /// <summary>
        /// Export předaného enumerable do csv, které vrátí jako binární data.
        /// </summary>
        public async Task<byte[]> ExportToCsvAsync<TData>(IEnumerable<TData> dataToExport)
        {
            dataToExport.ThrowIfNull();

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";"
            };

            using var stream = new MemoryStream();
            using var writer = new StreamWriter(stream);
            using var csv = new CsvWriter(writer, config);

            await csv.WriteRecordsAsync(dataToExport);
            await csv.FlushAsync();

            return stream.ToArray();
        }

        /// <summary>
        /// Export předaného enumerable do excel, které vrátí jako binární data.
        /// </summary>
        public async Task<byte[]> ExportToCsvAsync(IEnumerable<dynamic> dataToExport)
        {
            dataToExport.ThrowIfNull();

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";"
            };

            using var stream = new MemoryStream();
            using var writer = new StreamWriter(stream);
            using var csv = new CsvWriter(writer, config);

            await csv.WriteRecordsAsync(dataToExport);
            await csv.FlushAsync();

            return stream.ToArray();
        }

        public async Task<byte[]> ExportToExcelAsync<TData>(IEnumerable<TData> dataToExport)
        {
            dataToExport.ThrowIfNull();

            using var stream = new MemoryStream();

            // Po zapsání je potřeba uzavřít excel writer
            using (var excel = new ExcelWriter(stream, CultureInfo.InvariantCulture))
            {
                await excel.WriteRecordsAsync(dataToExport);
                await excel.FlushAsync();
            }

            return stream.ToArray();
        }

        /// <summary>
        /// Export předaného enumerable do stringu s oddělovačem jako tab. 
        /// Vhodné pro export dat do uživatelské schránky. Aplikace jako excel je 
        /// schopná data v takovém formátu vložit.
        /// </summary>
        public async Task<string> ExportToStringAsync<TData>(IEnumerable<TData> dataToExport)
        {
            dataToExport.ThrowIfNull();

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = "\t"
            };

            using var writer = new StringWriter();
            using var csv = new CsvWriter(writer, config);

            await csv.WriteRecordsAsync(dataToExport);
            await csv.FlushAsync();

            return writer.ToString();
        }
    }
}
