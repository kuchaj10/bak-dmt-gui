﻿using Microsoft.JSInterop;
using System.Text.Json;

namespace DMTGui.Client.Services
{
    public interface ILocalStorage
    {
        /// <summary>
        /// Uloží předanou hodnotu <paramref name="value"/> pod 
        /// klíčem <paramref name="key"/> do uložiště prohlížeče.
        /// </summary>
        Task SetValueAsync<TValue>(string key, TValue value);

        /// <summary>
        /// Vrátí hodnotu nalezenou pod předaným klíčem <paramref name="key"/> 
        /// z uložiště prohlížeče. Pokud hodnota není nalezena, tak vrátí default pro daný typ hodnoty. 
        /// </summary>
        Task<TValue> GetValueAsync<TValue>(string key);

        /// <summary>
        /// Vrátí hodnotu nalezenou pod předaným klíčem <paramref name="key"/> 
        /// z uložiště prohlížeče. Pokud hodnota není nalezena, tak vrátí předaný default.
        /// </summary>
        Task<TValue> GetValueAsync<TValue>(string key, TValue defaultValue);
    }

    /// <summary>
    /// Služba na manipulaci proměnnými uloženými v uložišti prohlížeče.
    /// </summary>
    public class LocalStorage : ILocalStorage
    {
        private readonly IJSRuntime _jsRuntime;
        private readonly ILogger<LocalStorage> _logger;

        public LocalStorage(IJSRuntime jsRuntime, ILogger<LocalStorage> logger)
        {
            _jsRuntime = jsRuntime;
            _logger = logger;
        }

        /// <inheritdoc/>
        public async Task<TValue> GetValueAsync<TValue>(string key)
        {
            var valueAsJson = await _jsRuntime.InvokeAsync<string>("GetValueFromLocalStorage", key);
            if (valueAsJson == null) 
                return default;

            _logger.LogDebug($"Z LocalStorage pod klíčem {key} získávám hodnotu:\n{valueAsJson}");

            return JsonSerializer.Deserialize<TValue>(valueAsJson);
        }

        /// <inheritdoc/>
        public async Task<TValue> GetValueAsync<TValue>(string key, TValue defaultValue)
        {
            var valueAsJson = await _jsRuntime.InvokeAsync<string>("GetValueFromLocalStorage", key);
            if (valueAsJson == null)
                return defaultValue;

            _logger.LogDebug($"Z LocalStorage pod klíčem {key} získávám hodnotu:\n{valueAsJson}");

            return JsonSerializer.Deserialize<TValue>(valueAsJson);
        }

        /// <inheritdoc/>
        public async Task SetValueAsync<TValue>(string key, TValue value)
        {
           if (value is null) 
                throw new ArgumentNullException(nameof(value));

           var valueAsJson = JsonSerializer.Serialize(value);

            _logger.LogDebug($"Do LocalStorage pod klíčem {key} ukládám hodnotu:\n{valueAsJson}");

            await _jsRuntime.InvokeAsync<string>("SetValueToLocalStorage", key, valueAsJson);
        }
    }
}
