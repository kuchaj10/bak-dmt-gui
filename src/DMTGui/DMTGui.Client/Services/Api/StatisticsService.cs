﻿using DMTGui.Shared.Contracts;
using DMTGui.Shared.Models;

namespace DMTGui.Client.Services.Api
{
    public class StatisticsService : HttpService<string, MigrationStatisticsDto, StatisticsSearchDto>, IStatisticsContract
    {
        private ILogger _logger;

        public StatisticsService(ILogger<StatisticsService> logger, HttpClient client) : base(logger, client, "api/statistics")
        {
            _logger = logger;
        }
    }
}
