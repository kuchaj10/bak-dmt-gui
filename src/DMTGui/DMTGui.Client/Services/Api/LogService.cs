﻿using DMTGui.Shared.Models;
using System.Net.Http.Json;
using DMTGui.Shared.Contracts;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Utils;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// Služba pro manipulaci se chybami z migrace
    /// </summary>
    public class LogService : ILogContract
    {
        private readonly HttpClient _httpClient;

        private const string _relativeUri = "api/logs";

        public LogService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<LogDto>> GetLogs(LogSearchDto logSearch = null)
        {
            var searchUri = HttpUtils.CreateUriFromModel(logSearch);

            var requestedUri = string.IsNullOrEmpty(searchUri) ? _relativeUri : $"{_relativeUri}?{searchUri}";

            var logs = await _httpClient.GetFromJsonAsync<LogDto[]>(requestedUri);

            return logs ?? Array.Empty<LogDto>();
        }

        /// <inheritdoc />
        public async Task<int> GetLogsCount(LogSearchDto logSearch = null)
        {
            var searchUri = HttpUtils.CreateUriFromModel(logSearch);

            var requestedUri = string.IsNullOrEmpty(searchUri) ? $"{_relativeUri}/count" : $"{_relativeUri}/count?{searchUri}";

            var count = await _httpClient.GetFromJsonAsync<int>(requestedUri);

            return count;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<string>> GetMigrationStates()
        {
            var states = await _httpClient.GetFromJsonAsync<string[]>($"{_relativeUri}/states");

            return states ?? Array.Empty<string>();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<string>> GetLogLevels()
        {
            var levels = await _httpClient.GetFromJsonAsync<string[]>($"{_relativeUri}/levels");

            return levels ?? Array.Empty<string>();
        }
    }
}
