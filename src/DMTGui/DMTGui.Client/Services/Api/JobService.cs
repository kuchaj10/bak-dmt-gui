﻿using DMTGui.Client.Extensions;
using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using System.Web;
using DMTGui.Shared.Contracts;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// Služba pro práci s úlohami
    /// </summary>
    public class JobService : HttpService<string, JobDto, SearchDto>, IJobContract
    {
        private readonly ILogger _logger;

        public JobService(ILogger<JobService> logger, HttpClient httpClient) : base(logger, httpClient, "api/jobs")
        {
            logger.ThrowIfNull();

            _logger = logger;
        }

        /// <inheritdoc />
        public async Task StartJob(string jobId)
        {
            jobId.ThrowIfEmpty();

            _logger.LogDebug($"Startuji job s identifikátorem '{jobId}'");

            var jobIdEncoded = jobId.ToString();

            var content = new StringContent("", System.Text.Encoding.UTF8, "application/json");
            var result = await HttpClient.PutAsync($"{RelativeUri}/{jobIdEncoded}/run/true", content);
            await result.ThrowIfNotSuccessAsync();
        }

        /// <inheritdoc />
        public async Task StopJob(string jobId)
        {
            jobId.ThrowIfEmpty();

            _logger.LogDebug($"Zastavuji job s identifikátorem '{jobId}'");

            var jobIdEncoded = jobId.ToString();

            var content = new StringContent("", System.Text.Encoding.UTF8, "application/json");
            var result = await HttpClient.PutAsync($"{RelativeUri}/{jobIdEncoded}/run/false", content);
            await result.ThrowIfNotSuccessAsync();
        }

        /// <inheritdoc />
        public async Task EnableJob(string jobId)
        {
            jobId.ThrowIfEmpty();

            _logger.LogDebug($"Zapínám job s identifikátorem '{jobId}'");

            var jobIdEncoded = jobId.ToString();

            var content = new StringContent("", System.Text.Encoding.UTF8, "application/json");
            var result = await HttpClient.PutAsync($"{RelativeUri}/{jobIdEncoded}/enable/true", content);
            await result.ThrowIfNotSuccessAsync();
        }

        /// <inheritdoc />
        public async Task DisableJob(string jobId)
        {
            jobId.ThrowIfEmpty();

            _logger.LogDebug($"Vypínám job s identifikátorem '{jobId}'");

            var jobIdEncoded = jobId.ToString();

            var content = new StringContent("", System.Text.Encoding.UTF8, "application/json");
            var result = await HttpClient.PutAsync($"{RelativeUri}/{jobIdEncoded}/enable/false", content);
            await result.ThrowIfNotSuccessAsync();
        }
    }
}
