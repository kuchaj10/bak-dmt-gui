﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Contracts;
using System.Net.Http.Json;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// Služba pro manipulaci s profily
    /// </summary>
    public class ProfileService : HttpService<string, ProfileDto, SearchDto>, IProfileContract
    {
        private readonly ILogger _logger;

        public ProfileService(ILogger<ProfileService> logger, HttpClient client) : base(logger, client, "api/profiles")
        {
            _logger = logger;
        }

        /// <inheritdoc />
        public async Task<ProfileDto> GetDefault()
        {
            _logger.LogDebug($"Získávám default profil");

            var result = await HttpClient.GetAsync($"{RelativeUri}/default");
            if (!result.IsSuccessStatusCode)
                return default;

            return await result.Content.ReadFromJsonAsync<ProfileDto>();
        }
    }
}
