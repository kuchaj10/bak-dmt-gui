﻿using DMTGui.Client.Extensions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Contracts;
using System.Net.Http.Json;
using System.Web;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// HTTP service implementující chování pro hromadné CRUD operace.
    /// </summary>
    /// <typeparam name="TIdentifier">Typ identifikátoru objektu, se kterým služba pracuje</typeparam>
    /// <typeparam name="TType">Typ objekty, se kterým služba pracuje</typeparam>
    public abstract class HttpBulkService<TIdentifier, TType, TSearch> : HttpService<TIdentifier, TType, TSearch>, ICrudBulkContract<TIdentifier, TType>
        where TSearch : class
    {
        private readonly ILogger _logger;

        protected HttpBulkService(ILogger logger, HttpClient client, string relativeUri) : base(logger, client, relativeUri)
        {
            logger.ThrowIfNull();
            client.ThrowIfNull();
            relativeUri.ThrowIfEmpty();

            _logger = logger;
            HttpClient = client;
            RelativeUri = relativeUri;
        }

        /// <inheritdoc />
        public async Task AddBulk(IEnumerable<TType> itemsToAdd)
        {
            itemsToAdd.ThrowIfEmpty();

            _logger.LogDebug("Přidávám hromadně několik objektů");

            var result = await HttpClient.PostAsJsonAsync($"{RelativeUri}/bulk", itemsToAdd);
            result.ThrowIfNotSuccess();
        }

        /// <inheritdoc />
        public async Task UpdateBulk(IEnumerable<TType> itemsToUpdate)
        {
            itemsToUpdate.ThrowIfEmpty();

            _logger.LogDebug("Aktualizuji hromadně několik objektů");

            var result = await HttpClient.PutAsJsonAsync($"{RelativeUri}/bulk", itemsToUpdate);
            result.ThrowIfNotSuccess();
        }
        /// <inheritdoc />
        public async Task DeleteBulk(IEnumerable<TIdentifier> identifiersToDelete)
        {
            identifiersToDelete.ThrowIfEmpty();

            _logger.LogDebug("Mažu hromadně několik objektů");

            // TODO (Jakub): Zamyslet se, jak to udělat líp. Třeba i pomocí 100 parametrů ke smazání najednou.
            foreach (var identifier in identifiersToDelete)
            {
                var identifierEncoded = HttpUtility.UrlEncode(identifier.ToString());

                await HttpClient.DeleteAsync($"{RelativeUri}/{identifierEncoded}");
            }
        }
    }
}
