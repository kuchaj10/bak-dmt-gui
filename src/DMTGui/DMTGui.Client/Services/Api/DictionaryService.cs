﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Contracts;
using System.Net.Http.Json;
using DMTGui.Shared.Utils;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// Služba pro manipulaci se slovníky
    /// </summary>
    public class DictionaryService : HttpService<string, DictionaryDto, SearchDto>, IDictionaryContract
    {
        private readonly ILogger _logger;

        public DictionaryService(ILogger<DictionaryService> logger, HttpClient client) : base(logger, client, "api/dictionaries")
        {
            _logger = logger;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<DictionaryDto>> GetAllNames(SearchDto search = null)
        {
            _logger.LogDebug($"Získávám všechny položky ({search})");

            var searchUri = HttpUtils.CreateUriFromModel(search);
            var requestedUri = $"{RelativeUri}/names";
            requestedUri = string.IsNullOrEmpty(searchUri) ? requestedUri : $"{requestedUri}?{searchUri}";

            var items = await HttpClient.GetFromJsonAsync<DictionaryDto[]>(requestedUri);

            return items ?? Enumerable.Empty<DictionaryDto>();
        }
    }
}
