﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Extensions;
using System.Net.Http.Json;
using System.Web;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// Služba pro manipulaci s záznamy migrace
    /// </summary>
    public class RecordService : IRecordContract
    {
        private readonly HttpClient _httpClient;

        private const string _relativeUri = "api/records";

        public RecordService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<RecordDto>> GetRecords()
        {
            var jobs = await _httpClient.GetFromJsonAsync<RecordDto[]>(_relativeUri);

            return jobs ?? new RecordDto[0];
        }

        /// <inheritdoc />
        public async Task<IEnumerable<RecordDto>> GetRecordsByProfileName(string profileName)
        {
            if (string.IsNullOrEmpty(profileName))
                throw new ArgumentNullException(nameof(profileName));

            profileName = HttpUtility.UrlEncode(profileName);

            var jobs = await _httpClient.GetFromJsonAsync<RecordDto[]>($"{_relativeUri}?profileName={profileName}");

            return jobs ?? new RecordDto[0];
        }

        /// <inheritdoc />
        public async Task<IEnumerable<RecordDto>> GetRecordsByJobId(Guid jobId)
        {
            var jobIdString = jobId.ToBase64String();
            var jobs = await _httpClient.GetFromJsonAsync<RecordDto[]>($"{_relativeUri}?jobId={jobIdString}");

            return jobs ?? new RecordDto[0];
        }

        /// <inheritdoc />
        public async Task<IEnumerable<string>> GetUsedStates()
        {
            var usedStates = await _httpClient.GetFromJsonAsync<IEnumerable<string>>($"{_relativeUri}/states"); ;

            return usedStates ?? new string[0];
        }
    }
}
