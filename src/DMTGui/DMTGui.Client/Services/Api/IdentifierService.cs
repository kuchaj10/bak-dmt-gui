﻿using DMTGui.Shared.Models;
using DMTGui.Shared.Contracts;
using System.Net.Http.Json;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// Služba pro manipulaci s identifikátory
    /// </summary>
    public class IdentifierService : HttpBulkService<int, IdentifierDto, SearchDto>, IIdentifierContract
    {
        public IdentifierService(ILogger<IdentifierService> logger, HttpClient client) : base(logger, client, "api/identifiers")
        {
        }

        /// <inheritdoc />
        public async Task<IEnumerable<string>> GetUsedStates()
        {
            var usedStates = await HttpClient.GetFromJsonAsync<IEnumerable<string>>($"{RelativeUri}/states");

            return usedStates ?? Array.Empty<string>();
        }
    }
}
