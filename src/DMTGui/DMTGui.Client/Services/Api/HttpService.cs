﻿using DMTGui.Client.Extensions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Contracts;
using System.Net.Http.Json;
using System.Web;
using DMTGui.Shared.Utils;

namespace DMTGui.Client.Services.Api
{
    /// <summary>
    /// HTTP service implemetující základní CRUD operace
    /// </summary>
    /// <typeparam name="TIdentifier">Typ identifikátoru objektu, se kterým služba pracuje</typeparam>
    /// <typeparam name="TType">Typ objekty, se kterým služba pracuje</typeparam>
    /// <typeparam name="TSearch">Typ query objektu, který se používá k vyhledávání</typeparam>
    public abstract class HttpService<TIdentifier, TType, TSearch> : ICrudContract<TIdentifier, TType, TSearch>
        where TSearch : class
    {
        private readonly ILogger _logger;
        protected string RelativeUri { get; set; }
        protected HttpClient HttpClient { get; set; }

        protected HttpService(ILogger logger, HttpClient httpClient, string relativeUri)
        {
            logger.ThrowIfNull();
            httpClient.ThrowIfNull();
            relativeUri.ThrowIfEmpty();

            _logger = logger;

            HttpClient = httpClient;
            RelativeUri = relativeUri;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<TType>> GetAll(TSearch search = null)
        {
            _logger.LogDebug($"Získávám všechny položky ({search})");

            var searchUri = HttpUtils.CreateUriFromModel(search);
            var requestedUri = string.IsNullOrEmpty(searchUri) ? RelativeUri : $"{RelativeUri}?{searchUri}";

            var items = await HttpClient.GetFromJsonAsync<TType[]>(requestedUri);

            return items ?? Enumerable.Empty<TType>();
        }

        /// <inheritdoc />
        public async Task<TType> Get(TIdentifier identifier)
        {
            identifier.ThrowIfNull();

            _logger.LogDebug($"Získávám položku pro identifikátor '{identifier}'");

            var identifierEncoded = identifier.ToString();

            var result = await HttpClient.GetAsync($"{RelativeUri}/{identifierEncoded}");
            result.ThrowIfNotSuccess();

            return await result.Content.ReadFromJsonAsync<TType>();
        }

        /// <inheritdoc />
        public async Task<TType> Add(TType item)
        {
            item.ThrowIfNull();

            _logger.LogDebug($"Přidávám novou položku '{item}'");

            var result = await HttpClient.PostAsJsonAsync(RelativeUri, item);
            result.ThrowIfNotSuccess();

            return await result.Content.DeserializeAsync<TType>();
        }

        /// <inheritdoc />
        public async Task Update(TIdentifier identifier, TType item)
        {
            item.ThrowIfNull();

            _logger.LogDebug($"Aktualizuji položku '{item}' s identifikátorem '{identifier}'");

            var identifierEncoded = identifier.ToString();

            var result = await HttpClient.PutAsJsonAsync($"{RelativeUri}/{identifierEncoded}", item);
            result.ThrowIfNotSuccess();
        }

        /// <inheritdoc />
        public async Task Delete(TIdentifier identifier)
        {
            identifier.ThrowIfNull();

            _logger.LogDebug($"Mažu položku s identifikátorem '{identifier}'");

            var identifierEncoded = identifier.ToString();

            var result = await HttpClient.DeleteAsync($"{RelativeUri}/{identifierEncoded}");
            result.ThrowIfNotSuccess();
        }
    }
}
