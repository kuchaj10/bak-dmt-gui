﻿using System.Xml;
using DMTGui.Client.Extensions;
using DMTGui.Client.Models;
using DMTGui.Client.Validators;
using DMTGui.Client.Validators.ValidatorActions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;

namespace DMTGui.Client.Services
{
    public interface IProfileValidatorService
    {
        /// <summary>
        /// Přidání validace atributu
        /// </summary>
        void AddValidation(IAttributeValidatorAction action);

        /// <summary>
        /// Přidání validace elementu
        /// </summary>
        void AddValidation(IElementValidatorAction action);

        /// <summary>
        /// Ověří tvar migračního profilu, nikoli jeho obsah
        /// </summary>
        bool CheckProfileXmlIsValid(string profileAsXml, out List<ValidatorWarning> warnings);

        /// <summary>
        /// Z předaného xml získá všechny elementy, které nejsou ignorované <paramref name="ignoredElements"/>, do zadané hlouby <paramref name="depth"/>. 
        /// </summary>
        List<ElementInfo> ExtractElementsByDepth(string xml, int depth, IEnumerable<string> ignoredElements);

        /// <summary>
        /// Kontrola předaného profilu proti nastaveným kontrolám z 
        /// <see cref="AddValidation(IAttributeValidatorAction)"/> a 
        /// <see cref="AddValidation(IElementValidatorAction)"/>.
        /// </summary>
        List<ValidatorWarning> ValidateProfile(string profileAsXml);
    }

    public class ProfileValidatorService : IProfileValidatorService
    {
        private ILogger<ProfileValidatorService> _logger;

        private XmlValidator<MigrationProfile> _profileXmlValidator;
        private ProfileValidator _profileValidator;

        public ProfileValidatorService(ILogger<ProfileValidatorService> logger)
        {
            _logger = logger;

            _profileXmlValidator = new XmlValidator<MigrationProfile>();
            _profileValidator = new ProfileValidator();
        }

        /// <inheritdoc/>
        public void AddValidation(IElementValidatorAction action)
        {
            action.ThrowIfNull();
            _profileValidator.AddValidatorAction(action);
        }

        /// <inheritdoc/>
        public void AddValidation(IAttributeValidatorAction action)
        {
            action.ThrowIfNull();
            _profileValidator.AddValidatorAction(action);
        }

        /// <inheritdoc/>
        public bool CheckProfileXmlIsValid(string profileAsXml, out List<ValidatorWarning> warnings)
        {
            profileAsXml.ThrowIfNull();
            return _profileXmlValidator.CheckIsValidXml(profileAsXml, out warnings);
        }

        /// <inheritdoc/>
        public List<ValidatorWarning> ValidateProfile(string profileAsXml)
        {
            profileAsXml.ThrowIfNull();

            var warnings = new List<ValidatorWarning>();

            //if (!_profileXmlValidator.CheckIsValidXml(profileAsXml, out warnings))
            //    return warnings;

            warnings.AddRange(_profileXmlValidator.Validate(profileAsXml, out var wrongXml));

            if (!wrongXml)
            {
                warnings.AddRange(_profileValidator.Validate(profileAsXml));
            }

            return warnings;
        }

        /// <inheritdoc/>
        public List<ElementInfo> ExtractElementsByDepth(string xml, int depth, IEnumerable<string> ignoredElements)
        {
            ignoredElements.ThrowIfNull();

            var extractedElements = new List<ElementInfo>();

            if (string.IsNullOrEmpty(xml))
                return extractedElements;

            try
            {
                using var stringReader = new StringReader(xml);
                using var xmlReader = XmlReader.Create(stringReader);

                while (xmlReader.Read())
                {
                    if (xmlReader.NodeType != XmlNodeType.Element)
                        continue;

                    // Pokud už mě dál nezajímá obsah tohoto elementu, tak ho přeskočím a jdu dál
                    if (xmlReader.Depth > depth || ignoredElements.Contains(xmlReader.Name))
                    {
                        xmlReader.Skip();
                        continue;
                    }

                    extractedElements.Add(xmlReader.GetElement());
                }
            }
            catch (Exception ex)
            {
                // Očekávaná chyba
                _logger.LogDebug(ex, "Nastala chyba při parsování xml souboru");
            }


            return extractedElements;
        }
    }

}
