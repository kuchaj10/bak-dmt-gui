﻿function SetValueToLocalStorage(key, value) {
    localStorage.setItem(key, value);
}

function GetValueFromLocalStorage(key) {
    return localStorage.getItem(key);
}
