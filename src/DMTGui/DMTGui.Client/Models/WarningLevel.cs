﻿namespace DMTGui.Client.Models
{
    /// <summary>
    /// Level upozornění
    /// </summary>
    public enum WarningLevel
    {
        Warning = 0,
        Error = 1,
    }
}
