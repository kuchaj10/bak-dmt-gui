﻿namespace DMTGui.Client.Models
{
    /// <summary>
    /// DTO pro uložení validačního upozornění
    /// </summary>
    public class ValidatorWarning
    {
        /// <summary>
        /// Úroveň upozornění
        /// </summary>
        public WarningLevel Level { get; set; } = WarningLevel.Warning;

        /// <summary>
        /// Příznak, zda se jedná o celořádkovou chybu
        /// </summary>
        public bool FullLineWarning { get; set; }

        /// <summary>
        /// Umístění chyby v editoru
        /// </summary>
        public PositionInfo Postition { get; set; } = PositionInfo.Zero;

        /// <summary>
        /// Zpráva upozornění, viditelná v editoru
        /// </summary>
        public string Message { get; set; }
    }
}
