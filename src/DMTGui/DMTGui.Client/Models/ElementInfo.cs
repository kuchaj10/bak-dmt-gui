﻿namespace DMTGui.Client.Models
{
    /// <summary>
    /// DTO uchovávající informace o elementu
    /// </summary>
    public class ElementInfo
    {
        /// <summary>
        /// Název elementu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Hlouba elementu v XML
        /// </summary>
        public int Depth { get; set; }

        /// <summary>
        /// Pozice elemetu v XML 
        /// </summary>
        public PositionInfo Position { get; set; }

        /// <summary>
        /// Attributy elementu
        /// </summary>
        public IEnumerable<AttributeInfo> Attributes { get; set; }
    }
}
