﻿using System.Xml;

namespace DMTGui.Client.Models
{
    /// <summary>
    /// Model pro uložení pozice
    /// </summary>
    public struct PositionInfo
    {
        public int StartLineNumber { get; set; }
        public int StartLinePosition { get; set; }
        public int EndLineNumber { get; set; }
        public int EndLinePosition { get; set; }

        public PositionInfo()
        {
            StartLineNumber = 0;
            StartLinePosition = 0;
            EndLineNumber = 0;
            EndLinePosition = 0;
        }

        public PositionInfo(int LineNumber, int LinePosition)
        {
            StartLineNumber = LineNumber;
            StartLinePosition = LinePosition;
            EndLineNumber = LineNumber;
            EndLinePosition = LinePosition;
        }

        public static PositionInfo Zero => new PositionInfo
        {
            StartLineNumber = 0,
            StartLinePosition = 0,
            EndLineNumber = 0,
            EndLinePosition = 0
        };
    }
}
