﻿using MudBlazor;

namespace DMTGui.Client.Models
{
    public class MudChartData
    {
        public ChartOptions Options { get; set; }

        public List<ChartSeries> Series { get; set; }

        public string[] XAxisLabels { get; set; }
    }
}
