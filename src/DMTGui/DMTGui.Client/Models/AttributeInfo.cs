﻿namespace DMTGui.Client.Models
{
    /// <summary>
    /// DTO pro uchovávání informací o atributu
    /// </summary>
    public class AttributeInfo
    {
        /// <summary>
        /// Název atributu
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Hodnota atributu
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Pozice atributu v XML 
        /// </summary>
        public PositionInfo AttributePosition { get; set; }

        /// <summary>
        /// Pozici hednoty attributu v XML
        /// </summary>
        public PositionInfo ValuePosition { get; set; }
    }
}
