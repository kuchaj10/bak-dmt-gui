﻿using DMTGui.Shared.Models;

namespace DMTGui.Client.Models
{
    public class LogFilterEntry
    {
        public string Name { get; set; }

        public LogSearchDto Search { get; set; }

        public LogFilterEntry()
        {
            Search = new LogSearchDto();
        }
    }

    public class LogsFilterData
    {
        public LogFilterEntry[] Entries { get; set; }

        public LogsFilterData()
        {
            Entries = Array.Empty<LogFilterEntry>();
        }
    }
}
