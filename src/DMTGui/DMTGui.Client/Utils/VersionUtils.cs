﻿using System.Diagnostics;
using System.Reflection;

namespace DMTGui.Client.Utils
{
    /// <summary>
    /// Nástroje pro získání verze běžící aplikace.
    /// </summary>
    public class VersionUtils
    {
        /// <summary>
        /// Vrátí verzi aplikace ve formátu "v0.0.0" pro major manor a patch.
        /// </summary>
        public static string GetVersionString()
        {
            var assemblyName = Assembly.GetExecutingAssembly().GetName();
            var version = assemblyName.Version;

            if (version is null)
                return "v0.0.0";

            return $"v{version.Major}.{version.Minor}.{version.Build}";
        }

        /// <summary>
        /// Vrátí příznak toho, jestli aplikace běží v produkčním prostředí či nikoli.
        /// </summary>
        public static bool CheckIsProduction()
        {
            var variableValue = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "";

            return variableValue.Contains("Production");
        }
    }
}