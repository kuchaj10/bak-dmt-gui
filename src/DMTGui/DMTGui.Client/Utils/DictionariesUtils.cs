﻿using CsvHelper;
using CsvHelper.Configuration;
using DMTGui.Client.Extensions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using System.Dynamic;
using System.Globalization;
using System.Text;

namespace DMTGui.Client.Utils
{
    /// <summary>
    /// Nástroje pro manipulaci s migračními slovníky
    /// </summary>
    public static class DictionariesUtils
    {
        /// <summary>
        /// Vezme předané csv v očekávaném tvaru a rozparsuje na <see cref="DictionaryDto"/>. 
        /// Příznak <paramref name="containsDescription"/> definuje, zda se mají očekávat popisky.
        /// </summary>
        /// <example>
        /// -- Očekávaný tvar:
        /// 
        /// Klíč   | Barva  | Chuť   |
        /// --------------------------
        /// Banán  | Žlutá  | Sladká |
        /// Jablko | Zelená | Kyselá |
        /// 
        /// -- Očekávaný tvar (s popisky):
        /// 
        /// Klíč   | Popis | Barva  | Popis | Chuť   | Popis |
        /// --------------------------------------------------
        /// Banán  |       | Žlutá  |       | Sladká |       | 
        /// Jablko |       | Zelená |       | Kyselá |       |
        /// 
        /// </example>
        public static async Task<DictionaryDto> ParseDictionaryFromCsvAsync(string dataToImport, string dictionaryName, int limit, bool skipEmpty, bool containsDescription, string delimiter, Encoding encoding)
        {
            if (containsDescription)
            {
                return await ParseDictionaryFromCsvWithDescriptionAsync(dataToImport, dictionaryName, limit, skipEmpty, delimiter, encoding);
            }
            else
            {
                return await ParseDictionaryFromCsvAsync(dataToImport, dictionaryName, limit, skipEmpty, delimiter, encoding);
            }
        }

        private static async Task<DictionaryDto> ParseDictionaryFromCsvAsync(string dataToImport, string dictionaryName, int limit, bool skipEmpty, string delimiter, Encoding encoding)
        {
            dataToImport.ThrowIfEmpty();
            dictionaryName.ThrowIfEmpty();

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
                Delimiter = delimiter,
                Encoding = encoding,
                IgnoreBlankLines = true,
                Mode = CsvMode.RFC4180,
                NewLine = "\r\n",
                MissingFieldFound = null,
            };

            using var reader = new StringReader(dataToImport);
            using var csv = new CsvReader(reader, config);

            csv.Read();
            csv.ReadHeader();
            csv.MakeHeadersUnique();

            var entries = new List<DictionaryEntryDto>();

            var index = 0;
            while (await csv.ReadAsync())
            {
                index++;

                var fields = csv.GetFields();

                var entryKey = fields[0].Value;

                var properties = fields
                    .Skip(1)
                    .Where(fld => !skipEmpty || !string.IsNullOrEmpty(fld.Value))
                    .Select(fld => new DictionaryEntryPropertyDto { Key = fld.Key, Value = fld.Value })
                    .ToArray();

                var entry = new DictionaryEntryDto { Key = entryKey, Properties = properties };

                entries.Add(entry);

                if (limit < index)
                    break;
            }

            var dictionary = new DictionaryDto { Name = dictionaryName, Entries = entries.ToArray() };

            return dictionary;
        }

        private static async Task<DictionaryDto> ParseDictionaryFromCsvWithDescriptionAsync(string dataToImport, string dictionaryName, int limit, bool skipEmpty, string delimiter, Encoding encoding)
        {
            dataToImport.ThrowIfEmpty();
            dictionaryName.ThrowIfEmpty();

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
                Delimiter = delimiter,
                Encoding = encoding,
                IgnoreBlankLines = true,
                Mode = CsvMode.RFC4180,
                NewLine = "\r\n",
                MissingFieldFound = null,
            };

            using var reader = new StringReader(dataToImport);
            using var csv = new CsvReader(reader, config);

            csv.Read();
            csv.ReadHeader();
            csv.MakeHeadersUnique();

            var entries = new List<DictionaryEntryDto>();

            var index = 0;
            while (await csv.ReadAsync())
            {
                index++;

                var fields = csv.GetFields();

                var properties = new List<DictionaryEntryPropertyDto>();
                var key = fields[0].Value;
                var desc = fields[1].Value;

                for (var i = 2; i < fields.Length; i += 2)
                {
                    if (skipEmpty && string.IsNullOrEmpty(fields[i].Value))
                        continue;

                    properties.Add(new DictionaryEntryPropertyDto { Key = fields[i].Key, Value = fields[i].Value, Descripton = fields[i + 1].Value });
                }

                var entry = new DictionaryEntryDto { Key = key, Properties = properties.ToArray(), Description = desc };

                entries.Add(entry);


                if (limit < index)
                    break;
            }

            var dictionary = new DictionaryDto { Name = dictionaryName, Entries = entries.ToArray() };

            return dictionary;
        }

        const string keyName = "Klíč";

        public static string[] GetColumnNamesFromEntries(IEnumerable<DictionaryEntryDto> entries, bool skipHeader = false)
        {
            var headerNames = new List<string>();

            if (!skipHeader)
                headerNames.Add(keyName);

            var propertyNames = entries
                .SelectMany(ent => ent.Properties)
                .Select(ent => ent.Key)
                .Distinct()
                .OrderBy(ent => ent);

            headerNames.AddRange(propertyNames);

            return headerNames.ToArray();
        }

        public static List<dynamic> GetRecordsFromDictionaryEntries(IEnumerable<DictionaryEntryDto> entries)
        {
            var records = new List<dynamic>();
            var headerNames = GetColumnNamesFromEntries(entries, true);

            foreach (var entry in entries)
            {
                var recordObject = new ExpandoObject();
                recordObject.TryAdd(FormatEntryName(keyName), entry.Key);
                recordObject.TryAdd(FormatEntryDescriptionName(keyName), entry.Description);

                foreach (var name in headerNames)
                {
                    var property = GetPropertyByKey(entry, name);

                    var formatedName = FormatEntryName(name);
                    if (!recordObject.TryAdd(formatedName, property?.Value ?? ""))
                        throw new Exception($"Není možné přidat duplicitní hodnotu {formatedName} do vytvářeného záznmu");

                    var formatedDesc = FormatEntryDescriptionName(name);
                    if (!recordObject.TryAdd(formatedDesc, property?.Descripton ?? ""))
                        throw new Exception($"Není možné přidat duplicitní hodnotu {formatedDesc} do vytvářeného záznmu");
                }

                records.Add(recordObject);
            }

            return records;
        }

        private static string FormatEntryName(string name)
        {
            return name;
        }

        private static string FormatEntryDescriptionName(string name)
        {
            name = FormatEntryName(name);

            return $"{name}_Popis";
        }

        private static DictionaryEntryPropertyDto GetPropertyByKey(DictionaryEntryDto entry, string key)
        {
            return entry.Properties.FirstOrDefault(p => p.Key == key);
        }
    }
}
