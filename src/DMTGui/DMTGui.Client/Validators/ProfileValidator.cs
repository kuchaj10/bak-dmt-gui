﻿using DMTGui.Client.Extensions;
using DMTGui.Client.Models;
using DMTGui.Client.Validators.ValidatorActions;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Utils;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using System.Xml;

namespace DMTGui.Client.Validators
{
    /// <summary>
    /// Validátor obsahu profilu. To znamená správnost slovníků, proměnných a dalších hodnot.
    /// </summary>
    class ProfileValidator
    {
        private List<IElementValidatorAction> _elementValidatorActions;
        private List<IAttributeValidatorAction> _attributeValidatorActions;

        public ProfileValidator()
        {
            _elementValidatorActions = new List<IElementValidatorAction>();
            _attributeValidatorActions = new List<IAttributeValidatorAction>();
        }

        /// <summary>
        /// Přidá validačeí akci pro element
        /// </summary>
        public void AddValidatorAction(IElementValidatorAction action)
        {
            action.ThrowIfNull();
            _elementValidatorActions.Add(action);
        }

        /// <summary>
        /// Přidá validační akci pro atribut
        /// </summary>
        public void AddValidatorAction(IAttributeValidatorAction action)
        {
            action.ThrowIfNull();
            _attributeValidatorActions.Add(action);
        }

        /// <summary>
        /// Zvaliduje xml migrační profil proti nastaveným validačním akcím
        /// </summary>
        public ValidatorWarning[] Validate(string profileAsXml)
        {
            var migrationProfile = XmlUtils.Deserialize<MigrationProfile>(profileAsXml);

            using var stringReader = new StringReader(profileAsXml);
            using var xmlReader = XmlReader.Create(stringReader);

            var validatorWarnings = new List<ValidatorWarning>();

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType != XmlNodeType.Element)
                    continue;

                var elementInfo = xmlReader.GetElement();

                ValidateElement(elementInfo, migrationProfile, ref validatorWarnings);
            }

            return validatorWarnings
                .OrderBy(wrn => wrn.Postition.StartLineNumber)
                .ThenBy(wrn => wrn.Postition.StartLinePosition)
                .ToArray();
        }

        private void ValidateElement(ElementInfo elementInfo, MigrationProfile migrationProfile, ref List<ValidatorWarning> validatorWarnings)
        {
            var actionWarnings = _elementValidatorActions
                .SelectMany(act => act.Validate(elementInfo, migrationProfile))
                .Where(act => act is not null)
                .ToList();

            validatorWarnings.AddRange(actionWarnings);

            foreach (var attributeInfo in elementInfo.Attributes)
            {
                var attActionWarnings = _attributeValidatorActions
                    .SelectMany(act => act.Validate(attributeInfo, migrationProfile))
                    .Where(act => act is not null)
                    .ToList();

                validatorWarnings.AddRange(attActionWarnings);
            }
        }
    }

}
