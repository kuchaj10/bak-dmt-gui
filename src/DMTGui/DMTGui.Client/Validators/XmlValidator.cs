﻿using DMTGui.Client.Models;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace DMTGui.Client.Validators
{
    /// <summary>
    /// Nástroj pro validaci xml dokumentů proti předanému typu <see cref="T"/>
    /// </summary>
    class XmlValidator<T>
    {
        public XmlValidator()
        {
        }

        /// <summary>
        /// Zvaliduje profil
        /// </summary>
        public List<ValidatorWarning> Validate(string profileAsXml, out bool wrongXml)
        {
            return ValidateXml(profileAsXml, out wrongXml);
        }

        /// <summary>
        /// Ověří, že se jedná o validní tvar xml. Nekontroluje obsah, ale pouze formát.
        /// </summary>
        public bool CheckIsValidXml(string xmlString, out List<ValidatorWarning> warnings)
        {
            warnings = new List<ValidatorWarning>();

            try
            {
                using var reader = new StringReader(xmlString);
                using var xmlReader = XmlReader.Create(reader);
                
                // Prolítnu celým xml souborem, bez nutnosti s čímkoli pracovat
                while (xmlReader.Read()) {}

                return true;
            }
            //catch (XmlException ex)
            //{
            //    var position = new PositionInfo(ex.LineNumber, ex.LinePosition);
            //    warnings.Add(new ValidatorWarning
            //    {
            //        Level = WarningLevel.Error,
            //        FullLineWarning = true,
            //        Postition = position,
            //        Message = ex.Message
            //    });
            //}
            catch (Exception ex)
            {
                var position = TryParsePositionFromMessage(ex.Message);
                warnings.Add(new ValidatorWarning
                {
                    Level = WarningLevel.Error,
                    FullLineWarning = true,
                    Postition = position,
                    Message = ex.Message
                });
            }

            return false;
        }

        /// <summary>
        /// "Validuje" xml proti modelu tím, že se ho pokusí serializovat a ohlásí všechny chyby.
        /// </summary>
        private static List<ValidatorWarning> ValidateXml(string xml, out bool wrongXml)
        {
            wrongXml = false;
            var warnings = new List<ValidatorWarning>();

            var serializer = new XmlSerializer(typeof(T));

            serializer.UnknownElement += new XmlElementEventHandler(
                (sender, e) => serializer_UnknownElement(sender, e, warnings));

            //serializer.UnknownNode += new XmlNodeEventHandler(
            //    (object sender, XmlNodeEventArgs e) => serializer_UnknownNode(sender, e, warnings));

            serializer.UnknownAttribute += new XmlAttributeEventHandler(
                (sender, e) => serializer_UnknownAttribute(sender, e, warnings));

            using var reader = new StringReader(xml);
            using var xmlReader = XmlReader.Create(reader);

            try
            {
                serializer.Deserialize(xmlReader);
            }
            catch (Exception ex)
            {
                wrongXml = true;
                warnings.Clear();

                var position = TryParsePositionFromMessage(ex.Message);
                warnings.Add(new ValidatorWarning
                {
                    Level = WarningLevel.Error,
                    FullLineWarning = true,
                    Postition = position,
                    Message = ex.Message
                });
            }

            return warnings;
        }

        private static PositionInfo TryParsePositionFromMessage(string message)
        {
            var firstPattern = "Line ([0-9]+), position ([0-9]+)";
            var firstMatch = Regex.Match(message, firstPattern, RegexOptions.Compiled);

            if (firstMatch.Success)
            {
                int.TryParse(firstMatch.Groups[1].Value, out var lineNumber);
                int.TryParse(firstMatch.Groups[2].Value, out var linePosition);

                return new PositionInfo(lineNumber, linePosition);
            }

            var secondPattern = @"\(([0-9]+), ([0-9]+)\)";
            var secondMatch = Regex.Match(message, secondPattern, RegexOptions.Compiled);

            if (secondMatch.Success)
            {
                int.TryParse(secondMatch.Groups[1].Value, out var lineNumber);
                int.TryParse(secondMatch.Groups[2].Value, out var linePosition);

                return new PositionInfo(lineNumber, linePosition);
            }

            return PositionInfo.Zero;

        }

        private static void serializer_UnknownElement(object sender, XmlElementEventArgs e, List<ValidatorWarning> warnings)
        {
            var position = new PositionInfo(e.LineNumber, e.LinePosition);

            warnings.Add(new ValidatorWarning
            {
                FullLineWarning = true,
                Postition = position,
                Message = $"Neznámý element '{e.Element.Name}'"
            });
        }

        //private static void serializer_UnknownNode(object sender, XmlNodeEventArgs e, List<ValidatorWarning> warnings)
        //{
        //    var startPosition = new PositionInfo { LineNumber = e.LineNumber, LinePosition = e.LinePosition };
        //    var endPosition = new PositionInfo { LineNumber = e.LineNumber, LinePosition = e.LinePosition + 1 };

        //    warnings.Add(new ValidatorWarning
        //    {
        //        FullLineWarning = false,
        //        StartPostition = startPosition,
        //        EndPostition = endPosition,
        //        Message = $"Neznámý node '{e.Name}'"
        //    });
        //}

        private static void serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e, List<ValidatorWarning> warnings)
        {
            var position = new PositionInfo(e.LineNumber, e.LinePosition);

            warnings.Add(new ValidatorWarning
            {
                FullLineWarning = true,
                Postition = position,
                Message = $"Neznámý atribut '{e.Attr.Name}'"
            });
        }

        //// <summary>
        //// Validuje xml proti xml schématu, které se pokusí vytvořit z předaného objektu.
        //// </summary>
        //static void ValidateXmlAgainsSchema<T>(string xml)
        //{
        //    var schemas = ExportSchemas<T>();
        //    //PrintSchemas(schemas);

        //    var settings = new XmlReaderSettings
        //    {
        //        ValidationType = ValidationType.Schema,
        //        ValidationFlags =
        //            XmlSchemaValidationFlags.ProcessIdentityConstraints |
        //            XmlSchemaValidationFlags.ProcessInlineSchema |
        //            XmlSchemaValidationFlags.ProcessSchemaLocation |
        //            XmlSchemaValidationFlags.ReportValidationWarnings
        //    };

        //    schemas.ToList().ForEach(sch => settings.Schemas.Add(sch));


        //    settings.ValidationEventHandler +=
        //        delegate (object sender, ValidationEventArgs args)
        //        {
        //            if (args.Severity == XmlSeverityType.Warning)
        //            {
        //                Console.WriteLine(args.Message);
        //            }
        //            else
        //            {
        //                Console.WriteLine(args.Exception.ToString());
        //            }
        //        };

        //    var serializer = new XmlSerializer(typeof(T));

        //    serializer.UnknownNode += new XmlNodeEventHandler(serializer_UnknownNode);
        //    serializer.UnknownAttribute += new XmlAttributeEventHandler(serializer_UnknownAttribute);
        //    serializer.UnknownElement += new XmlElementEventHandler(serializer_UnknownElement);


        //    //using var stream = new FileStream(pathToProfile, FileMode.Open);
        //    //using var reader = XmlReader.Create(stream, settings);

        //    using var reader = new StringReader(xml);

        //    serializer.Deserialize(reader);
        //}
    }

}
