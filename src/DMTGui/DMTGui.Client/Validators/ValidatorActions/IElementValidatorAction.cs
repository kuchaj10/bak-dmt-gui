﻿using DMTGui.Client.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using System.Xml;

namespace DMTGui.Client.Validators.ValidatorActions
{
    public interface IElementValidatorAction
    {
        /// <summary>
        /// Validace předaného elementu <paramref name="elementInfo"/> 
        /// </summary>
        List<ValidatorWarning> Validate(ElementInfo elementInfo, MigrationProfile profile);
    }

}
