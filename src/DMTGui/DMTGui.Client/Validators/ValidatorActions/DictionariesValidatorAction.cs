﻿using DMTGui.Client.Models;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration.Transformation.Fields;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration.Transformation.Values;

namespace DMTGui.Client.Validators.ValidatorActions
{
    // TODO (Jakub): Napsat test
    /// <summary>
    /// Validace existence slovníku, nebo položky slovníku, pokud je použitá v profilu.
    /// </summary>
    class DictionariesValidatorAction : IElementValidatorAction
    {
        private readonly IEnumerable<DictionaryDto> _dictionaries;

        public DictionariesValidatorAction(IEnumerable<DictionaryDto> dictionaries)
        {
            dictionaries.ThrowIfNull();

            _dictionaries = dictionaries;
        }

        /// <inheritdoc/>
        public List<ValidatorWarning> Validate(ElementInfo elementInfo, MigrationProfile profile)
        {
            elementInfo.ThrowIfNull();
            profile.ThrowIfNull();

            var elementName = elementInfo.Name;

            var warnings = elementName switch
            {
                SetDictionaryValueConfig.TypeName => ValidateSetDictionaryValueConfig(elementInfo),
                DictionaryFieldConfig.TypeName => ValidateDictionaryFieldConfig(elementInfo),
                _ => new List<ValidatorWarning>()
            };

            return warnings;
        }

        // Logika pro SetDictionaryValueConfig(TypeName).
        // SourceField musí být vyplněný.
        // Dictionary musí existovat.
        // Property musí existovat pro slovník.
        // DefaultKey existuje ve slovníku.
        private List<ValidatorWarning> ValidateSetDictionaryValueConfig(ElementInfo elementInfo)
        {
            var sourceFieldAttribute = GetAttribute(elementInfo, "sourceField", out var warning);
            if (sourceFieldAttribute is null)
                return new List<ValidatorWarning> { warning };

            var dictionaryAttribute = GetAttribute(elementInfo, "dictionary", out warning);
            if (dictionaryAttribute is null)
                return new List<ValidatorWarning> { warning };

            var dictionary = GetDictionary(_dictionaries, dictionaryAttribute, out warning);
            if (dictionary is null)
                return new List<ValidatorWarning> { warning };

            var propertyAttribute = GetAttribute(elementInfo, "property", out warning);
            if (propertyAttribute is null)
                return new List<ValidatorWarning> { warning };

            if (!CheckProperty(dictionary, dictionaryAttribute, propertyAttribute, out warning))
                return new List<ValidatorWarning> { warning };

            if (!CheckDefaultKey(dictionary, elementInfo, dictionaryAttribute, out warning))
                return new List<ValidatorWarning> { warning };

            return new List<ValidatorWarning>();
        }

        // Logika pro SetDictionaryValueConfig(TypeName).
        // Field musí být vyplněný.
        // Dictionary musí existovat.
        // DefaultKey existuje ve slovníku.
        private List<ValidatorWarning> ValidateDictionaryFieldConfig(ElementInfo elementInfo)
        {
            var sourceFieldAttribute = GetAttribute(elementInfo, "field", out var warning);
            if (sourceFieldAttribute is null)
                return new List<ValidatorWarning> { warning };

            var dictionaryAttribute = GetAttribute(elementInfo, "dictionary", out warning);
            if (dictionaryAttribute is null)
                return new List<ValidatorWarning> { warning };

            var dictionary = GetDictionary(_dictionaries, dictionaryAttribute, out warning);
            if (dictionary is null)
                return new List<ValidatorWarning> { warning };

            if (!CheckDefaultKey(dictionary, elementInfo, dictionaryAttribute, out warning))
                return new List<ValidatorWarning> { warning };

            return new List<ValidatorWarning>();
        }

        private static AttributeInfo GetAttribute(ElementInfo elementInfo, string attributeName, out ValidatorWarning warning)
        {
            warning = null;

            var attribute = elementInfo.Attributes.SingleOrDefault(att => att.Name == attributeName);
            if (attribute is null)
            {
                warning = new ValidatorWarning
                {
                    FullLineWarning = true,
                    Postition = elementInfo.Position,
                    Message = $"Chybí atribut '{attributeName}'"
                };
            }

            return attribute;
        }

        private static DictionaryDto GetDictionary(IEnumerable<DictionaryDto> dictionaries, AttributeInfo dictionaryAttribute, out ValidatorWarning warning)
        {
            warning = null;

            var dictionary = dictionaries.FirstOrDefault(dic => dic.Name == dictionaryAttribute.Value);
            if (dictionary is null)
            {
                warning = new ValidatorWarning
                {
                    FullLineWarning = false,
                    Postition = dictionaryAttribute.ValuePosition,
                    Message = $"Neexistuje slovník '{dictionaryAttribute.Value}'"
                };
            }

            return dictionary;
        }

        private static bool CheckProperty(DictionaryDto dictionary, AttributeInfo dictionaryAttribute, AttributeInfo propertyAttribute, out ValidatorWarning warning)
        {
            warning = null;

            var dictionaryProperties = dictionary.Entries.SelectMany(ent => ent.Properties);
            if (!dictionaryProperties.Any(prt => prt.Key == propertyAttribute.Value))
            {
                warning = new ValidatorWarning
                {
                    FullLineWarning = false,
                    Postition = propertyAttribute.ValuePosition,
                    Message = $"Neexistuje položka '{propertyAttribute.Value}' ve slovníku {dictionaryAttribute.Value}"
                };

                return false;
            }

            return true;
        }

        private static bool CheckDefaultKey(DictionaryDto dictionary, ElementInfo elementInfo, AttributeInfo dictionaryAttribute, out ValidatorWarning warning)
        {
            warning = null;

            // defaultKey není povinnej atribute
            var defaultKeyAttribute = elementInfo.Attributes.SingleOrDefault(att => att.Name == "defaultKey");
            if (defaultKeyAttribute is not null && !dictionary.Entries.Any(ent => ent.Key == defaultKeyAttribute.Value))
            {
                warning = new ValidatorWarning
                {
                    FullLineWarning = false,
                    Postition = defaultKeyAttribute.ValuePosition,
                    Message = $"Neexistuje defaultní hodnota '{defaultKeyAttribute.Value}' ve slovníku {dictionaryAttribute.Value}"
                };

                return false;
            }

            return true;
        }
    }
}
