﻿using DMTGui.Client.Models;
using DMTGui.Shared.Extensions;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using ICZ.Sps.DMT.DMTLib.Values;
using System.Text.RegularExpressions;

namespace DMTGui.Client.Validators.ValidatorActions
{
    /// <summary>
    /// Validace existence definice proměnné, pokud je použitá v profilu
    /// </summary>
    public class VariableValidatorAction : IAttributeValidatorAction
    {
        private const string VariablePattern = @"\{@([a-zA-Z0-9_]{1,})([-+]{1}[=]?[0-9YMD]{1,})?(;(.{1,}))?\}";

        private static readonly IEnumerable<string> BuiltInVariables = new[] { "DATE", "NOW", "TODAY", "NULL" };

        /// <inheritdoc/>
        public List<ValidatorWarning> Validate(AttributeInfo attributeInfo, MigrationProfile profile)
        {
            attributeInfo.ThrowIfNull();
            profile.ThrowIfNull();

            if (!attributeInfo.Value.Contains('@'))
                return new List<ValidatorWarning>();

            if (!IsValidVariable(attributeInfo.Value, attributeInfo, out var variable, out var warnings))
                return warnings;

            if (IsBuiltInVariable(variable))
                return new List<ValidatorWarning>();

            if (!IsVariable(variable, attributeInfo, profile, out warnings))
                return warnings;

            return new List<ValidatorWarning>();
        }

        private static bool IsValidVariable(string value, AttributeInfo attributeInfo, out string variable, out List<ValidatorWarning> warnings)
        {
            variable = null;
            warnings = null;

            var match = Regex.Match(value, VariablePattern);
            if (match.Success)
            {
                variable = match.Groups[1].ToString();
                return true;
            }

            warnings = new List<ValidatorWarning>()
            {
                new ValidatorWarning
                {
                    FullLineWarning = false,
                    Postition = attributeInfo.ValuePosition,
                    Message = $"Hodnota '{variable}' není validní proměnná"
                }
            };

            return false;
        }

        private static bool IsBuiltInVariable(string variable)
        {
            return BuiltInVariables.Any(vri => vri == variable);
        }

        private static bool IsVariable(string variable, AttributeInfo attributeInfo, MigrationProfile profile, out List<ValidatorWarning> warnings)
        {
            warnings = null;

            if (profile.MigrationConfiguration?.VariablesConfigs?.Any(vrb => vrb.Name == variable) ?? false)
                return true;

            warnings = new List<ValidatorWarning>()
            {
                new ValidatorWarning
                {
                    FullLineWarning = false,
                    Postition = attributeInfo.ValuePosition,
                    Message = $"Proměnná '{variable}' neexistuje"
                }
            };

            return false;
        }
    }
}
