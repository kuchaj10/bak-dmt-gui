﻿using DMTGui.Client.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using System.Xml;

namespace DMTGui.Client.Validators.ValidatorActions
{
    public interface IAttributeValidatorAction
    {
        /// <summary>
        /// Provede validaci předaného atributu
        /// </summary>
        List<ValidatorWarning> Validate(AttributeInfo attributeInfo, MigrationProfile profile);
    }
}
