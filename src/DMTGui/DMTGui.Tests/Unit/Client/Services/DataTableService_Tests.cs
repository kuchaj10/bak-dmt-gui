﻿using DMTGui.Client.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading.Tasks;

namespace DMTGui.Tests.Unit.Client.Services
{
    [TestClass]
    public class DataTableService_Tests
    {
        class Data
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }

        [TestMethod]
        public async Task ExportToString_IsValid()
        {
            // Příprava 
            var service = new DataTableService();
            var dataToExport = new[]
            {
                new Data { Name = "First",  Value = "Value 1" },
                new Data { Name = "Second", Value = "Value 2" },
            };

            // Zpracování 
            var exportedData = await service.ExportToStringAsync(dataToExport);

            // Test
            var expected =
                $"Name\tValue{Environment.NewLine}" +
                $"First\tValue 1{Environment.NewLine}" +
                $"Second\tValue 2{Environment.NewLine}";

            Assert.AreEqual(expected, exportedData);
        }

        [TestMethod]
        public async Task ExportToString_ThrowOnNull()
        {
            // Příprava 
            var service = new DataTableService();
            Data[] dataToExport = null;

            // Test
            try
            {
                await service.ExportToStringAsync(dataToExport);
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            }
        }
    }
}
