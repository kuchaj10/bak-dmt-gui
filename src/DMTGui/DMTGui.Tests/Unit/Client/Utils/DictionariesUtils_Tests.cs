﻿using DMTGui.Client.Utils;
using DMTGui.Shared.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMTGui.Tests.Unit.Client.Utils
{
    [TestClass]
    public class DictionariesUtils_Tests
    {
        [TestMethod]
        public void GetColumnNamesFromEntries_SingleEntry()
        {
            // Příprava 
            var first = new DictionaryEntryDto
            {
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Ruka" },
                    new DictionaryEntryPropertyDto { Key = "Noha" },
                    new DictionaryEntryPropertyDto { Key = "Hlava" },
                }
            };

            // Provedení 
            var columns = DictionariesUtils.GetColumnNamesFromEntries(new[] { first });

            // Test
            Assert.AreEqual("Klíč", columns[0]);
            Assert.AreEqual("Hlava", columns[1]);
            Assert.AreEqual("Noha", columns[2]);
            Assert.AreEqual("Ruka", columns[3]);
        }

        [TestMethod]
        public void GetColumnNamesFromEntries_MultipleEntries()
        {
            // Příprava 
            var first = new DictionaryEntryDto
            {
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Noha" },
                    new DictionaryEntryPropertyDto { Key = "Hlava" },
                }
            };

            var second = new DictionaryEntryDto
            {
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Ruka" },
                }
            };

            var third = new DictionaryEntryDto
            {
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Ruka" },
                    new DictionaryEntryPropertyDto { Key = "Noha" },
                }
            };

            // Provedení 
            var columns = DictionariesUtils.GetColumnNamesFromEntries(new[] { first, second, third });

            // Test
            Assert.AreEqual("Klíč", columns[0]);
            Assert.AreEqual("Hlava", columns[1]);
            Assert.AreEqual("Noha", columns[2]);
            Assert.AreEqual("Ruka", columns[3]);
        }

        [TestMethod]
        public void GetRecordsFromDictionaryEntries_SingleEntry()
        {
            // Příprava 
            var first = new DictionaryEntryDto
            {
                Key = "Položka slovníku 1",
                Description = "Popis slovníku 1",
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Položka 1", Descripton = "Popis 1", Value = "Hodnota 1" },
                    new DictionaryEntryPropertyDto { Key = "Položka 2", Descripton = "Popis 2", Value = "Hodnota 2"  },
                }
            };

            // Provedení 
            var records = DictionariesUtils.GetRecordsFromDictionaryEntries(new[] { first });
            IDictionary<string, object> expando = records[0];

            // Test
            Assert.IsNotNull(expando);
            Assert.AreEqual(expando["Klíč"], "Položka slovníku 1");
            Assert.AreEqual(expando["Klíč_Popis"], "Popis slovníku 1");
            Assert.AreEqual(expando["Položka 1"], "Hodnota 1");
            Assert.AreEqual(expando["Položka 1_Popis"], "Popis 1");
            Assert.AreEqual(expando["Položka 2"], "Hodnota 2");
            Assert.AreEqual(expando["Položka 2_Popis"], "Popis 2");
        }

        [TestMethod]
        public void GetRecordsFromDictionaryEntries_MutipleEntries()
        {
            // Příprava 
            var first = new DictionaryEntryDto
            {
                Key = "Položka slovníku 1",
                Description = "Popis slovníku 1",
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Položka 2", Descripton = "Popis 2", Value = "Hodnota 2"  },
                }
            };
            var second = new DictionaryEntryDto
            {
                Key = "Položka slovníku 2",
                Description = "Popis slovníku 2",
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Položka 1", Descripton = "Popis 1", Value = "Hodnota 1" },
                    new DictionaryEntryPropertyDto { Key = "Položka 2", Descripton = "Popis 2", Value = "Hodnota 2" },
                }
            };

            // Provedení 
            var records = DictionariesUtils.GetRecordsFromDictionaryEntries(new[] { first, second });


            // Test
            var expando = (IDictionary<string, object>)records[0];
            Assert.IsNotNull(expando);
            Assert.AreEqual(expando["Klíč"], "Položka slovníku 1");
            Assert.AreEqual(expando["Klíč_Popis"], "Popis slovníku 1");
            Assert.AreEqual(expando["Položka 2"], "Hodnota 2");
            Assert.AreEqual(expando["Položka 2_Popis"], "Popis 2");

            expando = (IDictionary<string, object>)records[1];
            Assert.IsNotNull(expando);
            Assert.AreEqual(expando["Klíč"], "Položka slovníku 2");
            Assert.AreEqual(expando["Klíč_Popis"], "Popis slovníku 2");
            Assert.AreEqual(expando["Položka 1"], "Hodnota 1");
            Assert.AreEqual(expando["Položka 1_Popis"], "Popis 1");
            Assert.AreEqual(expando["Položka 2"], "Hodnota 2");
            Assert.AreEqual(expando["Položka 2_Popis"], "Popis 2");
        }

        [TestMethod]
        public void GetRecordsFromDictionaryEntries_ThrowOnDuplicites()
        {
            // Příprava 
            var first = new DictionaryEntryDto
            {
                Key = "Položka slovníku 1",
                Properties = new DictionaryEntryPropertyDto[]
                {
                    new DictionaryEntryPropertyDto { Key = "Položka 1" },
                    new DictionaryEntryPropertyDto { Key = "Položka 1" },
                }
            };

            try
            {
                DictionariesUtils.GetRecordsFromDictionaryEntries(new[] { first });
                Assert.Fail();
            }
            catch
            {
            }
        }

        //Přidat chybu pokud budou dvě položky mít stejný jméno
    }
}
