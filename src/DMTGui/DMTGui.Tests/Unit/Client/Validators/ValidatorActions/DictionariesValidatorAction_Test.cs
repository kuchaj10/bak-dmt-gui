﻿using DMTGui.Client.Models;
using DMTGui.Client.Validators.ValidatorActions;
using DMTGui.Shared.Models;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DMTGui.Tests.Unit.Client.Validators.ValidatorActions
{
    [TestClass]
    public class DictionariesValidatorAction_Test
    {
        private static DictionaryDto[] Dictionaries;

        static DictionariesValidatorAction_Test()
        {
            // Definice validního testovacího slovníku 
            Dictionaries = new DictionaryDto[]
            {
                new DictionaryDto
                {
                    Name = "Slovnik_1",
                    Entries = new DictionaryEntryDto[]
                    {
                        new DictionaryEntryDto
                        {
                            Key = "Hodnota_1",
                            Properties = new DictionaryEntryPropertyDto[]
                            {
                                new DictionaryEntryPropertyDto
                                {
                                    Key = "Položka_1",
                                    Value = ""
                                },
                                new DictionaryEntryPropertyDto
                                {
                                    Key = "Položka_2",
                                    Value = ""
                                }
                            }
                        },
                        new DictionaryEntryDto
                        {
                            Key = "Hodnota_2",
                            Properties = new DictionaryEntryPropertyDto[]
                            {
                                new DictionaryEntryPropertyDto
                                {
                                    Key = "Položka_1",
                                    Value = ""
                                }
                            }
                        }
                    }
                }
            };
        }

        [TestMethod]
        public void SetDictionaryValueConfig_Valid()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "sourceField", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "property", Value = "Položka_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void SetDictionaryValueConfig_Missing_SourceField()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "property", Value = "Položka_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Chybí atribut")));
        }

        [TestMethod]
        public void SetDictionaryValueConfig_Missing_Dictionary()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "sourceField", Value = "" },
                    new AttributeInfo { Name = "property", Value = "Položka_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Chybí atribut")));
        }

        [TestMethod]
        public void SetDictionaryValueConfig_NotValid_Dictionary()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "sourceField", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_2" },
                    new AttributeInfo { Name = "property", Value = "Položka_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Neexistuje slovník")));
        }

        [TestMethod]
        public void SetDictionaryValueConfig_Missing_Property()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "sourceField", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Chybí atribut")));
        }

        [TestMethod]
        public void SetDictionaryValueConfig_NotValid_Property()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "sourceField", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "property", Value = "Položka_3" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Neexistuje položka")));
        }

        [TestMethod]
        public void SetDictionaryValueConfig_NotValid_DefaultKey()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "SetDictionaryValue",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "sourceField", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "property", Value = "Položka_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_3" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Neexistuje defaultní hodnota")));
        }


        [TestMethod]
        public void DictionaryFieldConfig_Valid()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "DictionaryField",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "field", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void DictionaryFieldConfig_Missing_Field()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "DictionaryField",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Chybí atribut")));
        }

        [TestMethod]
        public void DictionaryFieldConfig_Missing_Dictionary()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "DictionaryField",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "field", Value = "" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Chybí atribut")));
        }

        [TestMethod]
        public void DictionaryFieldConfig_NotValid_Dictionary()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "DictionaryField",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "field", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_2" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_1" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Neexistuje slovník")));
        }

        [TestMethod]
        public void DictionaryFieldConfig_NotValid_DefaultKey()
        {
            // Příprava
            var action = new DictionariesValidatorAction(Dictionaries);

            var elementInfo = new ElementInfo
            {
                Name = "DictionaryField",
                Attributes = new AttributeInfo[]
                {
                    new AttributeInfo { Name = "field", Value = "" },
                    new AttributeInfo { Name = "dictionary", Value = "Slovnik_1" },
                    new AttributeInfo { Name = "defaultKey", Value = "Hodnota_3" },
                }
            };

            // Provedení 
            var result = action.Validate(elementInfo, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any(war => war.Message.Contains("Neexistuje defaultní hodnota")));
        }
    }
}
