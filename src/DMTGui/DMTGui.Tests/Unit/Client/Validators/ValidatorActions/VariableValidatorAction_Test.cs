﻿using DMTGui.Client.Models;
using DMTGui.Client.Validators.ValidatorActions;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration;
using ICZ.Sps.DMT.DMTLib.Configuration.Migration.Variables;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace DMTGui.Tests.Unit.Client.Validators.ValidatorActions
{
    [TestClass]
    public class VariableValidatorAction_Test
    {
        [TestMethod]
        public void Validate_IsValid_NotVariable()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "" };

            // Provedení 
            var result = action.Validate(attribute, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void Validate_IsValid_Builtin()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "{@NOW}" };

            // Provedení 
            var result = action.Validate(attribute, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void Validate_IsValid_BuiltinFormatted()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "{@NOW;yyyy-MM-dd}" };

            // Provedení 
            var result = action.Validate(attribute, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void Validate_IsValid_BuiltinUpdated()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "{@NOW+1Y2M5D}" };

            // Provedení 
            var result = action.Validate(attribute, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void Validate_IsValid()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "{@test_variable}" };
            var profile = new MigrationProfile
            {
                MigrationConfiguration = new MigrationConfiguration
                {
                    VariablesConfigs = new List<VariableConfig>()
                    {
                        new VariableConfig { Name = "test_variable"}
                    }
                }
            };

            // Provedení 
            var result = action.Validate(attribute, profile);

            // Ověření 
            Assert.IsTrue(!result.Any());
        }

        [TestMethod]
        public void Validate_NotValid_NotExisting()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "{@test_proměnná}" };

            // Provedení 
            var result = action.Validate(attribute, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any());
        }

        [TestMethod]
        public void Validate_NotValid_WrongFormat()
        {
            // Příprava
            var action = new VariableValidatorAction();
            var attribute = new AttributeInfo { Name = "sourceField", Value = "{ @OW+1Y2M5D}" };

            // Provedení 
            var result = action.Validate(attribute, new MigrationProfile());

            // Ověření 
            Assert.IsTrue(result.Any());
        }
    }
}
