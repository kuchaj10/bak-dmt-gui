﻿using DMTGui.Shared.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace DMTGui.Tests.Unit.Shared.Extensions
{
    [TestClass]
    public class EnumerableExtensions_Tests
    {
        [TestMethod]
        public void SkipItem_WithSkippableItem()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var skipped = values.SkipItem("second");

            // Test
            Assert.AreEqual(2, skipped.Count());
            Assert.AreEqual("first", skipped.ElementAt(0));
            Assert.AreEqual("third", skipped.ElementAt(1));
        }

        [TestMethod]
        public void SkipItem_WithoutSkippableItem()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var skipped = values.SkipItem("---");

            // Test
            Assert.AreEqual(3, skipped.Count());
            Assert.AreEqual("first", skipped.ElementAt(0));
            Assert.AreEqual("second", skipped.ElementAt(1));
            Assert.AreEqual("third", skipped.ElementAt(2));
        }

        [TestMethod]
        public void SkipItem_WithoutItem()
        {
            // Příprava
            var values = new string[0];

            // Provedení 
            var skipped = values.SkipItem("second");

            // Test
            Assert.AreEqual(0, skipped.Count());
        }

        [TestMethod]
        public void SkipItem_ThrowOnNull()
        {
            // Příprava
            var values = new string[0];

            // Provedení 
            try
            {
                var skipped = values.SkipItem(null);
                Assert.Fail();
            }
            catch
            {
            }
        }

        [TestMethod]
        public void AppendRange_AddMultiple()
        {
            // Příprava
            var enumerable = new string[0];
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var added = enumerable.AppendRange(values);

            // Test
            Assert.AreEqual(3, added.Count());
        }

        [TestMethod]
        public void AppendRange_AddSingle()
        {
            // Příprava
            var enumerable = new string[0];
            var values = new[] { "first" };

            // Provedení 
            var added = enumerable.AppendRange(values);

            // Test
            Assert.AreEqual(1, added.Count());
        }

        [TestMethod]
        public void IndexOf_Existing()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var index = values.IndexOf("second");

            // Test
            Assert.AreEqual(1, index);
        }

        [TestMethod]
        public void IndexOf_NonExisting()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var index = values.IndexOf("---");

            // Test
            Assert.AreEqual(-1, index);
        }

        [TestMethod]
        public void MoveItemUp_Inside()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var moved = values.MoveItemUp("second");

            // Test
            Assert.AreEqual("second", moved.ElementAt(0));
            Assert.AreEqual("first", moved.ElementAt(1));
            Assert.AreEqual("third", moved.ElementAt(2));
        }

        [TestMethod]
        public void MoveItemUp_Edge()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var moved = values.MoveItemUp("first");

            // Test
            Assert.AreEqual("first", moved.ElementAt(0));
            Assert.AreEqual("second", moved.ElementAt(1));
            Assert.AreEqual("third", moved.ElementAt(2));
        }

        [TestMethod]
        public void MoveItemDown_Inside()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var moved = values.MoveItemDown("second");

            // Test
            Assert.AreEqual("first", moved.ElementAt(0));
            Assert.AreEqual("third", moved.ElementAt(1));
            Assert.AreEqual("second", moved.ElementAt(2));
        }

        [TestMethod]
        public void MoveItemDown_Edge()
        {
            // Příprava
            var values = new[] { "first", "second", "third" };

            // Provedení 
            var moved = values.MoveItemDown("third");

            // Test
            Assert.AreEqual("first", moved.ElementAt(0));
            Assert.AreEqual("second", moved.ElementAt(1));
            Assert.AreEqual("third", moved.ElementAt(2));
        }
    }
}
