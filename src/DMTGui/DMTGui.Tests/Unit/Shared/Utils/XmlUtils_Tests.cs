﻿using DMTGui.Shared.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DMTGui.Tests.Unit.Shared.Utils
{
    [TestClass]
    public class XmlUtils_Tests
    {
        public class Data
        {
            public int Id { get; set; }
            public string Value { get; set; }
        }

        [TestMethod]
        public void Deserialize()
        {
            // Příprava 
            var data = "<Data><Id>13</Id><Value>Hodnota 1</Value></Data>";

            // Provedení
            var deserialized = XmlUtils.Deserialize<Data>(data);

            // Test
            Assert.AreEqual(13, deserialized.Id);
            Assert.AreEqual("Hodnota 1", deserialized.Value);
        }
    }
}
