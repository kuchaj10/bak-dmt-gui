﻿using DMTGui.Shared.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMTGui.Tests.Unit.Shared.Utils
{
    [TestClass]
    public class HttpUtils_Tests
    {
        class Data
        {
            public int? Id { get; set; }

            public string Name { get; set; }

            public string[] Values { get; set; }
        }

        [TestMethod]
        public void CreateUriFromModel_BasicClass()
        {
            // Příprava 
            var data = new Data
            {
                Id = 1,
                Name = "Jmeno"
            };

            // Provedení 
            var uri = HttpUtils.CreateUriFromModel(data);

            // Test
            Assert.AreEqual("id=1&name=Jmeno", uri);
        }

        [TestMethod]
        public void CreateUriFromModel_Array()
        {
            // Příprava 
            var data = new Data
            {
                Values = new[]
                {
                    "prvni",
                    "druhy",
                    "treti",
                }
            };

            // Provedení 
            var uri = HttpUtils.CreateUriFromModel(data);

            // Test
            Assert.AreEqual("values=prvni&values=druhy&values=treti", uri);
        }

        [TestMethod]
        public void FormatParametersToUri_FromInt()
        {
            // Příprava 
            var data = new KeyValuePair<string, object>("klic", 1);

            // Provedení 
            var result = HttpUtils.FormatParametersToUri(new[] { data });

            // Test
            Assert.AreEqual("klic=1", result);
        }

        [TestMethod]
        public void FormatParametersToUri_FromString()
        {
            // Příprava 
            var data = new KeyValuePair<string, object>("klic", "hodnota");

            // Provedení 
            var result = HttpUtils.FormatParametersToUri(new[] { data });

            // Test
            Assert.AreEqual("klic=hodnota", result);
        }

        [TestMethod]
        public void FormatParametersToUri_FromEnum()
        {
            // Příprava 
            var data = new KeyValuePair<string, object>("klic", Color.Red);

            // Provedení 
            var result = HttpUtils.FormatParametersToUri(new[] { data });

            // Test
            Assert.AreEqual("klic=Color+%5bRed%5d", result);
        }

        [TestMethod]
        public void FormatParametersToUri_FromArray()
        {
            // Příprava 
            var data = (new[] { "hodnota", "hodnota1" })
                .Select(ent => new KeyValuePair<string, object>("klic", ent))
                .ToArray();

            // Provedení 
            var result = HttpUtils.FormatParametersToUri(data);

            // Test
            Assert.AreEqual("klic=hodnota&klic=hodnota1", result);
        }
    }
}
