﻿using DMTGui.Shared.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMTGui.Tests.Unit.Shared.Utils
{
    [TestClass]
    public class ReflectionUtils_Tests
    {
        class Data
        {
            public int? Id { get; set; }

            public string Name { get; set; }

            public string[] Values { get; set; }
        }

        [TestMethod]
        public void GetValuesFromModel_BasicClass()
        {
            // Příprava 
            var data = new Data
            {
                Id = 1,
                Name = "Jmeno"
            };

            // Provedení 
            var values = ReflectionUtils.GetValuesFromModel(data);

            // Test
            Assert.AreEqual("Id", values[0].Name);
            Assert.AreEqual(1, values[0].Value);
            Assert.AreEqual("Name", values[1].Name);
            Assert.AreEqual("Jmeno", values[1].Value);
        }
    }
}