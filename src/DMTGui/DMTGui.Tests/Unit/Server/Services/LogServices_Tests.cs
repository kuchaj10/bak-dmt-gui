﻿using DMTGui.Server.Services;
using DMTGui.Shared.Models;
using DMTGui.Tests.Helpers;
using ICZ.Sps.DMT.DMTDAL.Main;
using ICZ.Sps.DMT.DMTDAL.Main.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DMTGui.Tests.Unit.Server.Services
{
    [TestClass]
    public class LogServices_Tests
    {
        private const string DatabaseName = "LogServicesDatabase";

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            var options = new DbContextOptionsBuilder<DMTContext>()
                .UseInMemoryDatabase(databaseName: DatabaseName)
                .Options;

            using (var dbContext = new DMTContext(options))
            {
                dbContext.Logs.Add(new LogEntity { Date = DateTime.Parse("2022-06-01"), Level = "WARN", Thread = "1", Message = "Zpráva 1" });
                dbContext.Logs.Add(new LogEntity { Date = DateTime.Parse("2022-06-02"), Level = "INFO", Thread = "1", Message = "Zpráva 2" });
                dbContext.Logs.Add(new LogEntity { Date = DateTime.Parse("2022-06-03"), Level = "ERROR", Thread = "1", Message = "Zpráva 3" });
                dbContext.Logs.Add(new LogEntity { Date = DateTime.Parse("2022-06-04"), Level = "WARN", Thread = "1", Message = "Zpráva 4" });
                dbContext.Logs.Add(new LogEntity { Date = DateTime.Parse("2022-06-05"), Level = "INFO", Thread = "1", Message = "Zpráva 5" });
                dbContext.SaveChanges();
            }
        }

        [TestMethod]
        public async Task GetLogs_All()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            // Provedení
            var all = await service.GetLogsAsync();

            // Test
            Assert.AreEqual(5, all.Count());
        }

        [TestMethod]
        public async Task GetLogs_Pagging()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            var search = new LogSearchDto { Take = 2 };

            // Provedení
            var all = await service.GetLogsAsync(search);

            // Test
            Assert.AreEqual(2, all.Count());
        }

        [TestMethod]
        public async Task GetLogs_JustErrors()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            var search = new LogSearchDto { LogLevels = new[] { "ERROR" } };

            // Provedení
            var all = await service.GetLogsAsync(search);

            // Test
            Assert.AreEqual(1, all.Count());
        }

        [TestMethod]
        public async Task GetLogs_GivenDate()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            var search = new LogSearchDto
            {
                FromDate = DateTime.Parse("2022-06-01"),
                ToDate = DateTime.Parse("2022-06-03")
            };

            // Provedení
            var all = await service.GetLogsAsync(search);

            // Test
            Assert.AreEqual(3, all.Count());
        }

        [TestMethod]
        public async Task GetLogs_OrderByDate()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            var search = new LogSearchDto
            {
                SortColumn = LogSearchSortColumn.Date,
                IsSortAscending = true
            };

            // Provedení
            var all = await service.GetLogsAsync(search);

            // Test
            Assert.AreEqual(5, all.Count());
            Assert.IsTrue(all.ElementAt(0).Date < all.ElementAt(4).Date);
        }

        [TestMethod]
        public async Task GetLogs_OrderByDateDesc()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            var search = new LogSearchDto
            {
                SortColumn = LogSearchSortColumn.Date,
                IsSortAscending = false
            };

            // Provedení
            var all = await service.GetLogsAsync(search);

            // Test
            Assert.AreEqual(5, all.Count());
            Assert.IsTrue(all.ElementAt(0).Date > all.ElementAt(4).Date);
        }

        [TestMethod]
        public async Task CountLogs_All()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            // Provedení
            var count = await service.GetLogsCountAsync();

            // Test
            Assert.AreEqual(5, count);
        }

        [TestMethod]
        public async Task GetLogLevels_Any()
        {
            // Příprava 
            var logger = new EmptyLogger<LogService>();
            var factory = new MockDMTContextFactory(DatabaseName);
            var service = new LogService(logger, factory);

            // Provedení
            var levels = await service.GetLogLevelsAsync();

            // Test
            Assert.AreEqual("WARN", levels.ElementAt(0));
            Assert.AreEqual("INFO", levels.ElementAt(1));
            Assert.AreEqual("ERROR", levels.ElementAt(2));
        }
    }
}
