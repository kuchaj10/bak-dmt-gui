﻿using ICZ.Sps.DMT.DMTDAL.Main;
using Microsoft.EntityFrameworkCore;

namespace DMTGui.Tests.Helpers
{
    class MockDMTContextFactory : IDbContextFactory<DMTContext>
    {
        private string _databaseName;

        public MockDMTContextFactory(string databaseName)
        {
            _databaseName = databaseName;
        }

        public DMTContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<DMTContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            return new DMTContext(options);
        }
    }
}
