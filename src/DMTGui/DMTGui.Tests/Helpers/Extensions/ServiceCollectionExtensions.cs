﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMTGui.Tests.Helpers.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void RemoveDbContextFactory<TContext>(this IServiceCollection services) 
            where TContext : DbContext
        {
            var descriptor = services.Single(srv => srv.ServiceType == typeof(IDbContextFactory<TContext>));
            services.Remove(descriptor);

            descriptor = services.Single(srv => srv.ServiceType == typeof(DbContextOptions<TContext>));
            services.Remove(descriptor);

#pragma warning disable EF1001 // Internal EF Core API usage.
            descriptor = services.Single(srv => srv.ServiceType == typeof(IDbContextFactorySource<TContext>));
#pragma warning restore EF1001 // Internal EF Core API usage.
            services.Remove(descriptor);
        }
    }
}
