﻿using DMTGui.Tests.Helpers;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using DMTGui.Tests.Helpers.Extensions;
using System.Linq;
using System.Threading.Tasks;
using System;
using DMTGui.Shared.Extensions;
using DMTGui.Shared.Models;
using DMTGui.Client.Exceptions;
using server = DMTGui.Server;
using client = DMTGui.Client.Services.Api;
using DMTGui.Dal;
using DMTGui.Dal.Entities;

namespace DMTGui.Tests.Integrations
{
    [TestClass]
    public class ProfileContract_Tests
    {
        private const string DatabaseName = "IntegrationsProfilesDatabase";

        private static WebApplicationFactory<server.Program> _factory;

        private static Guid _profileGuid;
        private static Guid _profileToUpdateGuid;
        private static Guid _profileToDeleteGuid;

        [ClassInitialize]
        public static void ClassInit(TestContext testContext)
        {
            _factory = new WebApplicationFactory<server.Program>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        services.RemoveDbContextFactory<AppDbContext>();
                        services.AddDbContextFactory<AppDbContext>(opt => opt.UseInMemoryDatabase(DatabaseName));
                    });
                });

            _profileGuid = Guid.NewGuid();
            _profileToUpdateGuid = Guid.NewGuid();
            _profileToDeleteGuid = Guid.NewGuid();

            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(DatabaseName).Options;
            using (var dbContext = new AppDbContext(options))
            {
                dbContext.Profiles.Add(new ProfileEntity { Id = _profileGuid, Name = "Profil 1" });
                dbContext.Profiles.Add(new ProfileEntity { Id = _profileToUpdateGuid, Name = "Profil to update" });
                dbContext.Profiles.Add(new ProfileEntity { Id = _profileToDeleteGuid, Name = "Profil to delete" });
                dbContext.SaveChanges();
            }
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _factory.Dispose();
        }

        [TestMethod]
        public async Task ShouldReturnDefaultProfile()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);

            // Provedení
            var profile = await service.GetDefault();


            // Test
            Assert.IsNotNull(profile);
            Assert.IsTrue(profile.Data.Length > 0);
        }

        [TestMethod]
        public async Task ShouldReturnAllProfiles()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);

            // Provedení
            var all = await service.GetAll();

            // Test
            Assert.IsTrue(all.Count() > 1);
        }

        [TestMethod]
        public async Task ShouldReturnProfile()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);
            var guidAsString = _profileGuid.ToBase64String();

            // Provedení
            var profile = await service.Get(guidAsString);

            // Test
            Assert.AreEqual("Profil 1", profile.Name);

        }

        [TestMethod]
        public async Task ShouldAddNewProfile()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);
            var profile = new ProfileDto { Name = "Profil 2" };

            // Provedení
            var addedProfile = await service.Add(profile);

            // Test
            Assert.AreEqual(profile.Name, addedProfile.Name);
        }

        [TestMethod]
        public async Task ShouldReturnNotFound()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);
            var guidAsString = Guid.NewGuid().ToBase64String();
            var profile = new ProfileDto { Name = "Profile to update" };

            // Provedení + Test
            try
            {
                await service.Update(guidAsString, profile);
                Assert.Fail();
            }
            catch (ServerException ex)
            {
                Assert.AreEqual(404, ex.ErrorDetail.StatusCode);
            }
        }

        [TestMethod]
        public async Task ShouldUpdateProfile()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);
            var profile = new ProfileDto { Name = "Profile updated!" };
            var guidAsString = _profileToUpdateGuid.ToBase64String();

            // Provedení
            await service.Update(guidAsString, profile);

            // Test
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(DatabaseName).Options;
            using (var dbContext = new AppDbContext(options))
            {
                var exists = dbContext.Profiles.Any(prf => prf.Id == _profileToUpdateGuid && prf.Name == profile.Name);

                Assert.IsTrue(exists);
            }
        }

        [TestMethod]
        public async Task ShouldDeleteProfile()
        {
            // Příprava
            var client = _factory.CreateClient();
            var service = new client.ProfileService(new EmptyLogger<client.ProfileService>(), client);
            var guidAsString = _profileToDeleteGuid.ToBase64String();

            // Provedení
            await service.Delete(guidAsString);

            // Test
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(DatabaseName).Options;
            using (var dbContext = new AppDbContext(options))
            {
                Assert.IsTrue(!dbContext.Profiles.Any(prf => prf.Id == _profileToDeleteGuid));
            }
        }
    }
}
