﻿using System.Reflection;

[assembly: AssemblyTitle("DMTGui.Shared")]
[assembly: AssemblyDescription("Sdílená knihovna grafického rozhraní pro DMT")]
[assembly: AssemblyCompany("ICZ a.s.")]
[assembly: AssemblyProduct("")]
[assembly: AssemblyCopyright("ICZ a.s. 2022")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0.0+0")] 