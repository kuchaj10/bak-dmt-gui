﻿using System;

namespace DMTGui.Shared.Exceptions
{
    /// <summary>
    /// Chyba v případě nevalidního stavu při práci aplikace
    /// </summary>
    public class WrongOperationException : ServerException
    {
        public WrongOperationException()
        {
        }

        public WrongOperationException(string message) : base(message)
        {
        }

        public WrongOperationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
