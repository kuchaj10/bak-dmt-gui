﻿using System;

namespace DMTGui.Shared.Exceptions
{
    /// <summary>
    /// Chyba z běhu migrace
    /// </summary>
    public class MigrationException : ServerException
    {
        public MigrationException() : base()
        {
        }

        public MigrationException(string message) : base(message)
        {
        }

        public MigrationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
