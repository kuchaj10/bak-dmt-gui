﻿using System;

namespace DMTGui.Shared.Exceptions
{
    /// <summary>
    /// Chyba reprezentující, že zdroj nebyl nalezen
    /// </summary>
    public class NotFoundException : ServerException
    {
        public NotFoundException()
        {
        }

        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
