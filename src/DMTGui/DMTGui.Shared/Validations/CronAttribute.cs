﻿using DMTGui.Shared.Utils;
using System.ComponentModel.DataAnnotations;

namespace DMTGui.Shared.Validations
{
    public class CronAttribute : ValidationAttribute
    {
        public bool IsRequired { get; set; } = false;

        public override bool IsValid(object value)
        {
            if (value is not string cron)
                return false;

            if (string.IsNullOrEmpty(cron))
                return !IsRequired;

            return CronUtils.CheckIsCronValid(cron, out _);
        }
    }
}
