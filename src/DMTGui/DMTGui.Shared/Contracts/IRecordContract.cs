﻿using DMTGui.Shared.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DMTGui.Client.Services.Api
{
    public interface IRecordContract
    {
        /// <summary>
        /// Vrátí všechny migrované záznamy.
        /// </summary>
        Task<IEnumerable<RecordDto>> GetRecords();

        /// <summary>
        /// Vrátí migrované záznamy podle identifikátoru úlohy.
        /// </summary>
        Task<IEnumerable<RecordDto>> GetRecordsByJobId(Guid jobId);

        /// <summary>
        /// Vrátí migrované záznamy podle názvu profilu.
        /// </summary>
        Task<IEnumerable<RecordDto>> GetRecordsByProfileName(string profileName);
    }
}
