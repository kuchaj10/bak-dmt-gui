﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Interface pro implementaci metod na CRUD manipulaci většího množství dat
    /// </summary>
    /// <typeparam name="TIdentifier">Typ identifikátoru typicky string, int, nebo guid.</typeparam>
    /// <typeparam name="TType">Typ objektu, se kterým je pomocí CRUD metod pracováno</typeparam>
    public interface ICrudBulkContract<TIdentifier, TType>
    {
        /// <summary>
        /// Hromadně vytvoří předané objekty
        /// </summary>
        Task AddBulk(IEnumerable<TType> item);

        /// <summary>
        /// Hromadně aktaulizuje předané objekty
        /// </summary>
        Task UpdateBulk(IEnumerable<TType> item);

        /// <summary>
        /// Hromadně vymaže předané objekty
        /// </summary>
        Task DeleteBulk(IEnumerable<TIdentifier> identifier);
    }
}
