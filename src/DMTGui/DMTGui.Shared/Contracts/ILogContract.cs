﻿using DMTGui.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Záznamový kontrakt implementující základní CRUD metody
    /// </summary>
    public interface ILogContract
    {
        /// <summary>
        /// Vrátí všechny záznamy z migrace podle předaného <paramref name="logSearch"/>. Pokud je prázdný, tak vrátí všechny.
        /// </summary>
        Task<IEnumerable<LogDto>> GetLogs(LogSearchDto logSearch = null);

        /// <summary>
        /// Vráti počet záznamů z migrace podle předaného <paramref name="logSearch"/>. Pokud je prázdný, tak vrátí všechny.
        /// </summary>
        Task<int> GetLogsCount(LogSearchDto logSearch = null);

        /// <summary>
        /// Vrátí všechny migrační stavy na záznamech migrace. Takže pokud stav nebyl použit, tak ho nevrátí.
        /// </summary>
        Task<IEnumerable<string>> GetMigrationStates();

        /// <summary>
        /// Vrátí všechny úrovně použité na záznamech migrace. Pokud úroveň nebyla použitá, tak nic nevrátí.
        /// </summary>
        Task<IEnumerable<string>> GetLogLevels();
    }
}
