﻿using DMTGui.Shared.Models;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Profilový kontrakt implementující základní CRUD metody
    /// </summary>
    public interface IProfileContract : ICrudContract<string, ProfileDto, SearchDto>
    {
        /// <summary>
        /// Vrátí defaultní migrační profil. Používá se při vytvoření nového profilu pro usnadnění jeho inicializace.
        /// </summary>
        Task<ProfileDto> GetDefault();
    }
}
