﻿using DMTGui.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Identifikátorový kontrakt implementující základní CRUD metody
    /// </summary>
    public interface IIdentifierContract : ICrudBulkContract<int, IdentifierDto>, ICrudContract<int, IdentifierDto, SearchDto>
    {
        /// <summary>
        /// Vrátí seznam všech použitých stavů
        /// </summary>
        Task<IEnumerable<string>> GetUsedStates();
    }
}
