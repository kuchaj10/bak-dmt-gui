﻿using DMTGui.Shared.Models;

namespace DMTGui.Shared.Contracts
{
    public interface IStatisticsContract : ICrudContract<string, MigrationStatisticsDto, StatisticsSearchDto>
    {
    }
}
