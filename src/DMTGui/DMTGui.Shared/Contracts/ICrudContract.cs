﻿using DMTGui.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Interface pro implementaci metod na CRUD manipulaci
    /// </summary>
    /// <typeparam name="TIdentifier">Typ identifikátoru typicky string, int, nebo guid.</typeparam>
    /// <typeparam name="TType">Typ objektu, se kterým je pomocí CRUD metod pracováno</typeparam>
    public interface ICrudContract<TIdentifier, TType, TSearch>
        where TSearch : class
    {
        /// <summary>
        /// Získání všech objektu podle předaného <paramref name="search"/>
        /// </summary>
        Task<IEnumerable<TType>> GetAll(TSearch search = null);

        /// <summary>
        /// Získání vybraného objektu podle <paramref name="identifier"/>
        /// </summary>
        Task<TType> Get(TIdentifier identifier);

        /// <summary>
        /// Vytvoření předaného <paramref name="item"/>. Po vytvoření vrátí stejný objekt aktualizovaný 
        /// o potřebné informace jako je například identifikátor.
        /// </summary>
        Task<TType> Add(TType item);

        /// <summary>
        /// Aktualizuje předaný <paramref name="item"/> podle předaného <paramref name="identifier"/>.
        /// </summary>
        Task Update(TIdentifier identifier, TType item);

        /// <summary>
        /// Vymaže objekt podle předaného <paramref name="identifier"/>
        /// </summary>
        Task Delete(TIdentifier identifier);
    }
}
