﻿using DMTGui.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Slovníkový kontrakt implementující základní CRUD metody
    /// </summary>
    public interface IDictionaryContract : ICrudContract<string, DictionaryDto, SearchDto>
    {
        /// <summary>
        /// Záská všechny jména slovníků bez ostatních dat
        /// </summary>
        Task<IEnumerable<DictionaryDto>> GetAllNames(SearchDto search = null);
    }
}
