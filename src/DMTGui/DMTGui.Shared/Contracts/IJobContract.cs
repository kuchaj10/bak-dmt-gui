﻿using DMTGui.Shared.Models;
using System.Threading.Tasks;

namespace DMTGui.Shared.Contracts
{
    /// <summary>
    /// Úlohový kontrakt implementující základní CRUD metody
    /// </summary>
    public interface IJobContract : ICrudContract<string, JobDto, SearchDto>
    {
        /// <summary>
        /// Zahájí úlohu podle předaného <paramref name="jobId"/>. Pokud úloha už běží, tak se nic nestane.
        /// </summary>
        Task StartJob(string jobId);

        /// <summary>
        /// Zastaví úlohu podle předaného <paramref name="jobId"/>. Pokud úloha neběží, tak se nic nestane.
        /// </summary>
        Task StopJob(string jobId);

        /// <summary>
        /// Aktivuje úlohu podle předaného <paramref name="jobId"/>. Pokud je úloha aktivní, tak se nic nestane.
        /// </summary>
        Task EnableJob(string jobId);

        /// <summary>
        /// Vypne úlohu podle předaného <paramref name="jobId"/>. Pokud je úloha neaktniví, tak se nic nestane.
        /// </summary>
        Task DisableJob(string jobId);
    }
}
