﻿using System;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro migrační záznam
    /// </summary>
    public class RecordDto
    {
        /// <summary>
        /// Identifikátor
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Datum startu události
        /// </summary>
        public DateTime StartActionDate { get; set; }

        /// <summary>
        /// Datum konce události
        /// </summary>
        public DateTime EndActionDate { get; set; }

        /// <summary>
        /// Identifikátor objektu migrace
        /// </summary>
        public string ObjectId { get; set; }

        //public string GroupId { get; set; }

        /// <summary>
        /// Název profilu, který prováděl migraci
        /// </summary>
        public string ProfileName { get; set; }

        /// <summary>
        /// Identifikátor stavu migrace
        /// </summary>
        public int? MigrationStateId { get; set; }
    }
}
