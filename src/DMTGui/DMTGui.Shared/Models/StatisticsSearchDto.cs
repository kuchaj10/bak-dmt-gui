﻿using System;

namespace DMTGui.Shared.Models
{
    public class StatisticsSearchDto
    {
        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
