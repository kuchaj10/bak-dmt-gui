﻿using System;

namespace DMTGui.Shared.Models
{
    public class MigrationDayDto
    {
        /// <summary>
        /// Den migrace
        /// </summary>
        public DateTime Day { get; set; }

        /// <summary>
        /// Čas počátku migrace pro daný den
        /// </summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// Čas konce migrace pro daný den
        /// </summary>
        public TimeSpan EndTime { get; set; }

        /// <summary>
        /// Počet migrovaných dat
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Počet úspěšně migrovaných dat
        /// </summary>
        public int SuccessfullCount { get; set; }

        /// <summary>
        /// Počet neúspěšně migrovaných dat
        /// </summary>
        public int WrongCount { get; set; }

        /// <summary>
        /// Průměrná rychlost úspěšné migrace pro jeden objekty
        /// </summary>
        public TimeSpan SuccessfullSpeed { get; set; }
    }
}
