﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro link profilu, zkrácený záznam oproti celému profilu
    /// </summary>
    public class ProfileLinkDto
    {
        /// <summary>
        /// Identifikátor profilu
        /// </summary>
        public string ProfileId { get; set; }
        
        /// <summary>
        /// Název profilu
        /// </summary>
        public string ProfileName { get; set; }

        public override string ToString()
        {
            return ProfileName;
        }
    }
}
