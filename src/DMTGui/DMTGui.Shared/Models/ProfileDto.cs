﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO profilu
    /// </summary>
    public class ProfileDto
    {
        /// <summary>
        /// Identifikátoru
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Název profilu
        /// </summary>
        [MaxLength(256, ErrorMessage = "Maximální délka názvu je {1} znaků.")]
        public string Name { get; set; }

        /// <summary>
        /// Data profilu, která jsou reprezentována jako migrační profil. Tudíž se jedná o xml soubor.
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Verze profilu
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Počet chyb migrace
        /// </summary>
        public int ErrorMigrationCount { get; set; }

        /// <summary>
        /// Datum a čas vytvoření profilu
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Identifikátor úlohy se kterou je profil svázaný
        /// </summary>
        public string JobId { get; set; }

        /// <summary>
        /// Název úlohy se kterou je migrace spojená
        /// </summary>
        public string JobName { get; set; }

        public ProfileDto()
        {
        }

        public ProfileDto(ProfileDto that)
        {
            this.Id = that.Id;
            this.Name = that.Name;
            this.JobId = that.JobId;
            this.JobName = that.JobName;
            this.Data = that.Data;
            this.Version = that.Version;
            this.ErrorMigrationCount = that.ErrorMigrationCount;
        }

        public void SetProfile(ProfileDto that)
        {
            this.Id = that.Id;
            this.Name = that.Name;
            this.JobId = that.JobId;
            this.JobName = that.JobName;
            this.Data = that.Data;
            this.Version = that.Version;
            this.ErrorMigrationCount = that.ErrorMigrationCount;
        }

        public override string ToString()
        {
            var profileBuilder = new StringBuilder();
            profileBuilder.Append("{ ");
            profileBuilder.Append($"Id = {Id}, ");
            profileBuilder.Append($"Name = {Name}, ");
            profileBuilder.Append($"Version = {Version}, ");
            profileBuilder.Append($"ErrorMigrationCount = {ErrorMigrationCount}, ");
            profileBuilder.Append(" }");

            return profileBuilder.ToString();
        }
    }
}
