﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// Úroveň chyby
    /// </summary>
    public enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Error,
        Critical
    }
}
