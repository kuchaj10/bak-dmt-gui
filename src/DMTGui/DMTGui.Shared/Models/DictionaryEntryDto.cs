﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO slovníkového záznamu
    /// </summary>
    public class DictionaryEntryDto
    {
        /// <summary>
        /// Klíč
        /// </summary>
        [Required]
        [MaxLength(250, ErrorMessage = "Maximální délka klíče je {1} znaků.")]
        public string Key { get; set; }

        /// <summary>
        /// Seznam položek
        /// </summary>
        public DictionaryEntryPropertyDto[] Properties { get; set; }

        /// <summary>
        /// Popis
        /// </summary>
        [MaxLength(512, ErrorMessage = "Maximální délka popisu je {1} znaků.")]
        public string Description { get; set; }

        public DictionaryEntryDto()
        {
        }

        public DictionaryEntryDto(DictionaryEntryDto that)
        {
            Key = that.Key;
            Description = that.Description;

            if (that.Properties != null)
            {
                Properties = that.Properties.Select(prt => new DictionaryEntryPropertyDto(prt)).ToArray();
            }
        }
    }
}
