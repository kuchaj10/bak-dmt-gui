﻿using DMTGui.Shared.Validations;
using System.ComponentModel.DataAnnotations;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro spouštěče
    /// </summary>
    public class TriggerDto
    {
        /// <summary>
        /// Identifikátor
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Popis
        /// </summary>
        [MaxLength(1024, ErrorMessage = "Popis triggeru je pouze {1} znaků")]
        public string Description { get; set; }

        /// <summary>
        /// Nastavení, obsahuje CRON
        /// </summary>
        [Cron(ErrorMessage = "Cron '{1}' není validní", IsRequired = true)]
        public string Settings { get; set; }
    }
}
