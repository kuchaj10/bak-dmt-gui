﻿using System;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro záznam chyby
    /// </summary>
    public class LogDto
    {
        /// <summary>
        /// Identifikátor chyby
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Identifikátor profilu
        /// </summary>
        public string JobId { get; set; }

        /// <summary>
        /// Název profilu
        /// </summary>
        public string ProfileName {get; set;}

        /// <summary>
        /// Identifikátor objektu
        /// </summary>
        public string ObjectId { get; set; }

        //public int? MigrationRecordId { get; set; }

        /// <summary>
        /// Stav záznamu migrace
        /// </summary>
        public string MigrationRecordState { get; set; }

        /// <summary>
        /// Datum záznamu
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Vlákno záznamu
        /// </summary>
        public string Thread { get; set; }

        /// <summary>
        /// Level chyby záznamu
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// Kontext záznamu
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// Zpráva záznamu
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Chyba záznamu
        /// </summary>
        public string Exception { get; set; }
    }
}
