﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// Sloupeček podle kterého se mají řadit záznamy
    /// </summary>
    public enum LogSearchSortColumn
    {
        None,
        Id,
        ProfileName,
        ObjectId,
        JobId,
        //MigrationRecordId,
        MigrationRecordState,
        Date,
        Thread,
        Level,
        Context,
        Message,
        Exception
    }
}
