﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro vyhledávání, obsahuje základní informace a slouží k rozšiřování 
    /// </summary>
    public class SearchDto
    {
        /// <summary>
        /// Kolik záznamů se má přeskočit
        /// </summary>
        public int? Skip { get; set; }

        /// <summary>
        /// Kolik záznamů se má vzít
        /// </summary>
        public int? Take { get; set; }

        public override string ToString()
        {
            var skip = Skip?.ToString() ?? "null";
            var take = Take?.ToString() ?? "null";

            return $"{nameof(Skip)}={skip}, {nameof(Take)}={take}";
        }
    }
}
