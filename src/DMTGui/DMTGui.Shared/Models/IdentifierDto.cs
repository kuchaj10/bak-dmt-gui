﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO identifikátoru
    /// </summary>
    public class IdentifierDto
    {
        /// <summary>
        /// Identifikátor
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Název profilu
        /// </summary>
        public string ProfileName { get; set; }

        //public string JobName { get; set; }

        /// <summary>
        /// ZdrojId
        /// </summary>
        public string SourceId { get; set; }

        /// <summary>
        /// HodnotaId
        /// </summary>
        public string ValueId { get; set; }

        /// <summary>
        /// Identifikátor objektu
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// Skupinové id
        /// </summary>
        public string GroupId { get; set; }

        /// <summary>
        /// Stav identifikátoru
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Poznámka identifikátoru
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Seznam souvisejících identifikátorů
        /// </summary>
        public IdentifierDto[] RelatedIdentifiers { get; set; }
    }
}
