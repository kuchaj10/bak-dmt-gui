﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro přenos detailů chyby
    /// </summary>
    public class ErrorDetail
    {
        /// <summary>
        /// Kód statusu
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Zpráva chyby
        /// </summary>
        public string Message { get; set; }

    }
}
