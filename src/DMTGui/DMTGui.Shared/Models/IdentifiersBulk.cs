﻿namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro seznam většího množství identifikátorů
    /// </summary>
    public class IdentifiersBulk
    {
        public IdentifierDto[] Identifiers { get; set; }
    }
}
