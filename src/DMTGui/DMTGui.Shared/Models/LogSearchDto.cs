﻿using System;
using System.Collections.Generic;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO pro zadání detailů k vyhledávání logů
    /// </summary>
    public class LogSearchDto
    {
        public LogSearchDto()
        {
            LogLevels = new List<string>();
            SortColumn = LogSearchSortColumn.None;
        }

        /// <summary>
        /// Identifikátor úlohy
        /// </summary>
        public string JobId { get; set; }

        /// <summary>
        /// Název profilu
        /// </summary>
        public string ProfileName { get; set; }

        /// <summary>
        /// Identifikátor objektu 
        /// </summary>
        public string ObjectId { get; set; }

        //public int? MigrationRecordId { get; set; }

        /// <summary>
        /// Stav migračního záznamu
        /// </summary>
        public string MigrationRecordState { get; set; }

        /// <summary>
        /// Úrovně záznamu chyby migrace
        /// </summary>
        public IEnumerable<string> LogLevels { get; set; }

        /// <summary>
        /// Zpráva migrace
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Chyba migrace
        /// </summary>
        public string Exception { get; set; }

        /// <summary>
        /// Kontext migrace
        /// </summary>
        public string Context { get; set; }

        /// <summary>
        /// Začátek datumu do kterého může záznam spadat
        /// </summary>
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// Konec datumu do kterého může záznam spadat
        /// </summary>
        public DateTime? ToDate { get; set; }

        /// <summary>
        /// Počet záznamů na přeskočení
        /// </summary>
        public int? Skip { get; set; }

        /// <summary>
        /// Počet záznamů na přebrání
        /// </summary>
        public int? Take { get; set; }

        /// <summary>
        /// Příznak, zda se jedná o vzestupné řazení
        /// </summary>
        public bool IsSortAscending { get; set; }

        /// <summary>
        /// Podle jakého columnu se řadí
        /// </summary>
        public LogSearchSortColumn SortColumn { get; set; }
    }
}
