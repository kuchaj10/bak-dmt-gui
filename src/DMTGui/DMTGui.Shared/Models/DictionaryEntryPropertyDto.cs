﻿using System.ComponentModel.DataAnnotations;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// Položka hodnoty slovníku
    /// </summary>
    public class DictionaryEntryPropertyDto
    {
        /// <summary>
        /// Klíč hodnoty
        /// </summary>
        [Required]
        [MaxLength(250, ErrorMessage ="Maximální délka klíče je {1} znaků.")]
        public string Key { get; set; }

        /// <summary>
        /// Hodnota
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Popis
        /// </summary>
        [MaxLength(512, ErrorMessage ="Maximální délka popisu je {1} znaků.")]
        public string Descripton { get; set; }

        public DictionaryEntryPropertyDto()
        {
        }

        public DictionaryEntryPropertyDto(DictionaryEntryPropertyDto that)
        {
            Key = that.Key;
            Value = that.Value; 
            Descripton = that.Descripton;
        }
    }
}
