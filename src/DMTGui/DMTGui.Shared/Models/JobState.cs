﻿namespace DMTGui.Shared.Models
{
    public enum JobState
    {
        /// <summary>
        /// Úloha je zastavená a není naplánované žádný další běh.
        /// </summary>
        Enabled,

        /// <summary>
        /// Úloha je neaktivní
        /// </summary>
        Disabled,

        /// <summary>
        /// Úloha je zastavená, ale čeká na na automatické spuštění.
        /// </summary>
        Waiting,

        /// <summary>
        /// Úloha je aktuálně aktivní a zpracovávají se data.
        /// </summary>
        Running,

        /// <summary>
        /// Úloha není ve validní stavu
        /// </summary>
        Invalid

        //Active,
        //Inactive,
    }
}
