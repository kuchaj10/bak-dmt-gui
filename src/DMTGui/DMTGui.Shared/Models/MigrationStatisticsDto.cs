﻿namespace DMTGui.Shared.Models
{
    public class MigrationStatisticsDto
    {
        /// <summary>
        /// Id migrace
        /// </summary>
        public string MigrationId { get; set; }

        /// <summary>
        /// Název migrace
        /// </summary>
        public string MigrationName { get; set; }

        /// <summary>
        /// Informace ke dním migrace
        /// </summary>
        public MigrationDayDto[] Days { get; set; }
    }
}
