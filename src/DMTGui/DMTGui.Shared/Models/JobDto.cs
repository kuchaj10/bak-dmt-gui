﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO úlohy
    /// </summary>
    public class JobDto
    {
        public JobDto()
        {
            Profiles = new ProfileLinkDto[0];
            Triggers = new TriggerDto[0];
        }

        /// <summary>
        /// Identifikátor
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Název úlohy
        /// </summary>
        [Required(ErrorMessage = "Jméno úlohy je povinné")]
        [MaxLength(256, ErrorMessage = "Název úlohy je omezený na 256 znaků")]
        public string Name { get; set; }

        /// <summary>
        /// Popis úlohy
        /// </summary>
        [MaxLength(1024, ErrorMessage = "Popis úlohy je omezený na 1024 znaků")]
        public string Description { get; set; }

        /// <summary>
        /// Aktuální velikost fronty zpracování
        /// </summary>
        public int ActualQueueSize { get; set; }

        /// <summary>
        /// Velikost fronty na zpracování
        /// </summary>
        public int QueueSize { get; set; }

        /// <summary>
        /// Aktuvální krok migrace
        /// </summary>
        public int ActualStep { get; set; }

        /// <summary>
        /// Všechny kroky migrace
        /// </summary>
        public int StepCount { get; set; }

        /// <summary>
        /// Profily migrace
        /// </summary>
        public ProfileLinkDto[] Profiles { get; set; }

        /// <summary>
        /// Spouštěče migrace
        /// </summary>
        public TriggerDto[] Triggers { get; set; }

        /// <summary>
        /// Stavy úlohy
        /// </summary>
        public JobState State { get; set; }

        /// <summary>
        /// Datum a čas posledního běhu úlohy migrace
        /// </summary>
        public DateTime? LastRun { get; set; }

        /// <summary>
        /// Datum a čas konce běhu migrační úlohy
        /// </summary>
        public DateTime? ScheduledStop { get; set; }

        /// <summary>
        /// Datum a čas vytvoření úlohy
        /// </summary>
        public DateTime? Created { get; set; }
    }
}
