﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DMTGui.Shared.Models
{
    /// <summary>
    /// DTO slovníku
    /// </summary>
    public class DictionaryDto
    {
        public DictionaryDto()
        {
            Entries = Array.Empty<DictionaryEntryDto>();
        }

        /// <summary>
        /// Název slovníku
        /// </summary>
        [Required]
        [MaxLength(250, ErrorMessage = "Maximální název slovníku je pouze {1} znaků")]
        public string Name { get; set; }

        /// <summary>
        /// Hodnoty slovníku
        /// </summary>
        public DictionaryEntryDto[] Entries { get; set; }

        /// <summary>
        /// Počet hodnot slovníku
        /// </summary>
        public int EntriesCount { get; set; }
    }
}
