﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DMTGui.Shared.Extensions;

/// <summary>
/// Rozšíření pro <see cref="IEnumerable{T}"/>
/// </summary>
public static class EnumerableExtensions
{
    /// <summary>
    /// Přeskočí vybraný <paramref name="itemToSkip"/> v předaném <paramref name="enumerable"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static IEnumerable<TValue> SkipItem<TValue>(this IEnumerable<TValue> enumerable, TValue itemToSkip)
    {
        enumerable.ThrowIfNull();
        itemToSkip.ThrowIfNull();

        return enumerable.Where(itm => !itm.Equals(itemToSkip));
    }

    /// <summary>
    /// Na konec předaného <paramref name="enumerable"/> vloží všechny itemy z <paramref name="itemsToAppend"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static IEnumerable<TValue> AppendRange<TValue>(this IEnumerable<TValue> enumerable, IEnumerable<TValue> itemsToAppend)
    {
        enumerable.ThrowIfNull();
        // ReSharper disable once PossibleMultipleEnumeration
        itemsToAppend.ThrowIfNull();

        var enumerableAsList = enumerable.ToList();
        
        // ReSharper disable once PossibleMultipleEnumeration
        enumerableAsList.AddRange(itemsToAppend);

        return enumerableAsList;
    }

    /// <summary>
    /// Zjistí index <paramref name="itemToFind"/> z předaného <paramref name="enumerable"/>. Pokud ho nenajde, tak vrátí -1.
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
    public static int IndexOf<TValue>(this IEnumerable<TValue> enumerable, TValue itemToFind)
    {
        enumerable.ThrowIfNull();
        itemToFind.ThrowIfNull();

        var index = 0;
        foreach (var item in enumerable)
        {
            if (item.Equals(itemToFind))
            {
                return index;
            }

            index++;
        }
        return -1;
    }

    /// <summary>
    /// Najde <paramref name="itemToMove"/> v předaném <paramref name="enumerable"/> a ten posune o jeden item dopředu.
    /// </summary>
    /// <example>
    /// [1, 2, 3] => MoveItemUp(2) => [2, 1, 3]
    /// </example>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
	public static IEnumerable<TValue> MoveItemUp<TValue>(this IEnumerable<TValue> enumerable, TValue itemToMove)
	{
		enumerable.ThrowIfNull();
        itemToMove.ThrowIfNull();

		var index = enumerable.IndexOf(itemToMove);
		if (index < 0)
			throw new ArgumentException($"Value from argument {nameof(itemToMove)} is not present in {nameof(enumerable)} colection", nameof(itemToMove));

		enumerable = enumerable.SkipItem(itemToMove);

		return enumerable.InsertItem(itemToMove, index - 1);
	}

    /// <summary>
    /// Najde <paramref name="itemToMove"/> v předaném <paramref name="enumerable"/> a ten posune o jeden item dozadu.
    /// </summary>
    /// <example>
    /// [1, 2, 3] => MoveItemDown(1) => [2, 1, 3]
    /// </example>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentNullException"></exception>
	public static IEnumerable<TValue> MoveItemDown<TValue>(this IEnumerable<TValue> enumerable, TValue itemToMove)
	{
		enumerable.ThrowIfNull();
        itemToMove.ThrowIfNull();

		var index = enumerable.IndexOf(itemToMove);
		if (index < 0)
			throw new ArgumentException($"Value from argument {nameof(itemToMove)} is not present in {nameof(enumerable)} colection", nameof(itemToMove));

		enumerable = enumerable.SkipItem(itemToMove);

		return enumerable.InsertItem(itemToMove, index + 1);
	}

    /// <summary>
    /// Vloží <paramref name="itemToInsert"/> do předaného <paramref name="enumerable"/> na vybraný <paramref name="index"/>.
    /// </summary>
    /// <exception cref="ArgumentNullException"></exception>
	public static IEnumerable<TValue> InsertItem<TValue>(this IEnumerable<TValue> enumerable, TValue itemToInsert, int index)
	{
		enumerable.ThrowIfNull();
        itemToInsert.ThrowIfNull();

		var before = index > 0
			? enumerable.Take(index)
			: new TValue[0];

		var after = index < enumerable.Count()
			? enumerable.Skip(index)
			: new TValue[0];

		enumerable = before.Append(itemToInsert).AppendRange(after);

		return enumerable;
	}
}