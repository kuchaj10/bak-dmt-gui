﻿using System;
using System.Linq;

namespace DMTGui.Shared.Extensions
{
    /// <summary>
    /// Rozšíření pro <see cref="Type"/>
    /// </summary>
    public static class TypeExtensions
    {
        /// <summary>
        /// Ověří, že předaný objekt obsahuje konstruktor s typem argumentu <typeparamref name="TParam"/>
        /// </summary>
        public static bool HasConstructorWithType<TParam>(this Type type) where TParam : class
        {
            var typeToSearch = typeof(TParam);
            var parameters = type
                .GetConstructors()
                .SelectMany(cnt => cnt.GetParameters());

            return parameters.Any(pam => pam.ParameterType == typeToSearch);
        }
    }
}
