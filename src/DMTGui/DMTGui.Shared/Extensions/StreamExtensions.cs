﻿using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DMTGui.Shared.Extensions
{
    /// <summary>
    /// Rozšíření pro <see cref="Stream"/>
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// Přečte a získá data předaného streamu.
        /// </summary>
        public static async Task<byte[]> ReadAllBytesAsync(this Stream stream)
        {
            using var memoryStream = new MemoryStream();

            stream.Position = 0;
            await stream.CopyToAsync(memoryStream);

            return memoryStream.ToArray();
        }

        /// <summary>
        /// Z předaného streamu se pokusí získat encoding. Pokud se to nepodaří vrátí <see cref="Encoding.UTF8"/>.
        /// </summary>
        public static Encoding GetEncoding(this Stream stream)
        {
            using var reader = new StreamReader(stream, Encoding.UTF8, true);
            reader.Peek(); // Je potřeba udělat peek pro zjištění encodingu

            return reader.CurrentEncoding;
        }
    }
}
