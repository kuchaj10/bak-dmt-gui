﻿using System;

namespace DMTGui.Shared.Extensions
{
    public static class GuidExtensions
    {
        /// <summary>
        /// Předaný guid převede na kratší base64 textovou hodnotu. 
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public static string ToBase64String(this Guid guid)
        {
            var base64String = Convert.ToBase64String(guid.ToByteArray())
                .Substring(0, 22)
                .Replace("/", "_")
                .Replace("+", "-");

            return base64String;
        }

        /// <summary>
        /// Předaný base64 text převede na guid. 
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static Guid ToGuidFromBase64String(this string base64string)
        {
            base64string.ThrowIfWrongLength(22);

            base64string = base64string
                .Replace("_", "/")
                .Replace("-", "+");

            base64string = $"{base64string}==";

            var guidBytes = Convert.FromBase64String(base64string);

            return new Guid(guidBytes);
        }

        /// <summary>
        /// Ověří, že je možné string převést na guid.
        /// </summary>
        public static bool CanConvertToBase64(this string base64string)
        {
            if (base64string.Length != 22)
                return false;

            try
            {
                base64string = base64string
                .Replace("_", "/")
                .Replace("-", "+");

                base64string = $"{base64string}==";

                var guidBytes = Convert.FromBase64String(base64string);
                var guid = new Guid(guidBytes);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
