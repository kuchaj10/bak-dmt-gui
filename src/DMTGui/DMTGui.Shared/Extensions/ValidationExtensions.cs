﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace DMTGui.Shared.Extensions
{
    public static class ValidationExtensions
    {
		/// <summary>
		/// Předaný objekt zvaliduje pomocí <see cref="Validator"/>, který hlídá pravidla pomocí <see cref="System.ComponentModel.DataAnnotations"/>.
		/// V případě chyby vrátí seznam chybových zpráv, který odpovídá problémům z validace. 
		/// </summary>
		public static bool ValidateObject<TObject>(this TObject objectToValidate, out IEnumerable<string> errorMessages)
		{
			var context = new ValidationContext(objectToValidate);
			var validationResults = new List<ValidationResult>();

			var isValid = Validator.TryValidateObject(objectToValidate, context, validationResults, true);
			if (!isValid)
			{
				errorMessages = validationResults
					.Where(res => !string.IsNullOrEmpty(res.ErrorMessage))
					.Select(res => res.ErrorMessage)
					.ToArray();
			}
			else
			{
				errorMessages = Array.Empty<string>();
			}

			return isValid;
		}
	}
}
