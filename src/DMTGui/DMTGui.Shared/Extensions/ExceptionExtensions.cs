﻿using System;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Collections.Generic;

namespace DMTGui.Shared.Extensions
{
	/// <summary>
	/// Rozšíření pro validaci argumentů v metodách, nebo konstruktorech. 
	/// </summary>
    public static class ExceptionExtensions
    {
		/// <summary>
		/// Pokud je <paramref name="argument"/> null, tak vyhodí <see cref="ArgumentNullException"/>.
		/// </summary>
		/// <exception cref="ArgumentNullException"></exception>
		public static void ThrowIfNull<TArgument>(this TArgument argument, [CallerArgumentExpression("argument")] string paramName = null)
		{
			if (argument is null)
				throw new ArgumentNullException(paramName);
		}

		/// <summary>
		/// Pokud je <paramref name="argument"/> enumerable prázdný, tak vyhodí <see cref="ArgumentException"/>.
		/// </summary>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		public static void ThrowIfEmpty<T>(this IEnumerable<T> argument, [CallerArgumentExpression("argument")] string paramName = null)
		{
			ThrowIfNull(argument, paramName);

			if (!argument.Any())
				throw new ArgumentException("Enumerable cannot be empty.", paramName);
		}

		/// <summary>
		/// Pokud je <paramref name="argument"/> string prázdný, tak vyhodí <see cref="ArgumentException"/>.
		/// </summary>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		public static void ThrowIfEmpty(this string argument, [CallerArgumentExpression("argument")] string paramName = null)
		{
			ThrowIfNull(argument, paramName);

			if (string.IsNullOrEmpty(argument))
				throw new ArgumentException("Value cannot be empty.", paramName);
		}

		/// <summary>
		/// Pokud je <paramref name="argument"/> string whitespace, tak vyhodí <see cref="ArgumentException"/>.
		/// </summary>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		public static void ThrowIfWhiteSpace(this string argument, [CallerArgumentExpression("argument")] string paramName = null)
		{
			ThrowIfNull(argument, paramName);

			if (string.IsNullOrWhiteSpace(argument))
				throw new ArgumentException("Value cannot be whitespace.", paramName);
		}

		/// <summary>
		/// Pokud <paramref name="argument"/> nemá správnou délku <paramref name="correctLength"/>, tak vyhodí <see cref="ArgumentException"/>.
		/// </summary>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		public static void ThrowIfWrongLength(this string argument, int correctLength, [CallerArgumentExpression("argument")] string paramName = null)
		{
			ThrowIfNull(argument, paramName);

			if (argument.Length != correctLength)
				throw new ArgumentException($"Value \"{argument}\" has wrong length. Expected length {correctLength}.", paramName);
		}

		/// <summary>
		/// Ověří předaný <paramref name="argument"/> podle na něm definovaných DataAnnotations. Pokud není validní, tak vyhodí <see cref="ArgumentException"/> se seznamem nevalidních položek a zpráv.
		/// </summary>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		public static void ThrowIfNotValid<TArgument>(this TArgument argument, [CallerArgumentExpression("argument")] string paramName = null)
		{
			ThrowIfNull(argument, paramName);

			if (argument.ValidateObject(out var messages))
				return;

			messages = messages.Select(msg => $"\"{msg}\"");
			var joinMessages = string.Join(", ", messages);
			var typeName = argument.GetType().Name;

			throw new ArgumentException($"Value is not valid type of {typeName}: {joinMessages}", paramName);
		}

		//public static void ThrowIfLesser<TArgument>(this TArgument argument, TArgument lesserValue, [CallerArgumentExpression("argument")] string paramName = null)
		//	where TArgument : IComparable
		//{
		//	if (lesserValue.CompareTo(argument) == -1)
		//		throw new ArgumentException($"Argument nemůže být menší než {lesserValue}", paramName);
		//}
		//
		//public static void ThrowIfGreater<TArgument>(this TArgument argument, TArgument greaterValue, [CallerArgumentExpression("argument")] string paramName = null)
		//	where TArgument : IComparable
		//{
		//	if (greaterValue.CompareTo(argument) == 1)
		//		throw new ArgumentException($"Argument nemůže být větší než {greaterValue}", paramName);
		//}
		//
		//public static void ThrowIfNotBetween<TArgument>(this TArgument argument, TArgument lesserValue, TArgument greaterValue, [CallerArgumentExpression("argument")] string paramName = null)
		//	where TArgument : IComparable
		//{
		//	if (lesserValue.CompareTo(argument) == -1 || greaterValue.CompareTo(argument) == 1)
		//		throw new ArgumentException($"Argument nemůže být mimo {lesserValue} a {greaterValue}", paramName);
		//}
	}
}
