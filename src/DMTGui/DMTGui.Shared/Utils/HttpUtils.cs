﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Web;

namespace DMTGui.Shared.Utils
{
    /// <summary>
    /// Nástroje na práci s HTTP
    /// </summary>
    public class HttpUtils
    {
        /// <summary>
        /// Z předaného jednoduchého modelu získá položky a postaví z ních parametry pro uri.
        /// </summary>
        public static string CreateUriFromModel<TModel>(TModel model)
        {
            if (model is null)
                return "";

            var parameters = ExtractParamsFromModel(model);

            return FormatParametersToUri(parameters);
        }

        // TODO (Jakub): Udělat na to test
        /// <summary>
        /// Předanou třídu vezme a udělá z ní parametry pro http. 
        /// Funguje pouze na value type hodnoty a value type arraye. 
        /// Všem názvům u property dá první písmenko na lowercase.
        /// </summary>
        public static IEnumerable<KeyValuePair<string, object>> ExtractParamsFromModel<TModel>(TModel model)
        {
            var parameters = new List<KeyValuePair<string, object>>();
            var type = typeof(TModel);
            var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in properties)
            {
                var propertyName = FirstToLower(property.Name);

                var value = property.GetValue(model);

                if (property.PropertyType != typeof(string) && typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
                {
                    parameters.AddRange(ExtractEnumerable(propertyName, value as IEnumerable));

                }
                else
                {
                    parameters.Add(KeyValuePair.Create(propertyName, value));
                }
            }

            return parameters;
        }

        /// <summary>
        /// Předané parametry v kombinaci název hodnoty a hodnota naformátuje do podoby uri. 
        /// Například: hodnota1=AAA&hodnota2=BBB&array=CCC&array=DDD
        /// </summary>
        public static string FormatParametersToUri(IEnumerable<KeyValuePair<string, object>> parameters)
        {
            var formatedParameters = new List<string>();

            foreach (var parameter in parameters)
            {
                if (string.IsNullOrEmpty(parameter.Key))
                    throw new ArgumentException("Klíč nemůže být prázdný");

                var parameterKey = HttpUtility.UrlEncode(parameter.Key);
                var parameterValue = FormatValue(parameter.Value);

                if (string.IsNullOrEmpty(parameterValue))
                    continue;

                formatedParameters.Add($"{new string(parameterKey)}={parameterValue}");
            }

            return string.Join("&", formatedParameters);
        }

        public static string FormatValue(object value)
        {
            if (value is DateTime date)
            {
                value = date.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }

            if (value is DateTimeOffset dateOffset)
            {
                value = dateOffset.ToString("yyyy-MM-ddTHH:mm:ss.fff");
            }

            return HttpUtility.UrlEncode(HttpUtility.HtmlEncode(value));
        }

        private static List<KeyValuePair<string, object>> ExtractEnumerable(string paramName, IEnumerable enumerable)
        {
            enumerable ??= Array.Empty<string>();

            var parameters = new List<KeyValuePair<string, object>>();

            foreach (var value in enumerable)
            {
                parameters.Add(KeyValuePair.Create(paramName, value));
            }

            return parameters;
        }

        private static string FirstToLower(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            var textAsArray = text.ToCharArray();
            textAsArray[0] = char.ToLower(text[0]);

            return new string(textAsArray);
        }
    }
}
