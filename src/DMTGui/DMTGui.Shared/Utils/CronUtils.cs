﻿using CronExpressionDescriptor;
using DMTGui.Shared.Extensions;
using Quartz;
using System;

namespace DMTGui.Shared.Utils
{
    /// <summary>
    /// Nástroje pro manipulaci s CRON stringy
    /// </summary>
    public class CronUtils
    {
        /// <summary>
        /// Ověří pomocí definice triggerů v <see cref="Quartz"/> validitu předaného CRONu
        /// </summary>
        public static bool CheckIsCronValid(string cronString, out string message)
        {
            message = null;

            cronString.ThrowIfNull();

            try
            {
                var trigger = TriggerBuilder.Create()
                    .WithCronSchedule(cronString)
                    .Build();

                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Pokusí se z předaného CRONu získat další datum spouštění od aktuálního datumu.
        /// Pokud je CRON nevalidní, tak vrátí false. Jinak true a hodnotu, která může být 
        /// prázdná, pokud se neočekává další spuštění.
        /// </summary>
        public static bool TryGetCrontabNextOccurrence(string cronString, DateTime fromDate, out DateTime? nextOccurrence)
        {
            nextOccurrence = null;

            if (!CheckIsCronValid(cronString, out _))
                return false;

            nextOccurrence = GetCrontabNextOccurrence(cronString, fromDate);
            return true;
        }

        /// <summary>
        /// Pokusí se z předaného CRONu získat další datum spouštění od aktuálního datumu.
        /// </summary>
        public static DateTime? GetCrontabNextOccurrence(string cronString, DateTime fromDate)
        {
            cronString.ThrowIfEmpty();
            fromDate.ThrowIfNull();

            var trigger = TriggerBuilder.Create()
                    .WithCronSchedule(cronString)
                    .Build();

            DateTime? fireTime = null;

            var offset = trigger.GetFireTimeAfter(DateTime.Now);
            if (offset.HasValue)
            {
                fireTime = offset.Value.ToLocalTime().DateTime;
            }

            return fireTime;
        }

        /// <summary>
        /// Předaný CRON se pokusí reprezentovat jako lidsky čitelný string. Zatím v angličině.
        /// </summary>
        public static string GetHumanReadableCron(string cronString)
        {
            cronString.ThrowIfEmpty();

            try
            {
                var options = new Options()
                {
                    DayOfWeekStartIndexZero = false,
                    Use24HourTimeFormat = true,
                    Locale = "en",
                    Verbose = true
                };

                var humanReadableCron = ExpressionDescriptor.GetDescription(cronString, options);

                return humanReadableCron;
            }
            catch
            {
                return null;
            }
        }
    }
}
