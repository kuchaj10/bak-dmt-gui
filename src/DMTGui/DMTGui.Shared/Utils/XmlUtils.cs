﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace DMTGui.Shared.Utils
{
    /// <summary>
    /// Nástroje pro manipulaci s XML
    /// </summary>
    public class XmlUtils
    {
        /// <summary>
        /// Deserializuje předané <paramref name="xml"/> do předaného <typeparamref name="T"/>
        /// </summary>
        public static T Deserialize<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));

            using var reader = new StringReader(xml);
            using var xmlReader = XmlReader.Create(reader);

            return (T)serializer.Deserialize(xmlReader);
        }
    }
}
