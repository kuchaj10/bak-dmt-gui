﻿using System.Collections.Generic;

namespace DMTGui.Shared.Utils
{
    /// <summary>
    /// Pomocné nástroje reflexe
    /// </summary>
    public static class ReflectionUtils
    {
        /// <summary>
        /// Reprezentace hodnoty a názvu položky z modelu, ve kterém byla uložena.
        /// </summary>
        public struct ValueItem
        {
            /// <summary>
            /// Název položky
            /// </summary>
            public string Name { get; set; }

            /// <summary>
            /// Hodnota položky
            /// </summary>
            public object Value { get; set; }
        }

        /// <summary>
        /// Z jednoduchého nekomplexního modelu získá všechny položky jako <see cref="ValueItem"/>, 
        /// který obsahuje název a hodnotu položky.
        /// </summary>
        public static ValueItem[] GetValuesFromModel<TModel>(TModel model)
        {
            var items = new List<ValueItem>();

            var type = typeof(TModel);
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                var value = property.GetValue(model);
                if (value is null)
                    continue;

                items.Add(new ValueItem { Name = property.Name, Value = value });
            }

            return items.ToArray();
        }
    }
}
